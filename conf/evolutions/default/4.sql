-- !Ups
ALTER TABLE recurring_transactions
    ADD COLUMN payee_monetary_account_id CHAR(36) DEFAULT NULL;

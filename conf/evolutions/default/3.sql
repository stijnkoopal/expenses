-- !Ups
ALTER TABLE monetary_account
    ALTER COLUMN iban TYPE VARCHAR(34);
ALTER TABLE transaction
    ALTER COLUMN payer_iban TYPE VARCHAR(34);
ALTER TABLE transaction
    ALTER COLUMN payee_iban TYPE VARCHAR(34);
ALTER TABLE bunq_account
    ALTER COLUMN iban TYPE VARCHAR(34);
ALTER TABLE rabo_account
    ALTER COLUMN iban TYPE VARCHAR(34);
ALTER TABLE allocation_counterparties
    ALTER COLUMN iban TYPE VARCHAR(34);
ALTER TABLE recurring_transactions
    ALTER COLUMN payer_iban TYPE VARCHAR(34);
ALTER TABLE recurring_transactions
    ALTER COLUMN payee_iban TYPE VARCHAR(34);

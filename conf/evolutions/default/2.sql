-- !Ups
CREATE TABLE IF NOT EXISTS monetary_account_cluster
(
    id             CHAR(36)     NOT NULL PRIMARY KEY,
    user_id        VARCHAR(255) NOT NULL,
    name           VARCHAR(60),
    configurations TEXT,
    created        TIMESTAMP    NOT NULL,
    updated        TIMESTAMP
);

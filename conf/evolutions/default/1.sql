-- !Ups

CREATE TABLE IF NOT EXISTS monetary_account
(
    id           CHAR(36)       NOT NULL PRIMARY KEY,
    user_ids     VARCHAR(255)[] NOT NULL DEFAULT ARRAY []::VARCHAR[],
    iban         VARCHAR(25),
    institution  VARCHAR(255),
    alias        VARCHAR(255),
    currency     CHAR(3)        NOT NULL,
    joint        BOOLEAN        NOT NULL DEFAULT FALSE,
    auto_updated BOOLEAN        NOT NULL DEFAULT FALSE,
    created      TIMESTAMP      NOT NULL,
    updated      TIMESTAMP
);

CREATE TABLE IF NOT EXISTS transaction
(
    id                  CHAR(36)     NOT NULL PRIMARY KEY,
    monetary_account_id CHAR(36)     NOT NULL,
    amount              DECIMAL      NOT NULL,
    currency            CHAR(3)      NOT NULL,
    direction           VARCHAR(100) NOT NULL,
    payer_iban          VARCHAR(25),
    payer_name          VARCHAR(255),
    payee_iban          VARCHAR(25),
    payee_name          VARCHAR(255),
    description         TEXT         NOT NULL DEFAULT '',
    timestamp           TIMESTAMP    NOT NULL
);

CREATE INDEX IF NOT EXISTS idx_transaction_monetary_account_id
    ON transaction (monetary_account_id);

CREATE TABLE IF NOT EXISTS balance_history
(
    monetary_account CHAR(36)  NOT NULL,
    timestamp        TIMESTAMP NOT NULL,
    balance          DECIMAL   NOT NULL,
    PRIMARY KEY (monetary_account, timestamp)
);

CREATE TABLE IF NOT EXISTS bunq_authentication
(
    id          UUID         NOT NULL PRIMARY KEY,
    user_id     VARCHAR(255) NOT NULL,
    api_context TEXT,
    bunq_id     VARCHAR(255)
);

CREATE INDEX IF NOT EXISTS idx_bunq_authentication_user_id
    ON bunq_authentication (user_id);

CREATE UNIQUE INDEX IF NOT EXISTS idx_bunq_authentication_bunq_id
    ON bunq_authentication (bunq_id);

CREATE TABLE IF NOT EXISTS bunq_account
(
    id                     CHAR(36)     NOT NULL PRIMARY KEY,
    iban                   VARCHAR(25),
    bunq_id                INTEGER      NOT NULL,
    bunq_user_id           VARCHAR(100) NOT NULL,
    currency               CHAR(3)      NOT NULL,
    alias                  VARCHAR(255),
    last_transactions_sync TIMESTAMP,
    inserted               TIMESTAMP    NOT NULL,
    updated                TIMESTAMP
);

CREATE TABLE IF NOT EXISTS rabo_authentication
(
    user_id              VARCHAR(255) NOT NULL,
    access_token         TEXT         NOT NULL,
    access_token_expiry  TIMESTAMP    NOT NULL,
    refresh_token        TEXT         NOT NULL,
    refresh_token_expiry TIMESTAMP    NOT NULL
);

CREATE INDEX IF NOT EXISTS idx_rabo_authentication_user_id
    ON rabo_authentication (user_id);

CREATE TABLE IF NOT EXISTS rabo_account
(
    iban                   VARCHAR(25)  NOT NULL PRIMARY KEY,
    rabo_id                varchar(100) NOT NULL,
    currency               CHAR(3)      NOT NULL,
    alias                  VARCHAR(255),
    last_transactions_sync TIMESTAMP,
    inserted               TIMESTAMP    NOT NULL,
    updated                TIMESTAMP
);

CREATE TABLE IF NOT EXISTS allocation_counterparties
(
    id                    CHAR(36)  NOT NULL PRIMARY KEY,
    iban                  VARCHAR(25),
    name                  VARCHAR(500),
    categories_when_payer VARCHAR(80)[],
    categories_when_payee VARCHAR(80)[],
    partial_match_on      VARCHAR(255),
    created               TIMESTAMP NOT NULL DEFAULT NOW(),
    updated               TIMESTAMP
);


CREATE TABLE IF NOT EXISTS recurring_transactions
(
    id                  CHAR(36)     NOT NULL PRIMARY KEY,
    start_date          TIMESTAMP    NOT NULL,
    end_date            TIMESTAMP,
    frequency           VARCHAR(10),
    amount              DECIMAL      NOT NULL,
    currency            CHAR(3)      NOT NULL,
    direction           VARCHAR(100) NOT NULL,
    payee_iban          VARCHAR(25),
    payee_name          VARCHAR(255),
    payer_iban          VARCHAR(25),
    payer_name          VARCHAR(255),
    monetary_account_id CHAR(36)     NOT NULL,
    source              VARCHAR(50)  NOT NULL,
    "status"            VARCHAR(36)  NOT NULL DEFAULT 'ACTIVE',
    created             TIMESTAMP    NOT NULL DEFAULT NOW()
);

CREATE INDEX IF NOT EXISTS idx_recurring_transactions_monetary_account_id
    ON recurring_transactions (monetary_account_id);

CREATE TABLE IF NOT EXISTS recurring_materialized_transactions
(
    id                       CHAR(36)  NOT NULL PRIMARY KEY,
    recurring_transaction_id CHAR(36)  NOT NULL,
    transaction_date         TIMESTAMP NOT NULL,
    created                  TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE INDEX IF NOT EXISTS idx_recurring_materialized_transactions_recurring_tx_id
    ON recurring_materialized_transactions (recurring_transaction_id);

CREATE TABLE IF NOT EXISTS public.journal
(
    ordering        BIGSERIAL,
    persistence_id  VARCHAR(255) NOT NULL,
    sequence_number BIGINT       NOT NULL,
    deleted         BOOLEAN      DEFAULT FALSE,
    tags            VARCHAR(255) DEFAULT NULL,
    message         BYTEA        NOT NULL,
    PRIMARY KEY (persistence_id, sequence_number)
);

CREATE UNIQUE INDEX IF NOT EXISTS journal_ordering_idx ON public.journal (ordering);

CREATE TABLE IF NOT EXISTS public.snapshot
(
    persistence_id  VARCHAR(255) NOT NULL,
    sequence_number BIGINT       NOT NULL,
    created         BIGINT       NOT NULL,
    snapshot        BYTEA        NOT NULL,
    PRIMARY KEY (persistence_id, sequence_number)
);

CREATE TABLE IF NOT EXISTS monetary_account_cluster_user_selection
(
    user_id          varchar(255) NOT NULL PRIMARY KEY,
    selected_cluster varchar(255) NOT NULL,
    created          TIMESTAMP    NOT NULL DEFAULT NOW(),
    updated          TIMESTAMP
);

package serde

import org.scalatest.wordspec.AnyWordSpec
import play.api.libs.json.{Format, Json}

import scala.util.Success

class PlayJsonSerdeTest extends AnyWordSpec {
  case class Test(a: String, b: Int)
  object Test {
    implicit val format: Format[Test] = Json.format
  }

  sealed trait Parent
  case class Child(a: String)  extends Parent
  case class Child2(b: String) extends Parent

  object Parent {
    implicit val childFormat: Format[Child]   = Json.format
    implicit val child2Format: Format[Child2] = Json.format
    implicit val parentFormat: Format[Parent] = Json.format
  }

  "PlayJsonSerde" when {
    "serializing and deserializing a simple object" should {
      "work properly" in {
        import PlayJsonDeserialization._
        import PlayJsonSerialization._

        val obj          = Test("a", 2)
        val serialized   = Serializable.serialize(obj)
        val deserialized = Deserializable.deserialize[Test](serialized)

        assert(deserialized === Success(obj))
      }
    }

    "serializing and deserializing a hierarchical object" should {
      "work properly" in {
        import PlayJsonDeserialization._
        import PlayJsonSerialization._

        val obj          = Child2("b")
        val serialized   = Serializable.serialize[Parent](obj)
        val deserialized = Deserializable.deserialize[Parent](serialized)

        assert(deserialized === Success(obj))
      }
    }
  }
}

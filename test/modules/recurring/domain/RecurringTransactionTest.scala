package modules.recurring.domain

import java.time.{Instant, Period}
import java.util.{Currency, UUID}

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.persistence.testkit.scaladsl.EventSourcedBehaviorTestKit
import com.typesafe.config.ConfigFactory
import eu.timepit.refined.api.Refined
import modules.recurring.domain
import modules.recurring.domain.RecurringTransaction.Source.DirectDebit
import modules.recurring.domain.RecurringTransaction.{
  NewOutboundRecurringTransactionFound,
  NewRecurringTransactionInstanceFound,
  ProcessRecurringDirectDebitTransaction,
  TransactionParty
}
import org.scalatest.BeforeAndAfterEach
import org.scalatest.Inside.inside
import org.scalatest.wordspec.AnyWordSpecLike
import types.{MonetaryAccountId, TransactionId}

class RecurringTransactionTest
    extends ScalaTestWithActorTestKit(
      EventSourcedBehaviorTestKit.config
        .withFallback(ConfigFactory.load("application-test.conf")))
    with AnyWordSpecLike
    with BeforeAndAfterEach {

  val monetaryAccountId: MonetaryAccountId = Refined.unsafeApply(UUID.randomUUID().toString)
  private val eventSourcedTestKit =
    EventSourcedBehaviorTestKit[domain.RecurringTransaction.Command, domain.RecurringTransaction.Event, domain.RecurringTransaction.State](
      system,
      RecurringTransaction(monetaryAccountId, null))

  val eur = Currency.getInstance("EUR")

  override def beforeEach(): Unit = {
    super.beforeEach()
    eventSourcedTestKit.clear()
  }

  "Persistent actor" should {

    "persist recurring transaction and instance event in case of tx" in {
      val transactionId: TransactionId = Refined.unsafeApply(UUID.randomUUID().toString)
      val transactionDate              = Instant.now()
      val amount                       = BigDecimal(10)

      val cmd = ProcessRecurringDirectDebitTransaction(
        monetaryAccountId = monetaryAccountId,
        transactionId = transactionId,
        transactionDate = transactionDate,
        amount = amount,
        currency = eur,
        payee = TransactionParty(iban = None, name = None)
      )

      val result = eventSourcedTestKit.runCommand(cmd)

      result.events.length shouldBe 2

      var recurringTransactionId: Option[types.RecurringTransactionId] = None

      inside(result.events.head) {
        case NewOutboundRecurringTransactionFound(rId, monId, startDate, endDate, frequency, source, a, _, payee) =>
          recurringTransactionId = Some(rId)
          monId shouldBe monetaryAccountId
          startDate shouldBe transactionDate
          endDate shouldBe None
          frequency shouldBe Period.ofMonths(1)
          source shouldBe DirectDebit
          a shouldBe amount
          payee shouldBe TransactionParty(iban = None, name = None)
      }

      inside(result.events(1)) {
        case NewRecurringTransactionInstanceFound(rId, txId, a, txDate) =>
          rId shouldBe recurringTransactionId.get
          txId shouldBe transactionId
          a shouldBe amount
          txDate shouldBe transactionDate
      }
    }

  }
}

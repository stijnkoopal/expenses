#!/bin/sh

export HOSTNAME=$(kubectl get pods -o json | jq '.items[] | select(.metadata.name | startswith("expenses")) | .status.podIP' | sed "s/\"//g")
echo "Using HOSTNAME=$HOSTNAME"

sbt --jvm-debug 5005

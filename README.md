# expenses


allocations
1. Make sure that we can label allocationCounterparty in FE (only admin)
2. Store transaction details for allocation on transactionfetched
    Fields for:
        - user assigned category
        - category based on counterpart list
        - category based on prediction
3. Find and implement mechanisms to show categories:
    - Per month spend on stuff
    - Per category graph over time
    - Optimal graphs
3. Allow replay of transactions
4. On transaction fetched, also start labelling
    - Always assign automated label (if available)
5. Allow user to label transaction in FE (overridding all auto assigned categories)
6. Start building of automated model based on the set of transaction labels (google ML tables)
7. On transaction fetched predict category if not yet done with other automated tool
8. Balances graphql should take joint account into consideration in FE
9. Improve logging in streams, consistent everywhere
10. Find a way to remove all the delays in streams. For example: Emit only when stuff is stored or something

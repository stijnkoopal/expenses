package serde

import java.nio.charset.Charset
import java.util.Currency

import play.api.libs.json.Json.JsValueWrapper
import play.api.libs.json._
import de.cbley.refined.play.json._

import scala.util.{Failure, Success}

object PlayJsonSerialization extends PlayJsonDefaultSerialization {
  private val UTF8 = Charset.forName("UTF-8")

  implicit def playSerialize[T: Writes]: Serializable[T] = Json.toJson(_).toString.getBytes(UTF8)
}

object PlayJsonDeserialization extends PlayJsonDefaultDeserialization {

  private def errorsToString(errors: scala.collection.Seq[(JsPath, scala.collection.Seq[JsonValidationError])]): String = {
    errors
      .map { case (path, errs) => s"in '$path': ${errs.mkString(", ")}" }
      .mkString(";")
  }

  implicit def playDeserialize[T: Reads]: Deserializable[T] =
    input =>
      Json.fromJson[T](Json.parse(input)) match {
        case JsSuccess(result, _) => Success(result)
        case JsError(errors)      => Failure(new RuntimeException(errorsToString(errors)))
      }
}

trait PlayJsonDefaultSerialization extends DefaultWrites {
  implicit val currencyWrites: Writes[Currency] =
    (currency: Currency) => JsString(currency.getCurrencyCode)

  implicit def customMapWrites[K, V: Writes](): Writes[Map[K, V]] =
    (map: Map[K, V]) =>
      Json.obj(map.map {
        case (key, value) =>
          val ret: (String, JsValueWrapper) = key.toString -> Json.toJson(value)
          ret
      }.toSeq: _*)
}

trait PlayJsonDefaultDeserialization extends DefaultReads {
  import de.cbley.refined.play.json._

  implicit val currencyReads: Reads[Currency] = {
    case JsString(x) => JsSuccess(Currency.getInstance(x))
    case _           => JsError("String expected")
  }

  implicit def customMapReads[K: Reads, V: Reads]: Reads[Map[K, V]] = {
    case JsObject(underlying) =>
      underlying
        .map { case (key, value) => Json.fromJson[K](Json.parse(key)) -> Json.fromJson[V](value) }
        .map {
          case (JsSuccess(k, _), JsSuccess(v, _)) => JsSuccess(k -> v)
          case _                                  => JsError("rttot")
        }
        .foldLeft[JsResult[Map[K, V]]](JsSuccess(Map[K, V]())) {
          case (old: JsResult[Map[K, V]], result) =>
            result match {
              case JsSuccess(x, _) if old.isSuccess => JsSuccess(old.get + x)
              case _                                => JsError("o no")
            }
        }

    case _ => JsError("obj expected")
  }
}

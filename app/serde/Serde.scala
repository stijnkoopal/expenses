package serde

import play.api.libs.json.DefaultReads

import scala.util.Try

trait Serializable[A] {
  def serialize(a: A): Array[Byte]
}

object Serializable {
  def apply[A](implicit sh: Serializable[A]): Serializable[A] = sh

  def serialize[A: Serializable](a: A): Array[Byte] = Serializable[A].serialize(a)

  implicit class SerializableOps[A: Serializable](a: A) {
    def serialize(): Array[Byte] = Serializable[A].serialize(a)
  }
}

trait Deserializable[A] {
  def deserialize(a: Array[Byte]): Try[A]
}

object Deserializable extends DefaultReads {
  def apply[A](implicit sh: Deserializable[A]): Deserializable[A] = sh

  def deserialize[A: Deserializable](payload: Array[Byte]): Try[A] = Deserializable[A].deserialize(payload)
}

package app

import javax.inject.Singleton
import play.api.mvc.{Action, AnyContent, InjectedController}

@Singleton
class CorsController extends InjectedController {
  def headers = List(
    "Access-Control-Allow-Origin"      -> "*",
    "Access-Control-Allow-Methods"     -> "GET, POST, OPTIONS, DELETE, PUT",
    "Access-Control-Max-Age"           -> "3600",
    "Access-Control-Allow-Headers"     -> "Origin, Content-Type, Accept, Authorization",
    "Access-Control-Allow-Credentials" -> "true"
  )

  def rootOptions: Action[AnyContent] = options("/")
  def options(url: String): Action[AnyContent] = Action { _ =>
    NoContent.withHeaders(headers: _*)
  }
}

package app

import javax.inject.Singleton
import play.api.mvc.{Action, AnyContent, InjectedController}

@Singleton
class HealthController extends InjectedController {
  def ready(): Action[AnyContent] = Action { _ =>
    Ok("OK")
  }

  def live(): Action[AnyContent] = Action { _ =>
    Ok("OK")
  }
}

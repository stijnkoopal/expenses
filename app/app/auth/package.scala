package app

import eu.timepit.refined.collection.NonEmpty
import pdi.jwt.JwtClaim
import types._
import eu.timepit.refined.{refineMV, refineV}

package object auth {
  sealed trait SecurityContext {
    type ContextUserId
    def userId: ContextUserId
  }

  case object NotAuthenticated extends SecurityContext {
    override type ContextUserId = None.type
    override def userId: None.type = None
  }

  case class Authenticated(
      jwt: JwtClaim,
      token: String
  ) extends SecurityContext {
    override type ContextUserId = UserId
    override def userId: ContextUserId = refineV[NonEmpty](jwt.subject.get) match {
      case Left(error) => ??? // TODO: now what
      case Right(value) => value
    }
  }
}

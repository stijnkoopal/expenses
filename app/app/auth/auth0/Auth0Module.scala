package app.auth.auth0

import app.auth.AuthEnforcedAction
import play.api.inject.{Binding, Module}
import play.api.{Configuration, Environment}

class Auth0Module extends Module {
  override def bindings(environment: Environment, configuration: Configuration): Seq[Binding[_]] = Seq(
    bind[AuthEnforcedAction].toSelf,
    bind[AuthService].to[Auth0Service]
  )
}

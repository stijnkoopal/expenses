package app
package auth

import app.auth.auth0.AuthService
import javax.inject.Inject
import play.api.http.HeaderNames
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

case class SecuredRequest[A](
    securityContext: Authenticated,
    request: Request[A]
) extends WrappedRequest[A](request) {
  val userId: types.UserId = securityContext.userId
}

class AuthEnforcedAction @Inject() (
    bodyParser: BodyParsers.Default,
    authService: AuthService
)(implicit ec: ExecutionContext)
    extends ActionBuilder[SecuredRequest, AnyContent] {
  override def parser: BodyParser[AnyContent]               = bodyParser
  override protected def executionContext: ExecutionContext = ec

  private val headerTokenRegex = """Bearer (.+?)""".r

  override def invokeBlock[A](request: Request[A], block: SecuredRequest[A] => Future[Result]): Future[Result] =
    extractBearerToken(request) map { token =>
      authService.validateJwt(token) match {
        case Success(claim) => block(SecuredRequest(Authenticated(claim, token), request))
        case Failure(t)     => Future.successful(Results.Unauthorized(t.getMessage))
      }
    } getOrElse Future.successful(Results.Unauthorized)

  private def extractBearerToken[A](request: Request[A]): Option[String] =
    request.headers.get(HeaderNames.AUTHORIZATION) collect {
      case headerTokenRegex(token) => token
    }
}

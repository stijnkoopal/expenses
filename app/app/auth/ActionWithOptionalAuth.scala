package app
package auth

import app.auth.auth0.AuthService
import javax.inject.Inject
import play.api.http.HeaderNames
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

case class OptionalSecuredRequest[A](
    securityContext: SecurityContext,
    request: Request[A]
) extends WrappedRequest[A](request) {
  val userId: Option[types.UserId] = securityContext match {
    case auth @ Authenticated(_, _) => Some(auth.userId)
    case _                          => None
  }
}

class ActionWithOptionalAuth @Inject() (
    bodyParser: BodyParsers.Default,
    authService: AuthService
)(implicit ec: ExecutionContext)
    extends ActionBuilder[OptionalSecuredRequest, AnyContent] {
  override def parser: BodyParser[AnyContent]               = bodyParser
  override protected def executionContext: ExecutionContext = ec

  private val headerTokenRegex = """Bearer (.+?)""".r

  override def invokeBlock[A](request: Request[A], block: OptionalSecuredRequest[A] => Future[Result]): Future[Result] =
    extractBearerToken(request) map { token =>
      authService.validateJwt(token) match {
        case Success(claim) => block(OptionalSecuredRequest(Authenticated(claim, token), request))
        case Failure(_)     => block(OptionalSecuredRequest(NotAuthenticated, request))
      }
    } getOrElse block(OptionalSecuredRequest(NotAuthenticated, request))

  private def extractBearerToken[A](request: Request[A]): Option[String] =
    request.headers.get(HeaderNames.AUTHORIZATION) collect {
      case headerTokenRegex(token) => token
    }
}

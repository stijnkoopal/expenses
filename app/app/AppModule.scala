package app

import java.time.Clock

import play.api.inject.{Binding, Module}
import play.api.{Configuration, Environment}

class AppModule extends Module {
  override def bindings(environment: Environment, configuration: Configuration): Seq[Binding[_]] = Seq(
    bind[Clock].toInstance(Clock.systemUTC()),
    bind[AkkaManagementStart].toSelf.eagerly()
  )
}

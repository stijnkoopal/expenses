package app

import akka.actor.ActorSystem
import akka.management.cluster.bootstrap.ClusterBootstrap
import akka.management.scaladsl.AkkaManagement
import javax.inject.{Inject, Singleton}

@Singleton
class AkkaManagementStart @Inject() (system: ActorSystem) {
  System.out.println(s"env..HOSTNAME set to '${sys.env.get("HOSTNAME")}'")

  AkkaManagement(system).start()
  ClusterBootstrap(system).start()
}

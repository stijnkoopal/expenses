package modules.recurring.domain

import java.time.temporal.ChronoUnit
import java.time.{Instant, Period, ZoneId, ZonedDateTime}
import java.util.Currency

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.{Behaviors, TimerScheduler}
import akka.cluster.sharding.typed.scaladsl.EntityTypeKey
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.{Effect, EventSourcedBehavior}
import com.fasterxml.jackson.core.{JsonGenerator, JsonParser}
import com.fasterxml.jackson.databind.annotation.{JsonDeserialize, JsonSerialize}
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.fasterxml.jackson.databind.{DeserializationContext, SerializerProvider}
import eu.timepit.refined.types.string.NonEmptyString
import modules.core.domain.IdUtil
import modules.recurring.domain.RecurringTransaction.Command
import modules.recurring.services.RecurringTransactionEntityRepository
import play.api.Logging
import serde.{CommandSerializable, EventSerializable, SnapshotSerializable}
import types.MonetaryAccountId

import scala.concurrent.duration._
import scala.language.postfixOps

object RecurringTransaction {
  val defaultFrequency: Period = Period.ofMonths(1)

  val TypeKey: EntityTypeKey[Command] =
    EntityTypeKey[Command]("RecurringTransaction")

  def apply(entityId: MonetaryAccountId, entityRepository: RecurringTransactionEntityRepository): Behavior[Command] = {
    val persistenceId = PersistenceId(entityTypeHint = TypeKey.name, entityId = entityId.toString)
    Behaviors.withTimers(scheduler =>
      Behaviors.setup { _ =>
        new RecurringTransaction(persistenceId, entityId, scheduler, entityRepository).start()
    })
  }

  case object CheckStatusTimer

  trait Command           extends CommandSerializable
  case object CheckStatus extends Command

  case class ProcessRecurringSchedule(
      monetaryAccountId: types.MonetaryAccountId,
      frequency: Period,
      startDate: Instant,
      endDate: Option[Instant],
      amount: BigDecimal,
      currency: Currency,
      direction: types.TransactionDirection,
      otherSide: TransactionParty
  ) extends Command

  case class ProcessRecurringDirectDebitTransaction(
      monetaryAccountId: types.MonetaryAccountId,
      transactionId: types.TransactionId,
      transactionDate: Instant,
      amount: BigDecimal,
      currency: Currency,
      payee: TransactionParty
  ) extends Command

  case class ProcessTransactionWithSchedule(
      transactionId: types.TransactionId,
      amount: BigDecimal,
      transactionDate: Instant,
      payee: TransactionParty,
      payer: TransactionParty,
      description: String
  ) extends Command

  case class LinkToRecurringTransactionOnOtherSide(
      recurringTransactionId: types.TransactionId,
      recurringTransactionIdOnOtherSide: types.TransactionId,
      payee: types.MonetaryAccountId
  ) extends Command

  trait Event extends EventSerializable

  sealed trait NewRecurringTransactionFound extends Event
  case class NewInboundRecurringTransactionFound(
      id: types.RecurringTransactionId,
      monetaryAccountId: MonetaryAccountId,
      startDate: Instant,
      endDate: Option[Instant],
      frequency: Period,
      source: Source,
      amount: BigDecimal,
      currency: Currency,
      payer: TransactionParty
  ) extends NewRecurringTransactionFound

  case class NewOutboundRecurringTransactionFound(
      id: types.RecurringTransactionId,
      monetaryAccountId: MonetaryAccountId,
      startDate: Instant,
      endDate: Option[Instant],
      frequency: Period,
      source: Source,
      amount: BigDecimal,
      currency: Currency,
      payee: TransactionParty
  ) extends NewRecurringTransactionFound

  case class RecurringTransactionMonetaryAccountChanged(
      id: types.RecurringTransactionId,
      newMonetaryAccount: types.MonetaryAccountId
  ) extends Event

  case class RecurringTransactionAmountChanged(
      id: types.RecurringTransactionId,
      newAmount: BigDecimal
  ) extends Event

  case class RecurringTransactionFrequencyChanged(
      id: types.RecurringTransactionId,
      newFrequency: Period
  ) extends Event

  case class RecurringTransactionEnded(
      id: types.RecurringTransactionId,
      endDate: Instant
  ) extends Event

  case class RecurringTransactionStartDateChanged(
      id: types.RecurringTransactionId,
      newStartDate: Instant
  ) extends Event

  case class RecurringTransactionReopened(
      id: types.RecurringTransactionId,
      newEndDate: Option[Instant]
  ) extends Event

  case class NewRecurringTransactionInstanceFound(
      recurringTransactionId: types.RecurringTransactionId,
      transactionId: types.TransactionId,
      amount: BigDecimal,
      transactionDate: Instant
  ) extends Event

  case class LinkedToTransactionOnOtherSide(
      recurringTransactionId: types.RecurringTransactionId,
      recurringTransactionIdOnOtherSide: types.RecurringTransactionId,
      payee: MonetaryAccountId
  ) extends Event

  case class TransactionParty(
      iban: Option[types.IbanString],
      name: Option[NonEmptyString]
  )

  case class StateData(
      source: Source,
      startDate: Instant,
      endDate: Option[Instant],
      frequency: Period,
      amount: BigDecimal,
      currency: Currency,
      monetaryAccountId: types.MonetaryAccountId,
      direction: types.TransactionDirection,
      status: Status
  )

  case class LinkToOtherSide(
      monetaryAccountId: MonetaryAccountId,
      recurringTransactionId: types.RecurringTransactionId
  )
  case class Transaction(
      id: types.TransactionId,
      transactionDate: Instant
  )
  case class State(
      id: types.RecurringTransactionId,
      data: Option[StateData],
      linkedToOtherSide: Option[LinkToOtherSide],
      transactions: Seq[Transaction]
  ) extends SnapshotSerializable
  object State {
    def empty(id: types.RecurringTransactionId): State = State(
      id = id,
      data = None,
      linkedToOtherSide = None,
      transactions = Seq()
    )
  }

  @JsonSerialize(using = classOf[SourceJsonSerialization])
  @JsonDeserialize(using = classOf[SourceJsonDeserializer])
  sealed trait Source
  object Source {
    case object DirectDebit extends Source
    case object Schedule    extends Source
  }

  @JsonSerialize(using = classOf[StatusJsonSerialization])
  @JsonDeserialize(using = classOf[StatusJsonDeserializer])
  sealed trait Status
  object Status {
    case object Active extends Status
    case object Ended  extends Status
  }

  // TODO remove these
  class StatusJsonSerialization extends StdSerializer[Status](classOf[Status]) {
    override def serialize(value: Status, gen: JsonGenerator, provider: SerializerProvider): Unit = {
      val strValue = value match {
        case Status.Active => "ACTIVE"
        case Status.Ended  => "ENDED"
      }
      gen.writeString(strValue)
    }
  }

  class StatusJsonDeserializer extends StdDeserializer[Status](classOf[Status]) {
    override def deserialize(p: JsonParser, ctxt: DeserializationContext): Status = {
      p.getText match {
        case "ACTIVE" => Status.Active
        case "ENDED"  => Status.Ended
      }
    }
  }

  // TODO remove these
  class SourceJsonSerialization extends StdSerializer[Source](classOf[Source]) {
    override def serialize(value: Source, gen: JsonGenerator, provider: SerializerProvider): Unit = {
      val strValue = value match {
        case Source.DirectDebit => "DIRECT_DEBIT"
        case Source.Schedule    => "SCHEDULE"
      }
      gen.writeString(strValue)
    }
  }

  class SourceJsonDeserializer extends StdDeserializer[Source](classOf[Source]) {
    override def deserialize(p: JsonParser, ctxt: DeserializationContext): Source = {
      p.getText match {
        case "DIRECT_DEBIT" => Source.DirectDebit
        case "SCHEDULE"     => Source.Schedule
      }
    }
  }
}

class RecurringTransaction(persistenceId: PersistenceId,
                           entityId: types.RecurringTransactionId,
                           timers: TimerScheduler[Command],
                           entityRepository: RecurringTransactionEntityRepository)
    extends Logging {
  import RecurringTransaction._

  def start(): Behavior[Command] =
    EventSourcedBehavior(persistenceId, emptyState = State.empty(entityId), commandHandler, eventHandler)

  val commandHandler: (State, Command) => Effect[Event, State] = { (state, cmd) =>
    cmd match {
      case cmd: ProcessRecurringSchedule               => processCommand(state, cmd)
      case cmd: ProcessRecurringDirectDebitTransaction => processCommand(state, cmd)
      case cmd: ProcessTransactionWithSchedule         => processCommand(state, cmd)
      case cmd: LinkToRecurringTransactionOnOtherSide  => processCommand(state, cmd)
      case _: CheckStatus.type                         => checkStatus(state)
    }
  }

  private val eventHandler: (State, Event) => State = { (state, evt) =>
    evt match {
      case evt: NewInboundRecurringTransactionFound        => processEvent(state, evt)
      case evt: NewOutboundRecurringTransactionFound       => processEvent(state, evt)
      case evt: RecurringTransactionAmountChanged          => processEvent(state, evt)
      case evt: RecurringTransactionFrequencyChanged       => processEvent(state, evt)
      case evt: RecurringTransactionStartDateChanged       => processEvent(state, evt)
      case evt: RecurringTransactionMonetaryAccountChanged => processEvent(state, evt)
      case evt: RecurringTransactionEnded                  => processEvent(state, evt)
      case evt: RecurringTransactionReopened               => processEvent(state, evt)
      case evt: NewRecurringTransactionInstanceFound       => processEvent(state, evt)
      case evt: LinkedToTransactionOnOtherSide             => processEvent(state, evt)
    }
  }

  private def now(): ZonedDateTime = ZonedDateTime.now() // TODO: clock

  def checkStatus(state: State): Effect[Event, State] = {
    val endedEvent = state.data match {
      case Some(data) if data.status != Status.Ended =>
        val expiredByEndDate = data.endDate.exists(endDate => endDate.isBefore(now().toInstant))
        val expiredByLastTransactionAfterFrequency =
          state.transactions.lastOption.exists(lastTx =>
            lastTx.transactionDate.atZone(ZoneId.of("UTC")).plus(data.frequency).isBefore(now()))

        if (expiredByEndDate || expiredByLastTransactionAfterFrequency) {
          val expiryDate = state.transactions.lastOption.map(_.transactionDate).getOrElse(now().toInstant)
          Some(RecurringTransactionEnded(id = state.id, endDate = expiryDate))
        } else {
          None
        }
      case _ => None
    }

    val reopenedEvent = state.data match {
      case Some(data) if data.status == Status.Ended =>
        if (state.transactions.lastOption.exists(lastTx => lastTx.transactionDate.isAfter(data.endDate.getOrElse(now().toInstant)))) {
          Some(RecurringTransactionReopened(id = state.id, newEndDate = None))
        } else {
          None
        }
      case _ => None
    }

    Effect.persist(endedEvent.toSeq ++ reopenedEvent.toSeq).thenNoReply()
  }

  def processCommand(state: State, cmd: ProcessRecurringSchedule): Effect[Event, State] = {
    if (state.data.isEmpty) {
      val event = cmd.direction match {
        case types.TransactionDirection.Inbound =>
          NewInboundRecurringTransactionFound(
            id = state.id,
            frequency = cmd.frequency,
            startDate = cmd.startDate,
            endDate = cmd.endDate,
            monetaryAccountId = cmd.monetaryAccountId,
            amount = cmd.amount,
            currency = cmd.currency,
            payer = cmd.otherSide,
            source = Source.Schedule
          )

        case types.TransactionDirection.Outbound =>
          NewOutboundRecurringTransactionFound(
            id = state.id,
            frequency = cmd.frequency,
            startDate = cmd.startDate,
            endDate = cmd.endDate,
            monetaryAccountId = cmd.monetaryAccountId,
            amount = cmd.amount,
            currency = cmd.currency,
            payee = cmd.otherSide,
            source = Source.Schedule
          )
      }
      return Effect.persist(event).thenNoReply()
    }

    val stateData = state.data.get
    if (stateData.source != Source.Schedule) {
      return Effect.none.thenNoReply()
    }

    val amountChangedEvent =
      if (stateData.amount != cmd.amount) Some(RecurringTransactionAmountChanged(id = state.id, newAmount = cmd.amount)) else None

    val frequencyChangedEvent =
      if (stateData.frequency != cmd.frequency) Some(RecurringTransactionFrequencyChanged(id = state.id, newFrequency = cmd.frequency))
      else None

    val monetaryAccountChangedEvent =
      if (stateData.monetaryAccountId != cmd.monetaryAccountId)
        Some(RecurringTransactionMonetaryAccountChanged(id = state.id, newMonetaryAccount = cmd.monetaryAccountId))
      else None

    val statusChangedEvent =
      if (stateData.status == Status.Ended) Some(RecurringTransactionReopened(id = state.id, newEndDate = cmd.endDate)) else None // TODO: check for enddate on command

    // TODO end date

    // TODO: run status changes timer

    Effect
      .persist(amountChangedEvent.toSeq ++ frequencyChangedEvent.toSeq ++ statusChangedEvent.toSeq ++ monetaryAccountChangedEvent.toSeq)
      .thenRun((state: State) => {
        state.linkedToOtherSide match {
          case Some(link) =>
            val command = cmd.copy(
              monetaryAccountId = link.monetaryAccountId,
              amount = cmd.amount * -1,
              direction = types.TransactionDirection.Inbound,
              otherSide = TransactionParty(
                iban = None,
                name = None
              )
            )

            entityRepository.send(link.recurringTransactionId, command)

          case _ => ()
        }
      })
      .thenNoReply()
  }

  def processCommand(state: State, cmd: ProcessRecurringDirectDebitTransaction): Effect[Event, State] = {
    if (state.data.exists(data => data.source != Source.DirectDebit || data.direction != types.TransactionDirection.Outbound)) {
      return Effect.none
    }

    val newEvent = state.data match {
      case Some(_) => None
      case None =>
        Some(
          NewOutboundRecurringTransactionFound(
            id = state.id,
            monetaryAccountId = cmd.monetaryAccountId,
            startDate = cmd.transactionDate,
            endDate = None,
            frequency = defaultFrequency,
            source = Source.DirectDebit,
            amount = cmd.amount,
            currency = cmd.currency,
            payee = cmd.payee
          )
        )
    }

    val instanceEvent =
      if (state.transactions.exists(tx => tx.id == cmd.transactionId)) None
      else
        Some(
          NewRecurringTransactionInstanceFound(
            recurringTransactionId = state.id,
            transactionId = cmd.transactionId,
            amount = cmd.amount,
            transactionDate = cmd.transactionDate
          )
        )

    val amountChangedEvent = state.data.map(_.amount) match {
      case Some(amount) if amount != cmd.amount => Some(RecurringTransactionAmountChanged(id = state.id, newAmount = cmd.amount))
      case _                                    => None
    }

    val monetaryAccountChangedEvent = state.data.map(_.monetaryAccountId) match {
      case Some(monetaryAccountId) if monetaryAccountId != cmd.monetaryAccountId =>
        Some(
          RecurringTransactionMonetaryAccountChanged(
            id = state.id,
            newMonetaryAccount = cmd.monetaryAccountId
          )
        )

      case _ => None
    }

    val startDateChangedEvent = state.data.map(_.startDate) match {
      case Some(startDate) if cmd.transactionDate.isBefore(startDate) =>
        Some(RecurringTransactionStartDateChanged(id = state.id, newStartDate = cmd.transactionDate))
      case _ => None
    }

    val statusChangedEvent = state.data.flatMap(_.endDate) match {
      case Some(endDate) if cmd.transactionDate.isAfter(endDate) =>
        Some(RecurringTransactionReopened(id = state.id, newEndDate = None))
      case _ => None
    }

    timers.cancel(CheckStatusTimer)

    val lastTxDate = (state.transactions.map(_.transactionDate) :+ cmd.transactionDate).max
    if (lastTxDate.isAfter(now().toInstant)) {
      val currentFrequency = state.data.map(_.frequency).getOrElse(defaultFrequency)
      val daysInFrequency  = ChronoUnit.DAYS.between(now(), lastTxDate.plus(currentFrequency))

      timers.startSingleTimer(CheckStatusTimer, CheckStatus, FiniteDuration(daysInFrequency, DAYS))
    } else {
      timers.startSingleTimer(CheckStatusTimer, CheckStatus, FiniteDuration(1, MINUTES))
    }

    // TODO: frequency changes,

    Effect
      .persist(
        newEvent.toSeq ++ instanceEvent.toSeq ++ amountChangedEvent.toSeq ++ monetaryAccountChangedEvent.toSeq ++ statusChangedEvent.toSeq ++ startDateChangedEvent.toSeq
      )
      .thenNoReply()
  }

  def processCommand(state: State, cmd: ProcessTransactionWithSchedule): Effect[Event, State] = {
    val event = NewRecurringTransactionInstanceFound(
      recurringTransactionId = state.id,
      amount = cmd.amount,
      transactionId = cmd.transactionId,
      transactionDate = cmd.transactionDate
    )

    Effect
      .persist(event)
      .thenRun((state: State) => {
        state.linkedToOtherSide match {
          case Some(link) =>
            val newCommand = cmd.copy(
              transactionId = IdUtil.calculateTransactionId(
                payeeIban = cmd.payee.iban,
                payerIban = cmd.payer.iban,
                amount = cmd.amount * -1,
                description = cmd.description,
                transactionDate = cmd.transactionDate
              ),
              amount = cmd.amount * -1,
            )

            entityRepository.send(link.recurringTransactionId, newCommand)

          case _ => ()
        }
      })
      .thenNoReply()
  }

  def processCommand(state: State, cmd: LinkToRecurringTransactionOnOtherSide): Effect[Event, State] = {
    val event = state.linkedToOtherSide match {
      case Some(link) if link.monetaryAccountId != cmd.payee =>
        // Todo: also the schedule on other side ends here. So send msg to other actor
        Some(
          LinkedToTransactionOnOtherSide(
            recurringTransactionId = cmd.recurringTransactionId,
            recurringTransactionIdOnOtherSide = cmd.recurringTransactionIdOnOtherSide,
            payee = cmd.payee
          )
        )

      case None =>
        Some(
          LinkedToTransactionOnOtherSide(
            recurringTransactionId = cmd.recurringTransactionId,
            recurringTransactionIdOnOtherSide = cmd.recurringTransactionIdOnOtherSide,
            payee = cmd.payee
          )
        )

      case _ => None
    }

    if (event.isDefined) {
      Effect.persist(event.get).thenNoReply()
    } else {
      Effect.none.thenNoReply()
    }
  }

  def processEvent(state: State, evt: NewInboundRecurringTransactionFound): State = {
    val newData = StateData(
      source = evt.source,
      startDate = evt.startDate,
      endDate = evt.endDate,
      frequency = evt.frequency,
      amount = evt.amount,
      currency = evt.currency,
      monetaryAccountId = evt.monetaryAccountId,
      direction = types.TransactionDirection.Inbound,
      status = Status.Active
    )
    state.copy(data = Some(newData))
  }

  def processEvent(state: State, evt: NewOutboundRecurringTransactionFound): State = {
    val newData = StateData(
      source = evt.source,
      startDate = evt.startDate,
      endDate = evt.endDate,
      frequency = evt.frequency,
      amount = evt.amount,
      currency = evt.currency,
      monetaryAccountId = evt.monetaryAccountId,
      direction = types.TransactionDirection.Outbound,
      status = Status.Active
    )
    state.copy(data = Some(newData))
  }

  def processEvent(state: State, evt: RecurringTransactionMonetaryAccountChanged): State = {
    val newData = state.data.map(_.copy(monetaryAccountId = evt.newMonetaryAccount))
    state.copy(data = newData)
  }

  def processEvent(state: State, evt: RecurringTransactionAmountChanged): State = {
    val newData = state.data.map(_.copy(amount = evt.newAmount))
    state.copy(data = newData)
  }

  def processEvent(state: State, evt: RecurringTransactionFrequencyChanged): State = {
    val newData = state.data.map(_.copy(frequency = evt.newFrequency))
    state.copy(data = newData)
  }

  def processEvent(state: State, evt: RecurringTransactionStartDateChanged): State = {
    val newData = state.data.map(_.copy(startDate = evt.newStartDate))
    state.copy(data = newData)
  }

  def processEvent(state: State, evt: RecurringTransactionEnded): State = {
    val newData = state.data.map(_.copy(status = Status.Ended, endDate = Some(evt.endDate)))
    state.copy(data = newData)
  }

  def processEvent(state: State, evt: RecurringTransactionReopened): State = {
    val newData = state.data.map(_.copy(status = Status.Active, endDate = evt.newEndDate))
    state.copy(data = newData)
  }

  def processEvent(state: State, evt: NewRecurringTransactionInstanceFound): State = {
    val transactions =
      (state.transactions :+ Transaction(id = evt.transactionId, transactionDate = evt.transactionDate)).sortBy(_.transactionDate)

    state.copy(transactions = transactions)
  }

  def processEvent(state: State, evt: LinkedToTransactionOnOtherSide): State = {
    state.copy(
      linkedToOtherSide = Some(
        LinkToOtherSide(
          monetaryAccountId = evt.payee,
          recurringTransactionId = evt.recurringTransactionIdOnOtherSide
        )
      )
    )
  }
}

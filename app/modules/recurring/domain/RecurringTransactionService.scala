package modules.recurring.domain

import modules.core.messaging.{NewInboundTransactionFoundMsg, NewOutboundTransactionFoundMsg}
import modules.recurring.messaging.{RecurringDirectDebitTransactionDocument, RecurringScheduleDocument}

trait RecurringTransactionService {
  def processDirectDebitTransactionDocument(document: RecurringDirectDebitTransactionDocument): Unit
  def processRecurringScheduleDocument(document: RecurringScheduleDocument): Unit
  def processOutboundTransaction(event: NewOutboundTransactionFoundMsg): Unit
  def processInboundTransaction(event: NewInboundTransactionFoundMsg): Unit
}

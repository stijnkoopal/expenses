package modules.recurring.services

import akka.stream.Materializer
import javax.inject.{Inject, Singleton}
import modules.core.domain.IdUtil
import modules.core.messaging.{NewInboundTransactionFoundMsg, NewOutboundTransactionFoundMsg}
import modules.core.projections.{MonetaryAccountRepository, MonetaryAccountRow}
import modules.recurring.domain.RecurringTransaction.TransactionParty
import modules.recurring.domain.{RecurringTransaction, RecurringTransactionService}
import modules.recurring.messaging.{RecurringDirectDebitTransactionDocument, RecurringScheduleDocument}
import streaming.AkkaStreamImplicits._

// TODO: do not depend on monetaryAccountRepository from core here. Build up own repo in recurring (based on NewMonetaryAccountFoundMsg)
// Then, also update the recurring transaction if an account is added
@Singleton
private[recurring] class AkkaBackedRecurringTransactionService @Inject()(
    monetaryAccountRepository: MonetaryAccountRepository,
    recurringTransactionEntityRepository: RecurringTransactionEntityRepository
)(implicit mat: Materializer)
    extends RecurringTransactionService {

  override def processDirectDebitTransactionDocument(document: RecurringDirectDebitTransactionDocument): Unit = {
    val command = RecurringTransaction.ProcessRecurringDirectDebitTransaction(
      monetaryAccountId = document.monetaryAccountId,
      transactionId = document.id,
      transactionDate = document.responded,
      amount = document.amount,
      currency = document.currency,
      payee = RecurringTransaction.TransactionParty(iban = document.payee.iban, name = document.payee.name)
    )
    sendCommandTo(command)(document.recurringTransactionId)
  }

  override def processRecurringScheduleDocument(document: RecurringScheduleDocument): Unit = {
    val processScheduleCommand = RecurringTransaction.ProcessRecurringSchedule(
      monetaryAccountId = document.monetaryAccountId,
      frequency = document.frequency,
      startDate = document.startDate,
      endDate = document.endDate,
      amount = document.amount,
      currency = document.currency,
      otherSide = TransactionParty(
        iban = document.payee.iban,
        name = document.payee.name
      ),
      direction = types.TransactionDirection.Outbound
    )

    sendCommandTo(processScheduleCommand)(document.id)

    if (document.payee.iban.isDefined) {
      monetaryAccountRepository
        .fetchAllWithSimilarUser(document.monetaryAccountId)
        .grouped(Int.MaxValue)
        .map(_.collectFirst {
          case MonetaryAccountRow(id, _, Some(iban), _, _, _, _, _, _, _) if iban.value == document.payee.iban.get.value =>
            id
        })
        .collect {
          case Some(id) => id
        }
        .runForeach(payeeMonetaryAccountId => {
          val scheduleOnOtherSide = processScheduleCommand.copy(
            monetaryAccountId = payeeMonetaryAccountId,
            amount = processScheduleCommand.amount * -1,
            direction = types.TransactionDirection.Inbound,
            otherSide = TransactionParty(
              iban = document.payer.iban,
              name = document.payer.name
            )
          )

          val idOfScheduleOnOtherSide = IdUtil.calculateRecurringScheduleId(
            institution = document.institution,
            monetaryAccountId = payeeMonetaryAccountId,
            amount = document.amount * -1,
            payeeIban = None,
            description = document.description
          )

          sendCommandTo(scheduleOnOtherSide)(idOfScheduleOnOtherSide)

          val command = RecurringTransaction.LinkToRecurringTransactionOnOtherSide(
            recurringTransactionId = document.id,
            recurringTransactionIdOnOtherSide = idOfScheduleOnOtherSide,
            payee = payeeMonetaryAccountId
          )
          sendCommandTo(command)(document.id)
        })
    }
  }

  override def processOutboundTransaction(event: NewOutboundTransactionFoundMsg): Unit = {
    event.partOfScheduleId.foreach(scheduleId => {
      val command = RecurringTransaction.ProcessTransactionWithSchedule(
        transactionId = event.id,
        amount = event.amount,
        transactionDate = event.transactionDate,
        description = event.description,
        payee = TransactionParty(
          iban = event.payee.iban,
          name = event.payee.name
        ),
        payer = TransactionParty(
          iban = event.payer.iban,
          name = event.payer.name
        )
      )
      sendCommandTo(command)(scheduleId)
    })
  }

  override def processInboundTransaction(event: NewInboundTransactionFoundMsg): Unit = {
    // TODO
  }

  private def sendCommandTo(command: RecurringTransaction.Command)(recurringTransactionId: types.RecurringTransactionId): Unit =
    recurringTransactionEntityRepository.send(recurringTransactionId, command)
}

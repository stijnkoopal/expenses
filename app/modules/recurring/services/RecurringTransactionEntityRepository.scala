package modules.recurring.services

import akka.actor.typed.scaladsl.Behaviors
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity}
import akka.util.Timeout
import eu.timepit.refined.refineV
import eu.timepit.refined.string.Uuid
import javax.inject.{Inject, Singleton}
import modules.recurring.domain.RecurringTransaction
import play.api.Logging
import types.RecurringTransactionId

import scala.concurrent.duration.DurationInt

trait RecurringTransactionEntityRepository {
  def send(id: RecurringTransactionId, command: RecurringTransaction.Command): Unit
}

@Singleton
class ShardedRecurringTransactionEntityRepository @Inject()(sharding: ClusterSharding)
    extends RecurringTransactionEntityRepository
    with Logging {
  private implicit val askTimeout: Timeout = Timeout(5.seconds)

  sharding.init(Entity(typeKey = RecurringTransaction.TypeKey) { entityContext =>
    {
      refineV[Uuid](entityContext.entityId) match {
        case Left(error) =>
          logger.warn(s"Could not refine ${entityContext.entityId} to RecurringTransactionId, not starting entity")
          logger.warn(error)
          Behaviors.empty

        case Right(recurringTransactionId) =>
          RecurringTransaction(entityId = recurringTransactionId, entityRepository = this)
      }
    }
  })

  override def send(id: RecurringTransactionId, command: RecurringTransaction.Command): Unit = {
    val entityRef = sharding.entityRefFor(RecurringTransaction.TypeKey, id.toString)
    entityRef ! command
  }
}

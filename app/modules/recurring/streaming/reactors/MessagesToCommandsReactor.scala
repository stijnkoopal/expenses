package modules.recurring.streaming.reactors

import akka.Done
import akka.stream.scaladsl.{Keep, Sink}
import akka.stream.{Materializer, SharedKillSwitch}
import javax.inject.{Inject, Singleton}
import messaging.MessageReceiver
import modules.core.messaging.{NewInboundTransactionFoundMsg, NewOutboundTransactionFoundMsg, TransactionMessaging}
import modules.recurring.domain.RecurringTransactionService
import modules.recurring.messaging.{RecurringDirectDebitTransactionDocument, RecurringMessaging, RecurringScheduleDocument}
import play.api.Logging
import streaming.AkkaStreamImplicits._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
private[recurring] class MessagesToCommandsReactor @Inject()(
    receiver: MessageReceiver,
    recurringTransactionService: RecurringTransactionService
)(
    implicit mat: Materializer,
    ec: ExecutionContext
) extends Logging {

  import modules.core.messaging.TransactionSerde._
  import modules.recurring.messaging.RecurringTransactionSerde._
  import serde.PlayJsonDeserialization._

  logger.info(s"Starting to listen for 'RecurringScheduleDocument' messages on '${RecurringMessaging.channel}'")
  private val recurringScheduleDocumentSource = receiver
    .receive[RecurringScheduleDocument](channel = RecurringMessaging.channel, payloadType = RecurringScheduleDocument.payloadType)

  logger.info(s"Starting to listen for 'RecurringDirectDebitTransactionDocument' messages on '${RecurringMessaging.channel}'")
  private val recurringDirectDebitDocumentSource = receiver
    .receive[RecurringDirectDebitTransactionDocument](
      channel = RecurringMessaging.channel,
      payloadType = RecurringDirectDebitTransactionDocument.payloadType
    )

  logger.info(s"Starting to listen for 'NewOutboundTransactionFoundMsg' messages on '${TransactionMessaging.channel}'")
  private val outboundTransactionsSource = receiver.receive[NewOutboundTransactionFoundMsg](
    channel = TransactionMessaging.channel,
    payloadType = NewOutboundTransactionFoundMsg.payloadType
  )

  logger.info(s"Starting to listen for 'NewInboundTransactionFoundMsg' messages on '${TransactionMessaging.channel}'")
  private val inboundTransactionsSource = receiver.receive[NewInboundTransactionFoundMsg](
    channel = TransactionMessaging.channel,
    payloadType = NewInboundTransactionFoundMsg.payloadType
  )

  def start(killSwitch: SharedKillSwitch): Future[Done] = {
    val scheduleFuture = recurringScheduleDocumentSource
      .alsoTo(Sink.foreach(document => recurringTransactionService.processRecurringScheduleDocument(document)))
      .via(killSwitch.flow)
      .toMat(Sink.ignore)(Keep.right)
      .run()

    val directDebitFuture = recurringDirectDebitDocumentSource
      .alsoTo(Sink.foreach(document => recurringTransactionService.processDirectDebitTransactionDocument(document)))
      .via(killSwitch.flow)
      .toMat(Sink.ignore)(Keep.right)
      .run()

    val outboundTransactionsFuture = outboundTransactionsSource
      .alsoTo(Sink.foreach(msg => recurringTransactionService.processOutboundTransaction(msg)))
      .via(killSwitch.flow)
      .toMat(Sink.ignore)(Keep.right)
      .run()

    val inboundTransactionsFuture = inboundTransactionsSource
      .alsoTo(Sink.foreach(msg => recurringTransactionService.processInboundTransaction(msg)))
      .via(killSwitch.flow)
      .toMat(Sink.ignore)(Keep.right)
      .run()

    Future
      .sequence(
        Seq(
          inboundTransactionsFuture,
          outboundTransactionsFuture,
          scheduleFuture,
          directDebitFuture
        ))
      .map(_ => Done)
  }
}

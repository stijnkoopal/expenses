package modules.recurring.streaming.reactors

import akka._
import akka.actor.ActorSystem
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity}
import akka.persistence.jdbc.query.scaladsl.JdbcReadJournal
import akka.persistence.query.{EventEnvelope, PersistenceQuery}
import akka.persistence.typed.PersistenceId
import akka.stream.scaladsl.{Keep, Sink, Source}
import akka.stream.{Materializer, SharedKillSwitch}
import akka.util.Timeout
import javax.inject.{Inject, Singleton}
import messaging.MessagePublisher
import modules.core.streaming.reactors.SequenceNumberStore.{GetSequenceNumber, GetSequenceNumberResponse, UpdateSequenceNumber}
import modules.core.streaming.reactors.SequenceNumberStore
import modules.recurring.domain.RecurringTransaction
import modules.recurring.domain.RecurringTransaction._
import modules.recurring.messaging._
import play.api.Logging
import streaming.AkkaStreamImplicits._

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps

@Singleton()
private[recurring] class PublishEventsReactor @Inject()(
    system: ActorSystem,
    publisher: MessagePublisher,
    sharding: ClusterSharding
)(implicit mat: Materializer)
    extends Logging {
  import modules.recurring.messaging.RecurringTransactionSerde._
  import serde.PlayJsonSerialization._

  implicit val timeout: Timeout = 300 seconds

  sharding.init(Entity(typeKey = SequenceNumberStore.TypeKey) { entityContext =>
    SequenceNumberStore(
      entityContext.entityId,
      PersistenceId(entityTypeHint = entityContext.entityTypeKey.name, entityId = entityContext.entityId)
    )
  })

  val readJournal: JdbcReadJournal = PersistenceQuery(system).readJournalFor[JdbcReadJournal](JdbcReadJournal.Identifier)

  val source: Source[Any, NotUsed] =
    readJournal
      .persistenceIds()
      .filter(persistenceId => persistenceId.startsWith(RecurringTransaction.TypeKey.name))
      .flatMapConcat(persistenceId => {
        val entity = sharding.entityRefFor(SequenceNumberStore.TypeKey, persistenceId.replace("|", ""))
        Source
          .future(entity.ask[GetSequenceNumberResponse](ref => GetSequenceNumber(ref)))
          .map(response => (persistenceId, response))
      })
      .flatMapMerge(
        100, {
          case (persistenceId, startSequence) =>
            readJournal
              .eventsByPersistenceId(persistenceId, startSequence.sequenceNumber + 1, Long.MaxValue)
        }
      )
      .flatMapConcat {
        case EventEnvelope(_, persistenceId, sequenceNr, evt: NewOutboundRecurringTransactionFound) =>
          publish(
            buildMessage(evt),
            RecurringMessaging.channel,
            NewOutboundRecurringTransactionFoundMsg.payloadType,
            persistenceId,
            sequenceNr
          )

        case EventEnvelope(_, persistenceId, sequenceNr, evt: NewInboundRecurringTransactionFound) =>
          publish(
            buildMessage(evt),
            RecurringMessaging.channel,
            NewInboundRecurringTransactionFoundMsg.payloadType,
            persistenceId,
            sequenceNr
          )

        case EventEnvelope(_, persistenceId, sequenceNr, evt: RecurringTransactionMonetaryAccountChanged) =>
          publish(
            buildMessage(evt),
            RecurringMessaging.channel,
            RecurringTransactionMonetaryAccountChangedMsg.payloadType,
            persistenceId,
            sequenceNr
          )

        case EventEnvelope(_, persistenceId, sequenceNr, evt: RecurringTransactionAmountChanged) =>
          publish(
            buildMessage(evt),
            RecurringMessaging.channel,
            RecurringTransactionAmountChangedMsg.payloadType,
            persistenceId,
            sequenceNr
          )

        case EventEnvelope(_, persistenceId, sequenceNr, evt: RecurringTransactionFrequencyChanged) =>
          publish(
            buildMessage(evt),
            RecurringMessaging.channel,
            RecurringTransactionFrequencyChangedMsg.payloadType,
            persistenceId,
            sequenceNr
          )

        case EventEnvelope(_, persistenceId, sequenceNr, evt: RecurringTransactionStartDateChanged) =>
          publish(
            buildMessage(evt),
            RecurringMessaging.channel,
            RecurringTransactionStartDateChangedMsg.payloadType,
            persistenceId,
            sequenceNr
          )

        case EventEnvelope(_, persistenceId, sequenceNr, evt: RecurringTransactionEnded) =>
          publish(buildMessage(evt), RecurringMessaging.channel, RecurringTransactionEndedMsg.payloadType, persistenceId, sequenceNr)

        case EventEnvelope(_, persistenceId, sequenceNr, evt: RecurringTransactionReopened) =>
          publish(buildMessage(evt), RecurringMessaging.channel, RecurringTransactionReopenedMsg.payloadType, persistenceId, sequenceNr)

        case EventEnvelope(_, persistenceId, sequenceNr, evt: NewRecurringTransactionInstanceFound) =>
          publish(
            buildMessage(evt),
            RecurringMessaging.channel,
            NewRecurringTransactionInstanceFoundMsg.payloadType,
            persistenceId,
            sequenceNr
          )

        case EventEnvelope(_, persistenceId, sequenceNr, evt: LinkedToTransactionOnOtherSide) =>
          publish(
            buildMessage(evt),
            RecurringMessaging.channel,
            LinkedToTransactionOnOtherSideMsg.payloadType,
            persistenceId,
            sequenceNr
          )

        case _ =>
          Source.empty
      }

  def start(killSwitch: SharedKillSwitch)(implicit ec: ExecutionContext): Future[Done] = {
    logger.info(s"Starting to publish some events from MonetaryAccount")
    source
      .via(killSwitch.flow)
      .toMat(Sink.ignore)(Keep.right)
      .run()
  }

  private def publish[T: serde.Serializable](
      message: T,
      channel: String,
      payloadType: String,
      persistenceId: String,
      sequenceNumber: Long
  ) =
    Source
      .single(message)
      .alsoTo(publisher.publishTo[T](channel, payloadType))
      .alsoTo(
        Sink.foreach(_ => {
          val entity = sharding.entityRefFor(SequenceNumberStore.TypeKey, persistenceId.replace("|", ""))
          entity ! UpdateSequenceNumber(newSequenceNumber = sequenceNumber) // TODO: use actorRefWithBackpressure
        })
      )

  private def buildMessage(evt: NewOutboundRecurringTransactionFound): NewOutboundRecurringTransactionFoundMsg =
    NewOutboundRecurringTransactionFoundMsg(
      id = evt.id,
      monetaryAccountId = evt.monetaryAccountId,
      startDate = evt.startDate,
      endDate = evt.endDate,
      frequency = evt.frequency,
      source = evt.source.toString,
      amount = evt.amount,
      currency = evt.currency,
      payee = TransactionPartyInMessage(iban = evt.payee.iban, name = evt.payee.name)
    )

  private def buildMessage(evt: NewInboundRecurringTransactionFound): NewInboundRecurringTransactionFoundMsg =
    NewInboundRecurringTransactionFoundMsg(
      id = evt.id,
      monetaryAccountId = evt.monetaryAccountId,
      startDate = evt.startDate,
      endDate = evt.endDate,
      frequency = evt.frequency,
      source = evt.source.toString,
      amount = evt.amount,
      currency = evt.currency,
      payer = TransactionPartyInMessage(iban = evt.payer.iban, name = evt.payer.name)
    )

  private def buildMessage(evt: RecurringTransactionMonetaryAccountChanged): RecurringTransactionMonetaryAccountChangedMsg =
    RecurringTransactionMonetaryAccountChangedMsg(id = evt.id, newMonetaryAccount = evt.newMonetaryAccount)

  private def buildMessage(evt: RecurringTransactionAmountChanged): RecurringTransactionAmountChangedMsg =
    RecurringTransactionAmountChangedMsg(id = evt.id, newAmount = evt.newAmount)

  private def buildMessage(evt: RecurringTransactionFrequencyChanged): RecurringTransactionFrequencyChangedMsg =
    RecurringTransactionFrequencyChangedMsg(id = evt.id, newFrequency = evt.newFrequency)

  private def buildMessage(evt: RecurringTransactionStartDateChanged): RecurringTransactionStartDateChangedMsg =
    RecurringTransactionStartDateChangedMsg(id = evt.id, newStartDate = evt.newStartDate)

  private def buildMessage(evt: RecurringTransactionEnded): RecurringTransactionEndedMsg =
    RecurringTransactionEndedMsg(id = evt.id, endDate = evt.endDate)

  private def buildMessage(evt: RecurringTransactionReopened): RecurringTransactionReopenedMsg =
    RecurringTransactionReopenedMsg(id = evt.id, newEndDate = evt.newEndDate)

  private def buildMessage(evt: NewRecurringTransactionInstanceFound): NewRecurringTransactionInstanceFoundMsg =
    NewRecurringTransactionInstanceFoundMsg(
      recurringTransactionId = evt.recurringTransactionId,
      transactionId = evt.transactionId,
      amount = evt.amount,
      transactionDate = evt.transactionDate
    )

  private def buildMessage(evt: LinkedToTransactionOnOtherSide): LinkedToTransactionOnOtherSideMsg =
    LinkedToTransactionOnOtherSideMsg(
      recurringTransactionId = evt.recurringTransactionId,
      recurringTransactionIdOnOtherSide = evt.recurringTransactionIdOnOtherSide,
      payee = evt.payee
    )
}

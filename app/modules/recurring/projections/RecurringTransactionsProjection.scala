package modules.recurring.projections

import java.time.{Clock, Instant, Period}
import java.util.Currency

import akka.stream.scaladsl.{Merge, Sink, Source}
import akka.stream.{Materializer, OverflowStrategy, SharedKillSwitch}
import akka.{Done, NotUsed}
import eu.timepit.refined.api.Refined
import eu.timepit.refined.types.string.NonEmptyString
import javax.inject.{Inject, Singleton}
import messaging.MessageReceiver
import modules.core.domain
import modules.recurring.messaging._
import org.reactivestreams.Publisher
import persistence.DatabaseExecutionContext
import persistence.ScalalikejdbcImplicits._
import play.api.Logging
import scalikejdbc.streams._
import scalikejdbc.{DB, SQLSyntax, WrappedResultSet}
import serde.Deserializable
import streaming.AkkaStreamImplicits._
import streaming.AkkaStreamsUtil

import scala.concurrent.Future
import scala.language.postfixOps

case class TransactionParty(iban: Option[types.IbanString], name: Option[NonEmptyString])

case class RecurringTransactionRow(
    id: types.RecurringTransactionId,
    startDate: Instant,
    endDate: Option[Instant],
    frequency: Period,
    direction: types.TransactionDirection,
    amount: BigDecimal,
    currency: Currency,
    payer: TransactionParty,
    payee: TransactionParty,
    monetaryAccountId: types.MonetaryAccountId,
    payeeMonetaryAccountId: Option[types.MonetaryAccountId],
    status: String,
    source: String
) {
  def toMinimalTransaction: domain.MinimalTransaction = domain.MinimalTransaction(
    payeeIban = payee.iban,
    payeeName = None,
    payerIban = payer.iban,
    payerName = None,
    monetaryAccountId = monetaryAccountId,
    direction = direction,
    description = ""
  )
}

object RecurringTransactionRow {
  def fromResultSet(rs: WrappedResultSet): RecurringTransactionRow = RecurringTransactionRow(
    id = rs.get("id"),
    startDate = rs.get("start_date"),
    endDate = rs.get("end_date"),
    frequency = rs.get("frequency"),
    direction = rs.get("direction"),
    amount = rs.get("amount"),
    currency = Currency.getInstance(rs.string("currency")),
    payee = TransactionParty(iban = rs.get("payee_iban"), name = rs.get("payee_name")),
    payer = TransactionParty(iban = rs.get("payer_iban"), name = rs.get("payer_name")),
    monetaryAccountId = rs.get("monetary_account_id"),
    payeeMonetaryAccountId = rs.stringOpt("payee_monetary_account_id").map(Refined.unsafeApply),
    status = rs.get("status"),
    source = rs.get("source")
  )
}

case class RecurringMaterializedTransaction(
    id: types.TransactionId,
    recurringTransactionId: types.RecurringTransactionId,
    transactionDate: Instant
)
object RecurringMaterializedTransaction {
  def fromResultSet(rs: WrappedResultSet): RecurringMaterializedTransaction = RecurringMaterializedTransaction(
    id = rs.get("id"),
    recurringTransactionId = rs.get("recurring_transaction_id"),
    transactionDate = rs.get("transaction_date")
  )
}

trait RecurringTransactionsRepository {
  def fetchActiveInboundByMonetaryAccountIds(monetaryAccountIds: Seq[types.MonetaryAccountId]): Publisher[RecurringTransactionRow]
  def fetchActiveOutboundByMonetaryAccountIds(monetaryAccountIds: Seq[types.MonetaryAccountId]): Publisher[RecurringTransactionRow]

  def fetchOutboundInstancesByMonetaryAccountIdsInPeriod(
      monetaryAccountIds: Seq[types.MonetaryAccountId],
      startDate: Instant,
      endDate: Instant
  ): Publisher[RecurringMaterializedTransaction]

  def fetchActiveOutboundInstancesByMonetaryAccountIdsInPeriod(
      monetaryAccountIds: Seq[types.MonetaryAccountId],
      startDate: Instant,
      endDate: Instant
  ): Publisher[RecurringMaterializedTransaction]

  def fetchInboundInstancesByMonetaryAccountIdsInPeriod(
      monetaryAccountIds: Seq[types.MonetaryAccountId],
      startDate: Instant,
      endDate: Instant
  ): Publisher[RecurringMaterializedTransaction]

  def fetchActiveInboundInstancesByMonetaryAccountIdsInPeriod(
      monetaryAccountIds: Seq[types.MonetaryAccountId],
      startDate: Instant,
      endDate: Instant
  ): Publisher[RecurringMaterializedTransaction]
}

@Singleton
private[recurring] class PostgresRecurringTransactionsProjection @Inject()(receiver: MessageReceiver, clock: Clock)(
    implicit ec: DatabaseExecutionContext,
    mat: Materializer
) extends RecurringTransactionsRepository
    with Logging {
  import RecurringTransactionSerde._
  import serde.PlayJsonDeserialization._

  private val recurringTransactionTable             = SQLSyntax.createUnsafely("recurring_transactions")
  private val recurringMaterializedTransactionTable = SQLSyntax.createUnsafely("recurring_materialized_transactions")

  def start(killSwitch: SharedKillSwitch): Future[Done] = {
    val listeners = Seq(
      listenFor[NewInboundRecurringTransactionFoundMsg](NewInboundRecurringTransactionFoundMsg.payloadType),
      listenFor[NewOutboundRecurringTransactionFoundMsg](NewOutboundRecurringTransactionFoundMsg.payloadType),
      listenFor[RecurringTransactionMonetaryAccountChangedMsg](RecurringTransactionMonetaryAccountChangedMsg.payloadType),
      listenFor[RecurringTransactionAmountChangedMsg](RecurringTransactionAmountChangedMsg.payloadType),
      listenFor[RecurringTransactionFrequencyChangedMsg](RecurringTransactionFrequencyChangedMsg.payloadType),
      listenFor[RecurringTransactionStartDateChangedMsg](RecurringTransactionStartDateChangedMsg.payloadType),
      listenFor[RecurringTransactionEndDateChangedMsg](RecurringTransactionEndDateChangedMsg.payloadType),
      listenFor[RecurringTransactionEndedMsg](RecurringTransactionEndedMsg.payloadType),
      listenFor[RecurringTransactionReopenedMsg](RecurringTransactionReopenedMsg.payloadType),
      listenFor[NewRecurringTransactionInstanceFoundMsg](NewRecurringTransactionInstanceFoundMsg.payloadType),
    )

    Source
      .combine(
        listeners.head,
        listeners(1),
        listeners(2),
        listeners(3),
        listeners(4),
        listeners(5),
        listeners(6),
        listeners(7),
        listeners(8),
        listeners(9)
      )(Merge(_))
      .via(killSwitch.flow)
      .buffer(200, OverflowStrategy.backpressure)
      .runWith(Sink.foreachAsync(1)(handle))
  }

  private def listenFor[MsgType: Deserializable](payloadType: String): Source[Any, NotUsed] = {
    logger.info(s"Starting to listen for '$payloadType' messages on '${RecurringMessaging.channel}'")
    receiver
      .receive[MsgType](channel = RecurringMessaging.channel, payloadType = payloadType)
  }

  override def fetchActiveInboundByMonetaryAccountIds(ids: Seq[types.MonetaryAccountId]): Publisher[RecurringTransactionRow] =
    fetchActiveByMonetaryAccountIdsInDirection(ids, types.TransactionDirection.Inbound)

  override def fetchActiveOutboundByMonetaryAccountIds(ids: Seq[types.MonetaryAccountId]): Publisher[RecurringTransactionRow] =
    fetchActiveByMonetaryAccountIdsInDirection(ids, types.TransactionDirection.Outbound)

  private def fetchActiveByMonetaryAccountIdsInDirection(
      ids: Seq[types.MonetaryAccountId],
      direction: types.TransactionDirection
  ): Publisher[RecurringTransactionRow] =
    if (ids.isEmpty) AkkaStreamsUtil.emptyPublisher()
    else
      DB.readOnlyStream {
        sqlr"""
          select * from $recurringTransactionTable
          where monetary_account_id IN ($ids) AND direction = $direction and status = 'ACTIVE'
         """
          .map(RecurringTransactionRow.fromResultSet)
          .iterator()
      }

  override def fetchOutboundInstancesByMonetaryAccountIdsInPeriod(
      monetaryAccountIds: Seq[types.MonetaryAccountId],
      startDate: Instant,
      endDate: Instant
  ): Publisher[RecurringMaterializedTransaction] =
    fetchInstancesByMonetaryAccountIdsInPeriodWithDirection(
      monetaryAccountIds = monetaryAccountIds,
      direction = types.TransactionDirection.Outbound,
      startDate = startDate,
      endDate = endDate
    )

  override def fetchActiveOutboundInstancesByMonetaryAccountIdsInPeriod(
      monetaryAccountIds: Seq[types.MonetaryAccountId],
      startDate: Instant,
      endDate: Instant
  ): Publisher[RecurringMaterializedTransaction] =
    fetchActiveInstancesByMonetaryAccountIdsInPeriodWithDirection(
      monetaryAccountIds = monetaryAccountIds,
      direction = types.TransactionDirection.Outbound,
      startDate = startDate,
      endDate = endDate
    )

  override def fetchInboundInstancesByMonetaryAccountIdsInPeriod(
      monetaryAccountIds: Seq[types.MonetaryAccountId],
      startDate: Instant,
      endDate: Instant
  ): Publisher[RecurringMaterializedTransaction] =
    fetchInstancesByMonetaryAccountIdsInPeriodWithDirection(
      monetaryAccountIds,
      types.TransactionDirection.Inbound,
      startDate,
      endDate
    )

  override def fetchActiveInboundInstancesByMonetaryAccountIdsInPeriod(
      monetaryAccountIds: Seq[types.MonetaryAccountId],
      startDate: Instant,
      endDate: Instant
  ): Publisher[RecurringMaterializedTransaction] =
    fetchActiveInstancesByMonetaryAccountIdsInPeriodWithDirection(
      monetaryAccountIds,
      types.TransactionDirection.Inbound,
      startDate,
      endDate
    )

  private def fetchActiveInstancesByMonetaryAccountIdsInPeriodWithDirection(
      monetaryAccountIds: Seq[types.MonetaryAccountId],
      direction: types.TransactionDirection,
      startDate: Instant,
      endDate: Instant
  ): Publisher[RecurringMaterializedTransaction] =
    if (monetaryAccountIds.isEmpty) AkkaStreamsUtil.emptyPublisher()
    else
      DB.readOnlyStream {
        sqlr"""
          select $recurringMaterializedTransactionTable.* from $recurringMaterializedTransactionTable
          inner join $recurringTransactionTable ON ${recurringTransactionTable}.id = ${recurringMaterializedTransactionTable}.recurring_transaction_id
          where monetary_account_id IN ($monetaryAccountIds) 
            AND direction = $direction 
            AND status = 'ACTIVE'
            AND transaction_date BETWEEN $startDate AND $endDate
         """
          .map(RecurringMaterializedTransaction.fromResultSet)
          .iterator()
      }

  private def fetchInstancesByMonetaryAccountIdsInPeriodWithDirection(
      monetaryAccountIds: Seq[types.MonetaryAccountId],
      direction: types.TransactionDirection,
      startDate: Instant,
      endDate: Instant
  ): Publisher[RecurringMaterializedTransaction] =
    if (monetaryAccountIds.isEmpty) AkkaStreamsUtil.emptyPublisher()
    else
      DB.readOnlyStream {
        sqlr"""
          select $recurringMaterializedTransactionTable.* from $recurringMaterializedTransactionTable
          inner join $recurringTransactionTable ON ${recurringTransactionTable}.id = ${recurringMaterializedTransactionTable}.recurring_transaction_id
          where monetary_account_id IN ($monetaryAccountIds) 
            AND direction = $direction 
            AND transaction_date BETWEEN $startDate AND $endDate
         """
          .map(RecurringMaterializedTransaction.fromResultSet)
          .iterator()
      }

  private def handle(evt: Any): Future[Unit] = evt match {
    case evt: NewInboundRecurringTransactionFoundMsg        => handle(evt)
    case evt: NewOutboundRecurringTransactionFoundMsg       => handle(evt)
    case evt: RecurringTransactionMonetaryAccountChangedMsg => handle(evt)
    case evt: RecurringTransactionAmountChangedMsg          => handle(evt)
    case evt: RecurringTransactionFrequencyChangedMsg       => handle(evt)
    case evt: RecurringTransactionEndDateChangedMsg         => handle(evt)
    case evt: RecurringTransactionStartDateChangedMsg       => handle(evt)
    case evt: RecurringTransactionEndedMsg                  => handle(evt)
    case evt: RecurringTransactionReopenedMsg               => handle(evt)
    case evt: NewRecurringTransactionInstanceFoundMsg       => handle(evt)
    case _ =>
      throw new Error(s"RecurringTransactionsProjection does not how to handle $evt")
  }

  private def handle(evt: NewOutboundRecurringTransactionFoundMsg): Future[Unit] = DB.futureLocalTx { implicit session =>
    Future {
      sqlr"""
          INSERT INTO $recurringTransactionTable(id, start_date, end_date, frequency, amount, currency, direction, payee_iban, payee_name, monetary_account_id, source)
          VALUES (${evt.id}, ${evt.startDate}, ${evt.endDate}, ${evt.frequency.toString}, ${evt.amount}, ${evt.currency.getCurrencyCode}, ${types.TransactionDirection.Outbound}, ${evt.payee.iban}, ${evt.payee.name}, ${evt.monetaryAccountId}, ${evt.source})
          ON CONFLICT (id)
          DO NOTHING
      """.update().apply()
    }
  }

  private def handle(evt: NewInboundRecurringTransactionFoundMsg): Future[Unit] = DB.futureLocalTx { implicit session =>
    Future {
      sqlr"""
          INSERT INTO $recurringTransactionTable(id, start_date, end_date, frequency, amount, currency, direction, payer_iban, payer_name, monetary_account_id, source)
          VALUES (${evt.id}, ${evt.startDate}, ${evt.endDate}, ${evt.frequency.toString}, ${evt.amount}, ${evt.currency.getCurrencyCode}, ${types.TransactionDirection.Inbound}, ${evt.payer.iban}, ${evt.payer.name}, ${evt.monetaryAccountId}, ${evt.source})
          ON CONFLICT (id)
          DO NOTHING
      """.update().apply()
    }
  }

  private def handle(evt: RecurringTransactionMonetaryAccountChangedMsg): Future[Unit] = DB.futureLocalTx { implicit session =>
    Future {
      sqlr"""
        UPDATE $recurringTransactionTable SET monetary_account_id = ${evt.newMonetaryAccount} WHERE id = ${evt.id}
      """.update().apply()
    }
  }

  private def handle(evt: RecurringTransactionAmountChangedMsg): Future[Unit] = DB.futureLocalTx { implicit session =>
    Future {
      sqlr"""
        UPDATE $recurringTransactionTable SET amount = ${evt.newAmount} WHERE id = ${evt.id}
      """.update().apply()
    }
  }

  private def handle(evt: RecurringTransactionFrequencyChangedMsg): Future[Unit] = DB.futureLocalTx { implicit session =>
    Future {
      sqlr"""
        UPDATE $recurringTransactionTable SET frequency = ${evt.newFrequency.toString} WHERE id = ${evt.id}
      """.update().apply()
    }
  }

  private def handle(evt: RecurringTransactionEndDateChangedMsg): Future[Unit] = DB.futureLocalTx { implicit session =>
    Future {
      sqlr"""
        UPDATE $recurringTransactionTable SET end_date = ${evt.newEndDate} WHERE id = ${evt.id}
      """.update().apply()
    }
  }

  private def handle(evt: RecurringTransactionStartDateChangedMsg): Future[Unit] = DB.futureLocalTx { implicit session =>
    Future {
      sqlr"""
        UPDATE $recurringTransactionTable SET start_date = ${evt.newStartDate} WHERE id = ${evt.id}
      """.update().apply()
    }
  }

  private def handle(evt: RecurringTransactionEndedMsg): Future[Unit] = DB.futureLocalTx { implicit session =>
    Future {
      sqlr"""
        UPDATE $recurringTransactionTable SET status = 'ENDED', end_date = ${evt.endDate} WHERE id = ${evt.id}
      """.update().apply()
    }
  }

  private def handle(evt: RecurringTransactionReopenedMsg): Future[Unit] = DB.futureLocalTx { implicit session =>
    Future {
      sqlr"""
        UPDATE $recurringTransactionTable SET status = 'ACTIVE' WHERE id = ${evt.id}
      """.update().apply()
    }
  }

  private def handle(evt: NewRecurringTransactionInstanceFoundMsg): Future[Unit] = DB.futureLocalTx { implicit session =>
    Future {
      sqlr"""
        INSERT INTO $recurringMaterializedTransactionTable(id, recurring_transaction_id, transaction_date)
          VALUES (${evt.transactionId}, ${evt.recurringTransactionId}, ${evt.transactionDate})
          ON CONFLICT (id)
          DO NOTHING
      """.update().apply()
    }
  }
}

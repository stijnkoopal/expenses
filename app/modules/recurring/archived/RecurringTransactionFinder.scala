package modules.recurring.archived

import modules.core.projections.Transaction

trait RecurringTransactionFinder {
  def findRecurringTransactions(transactions: Seq[Transaction]): Set[RecurringTransactionFromModel]
}

package modules.recurring.archived

import java.time._
import java.time.temporal.ChronoUnit.DAYS
import java.time.temporal.TemporalAmount

import javax.inject.{Inject, Singleton}
import modules.core.projections.Transaction

import scala.annotation.tailrec

// TODO: also include other frequencies
@Singleton
class IncrementalDatesRecurringTransactionsFinder @Inject() (val transactionSimilarityChecker: TransactionSimilarityChecker)
    extends RecurringTransactionFinder {
  private val banksClosed: Set[MonthDay] = Set(
    MonthDay.of(1, 1),
    MonthDay.of(4, 19),
    MonthDay.of(4, 22),
    MonthDay.of(5, 1),
    MonthDay.of(5, 5),
    MonthDay.of(12, 25),
    MonthDay.of(12, 26)
  )

  override def findRecurringTransactions(transactions: Seq[Transaction]): Set[RecurringTransactionFromModel] = {
    @tailrec
    def findSimilarTransactionWithPattern(
        toTest: Transaction,
        transactionsAfter: Seq[Transaction],
        result: Seq[Transaction]
    ): Seq[Transaction] = {
      val (expectedBeginNextDate, expectedEndNextDate) = getNextAvailableTransferRangeAfter(toTest.timestamp, Period.ofMonths(1))

      val transactionsInRange =
        transactionsAfter.filter(tx => tx.timestamp.isAfter(expectedBeginNextDate) && tx.timestamp.isBefore(expectedEndNextDate))
      val similarTransactionInRange = transactionsInRange.filter(tx => transactionSimilarityChecker.isSimilarTransaction(toTest, tx))

      if (similarTransactionInRange.size == 1) {
        findSimilarTransactionWithPattern(similarTransactionInRange.head, transactionsAfter, result :+ similarTransactionInRange.head)
      } else {
        result
      }
    }

    @tailrec
    def inner(
        remainingTransactions: Seq[Transaction],
        result: Set[RecurringTransactionFromModel]
    ): Set[RecurringTransactionFromModel] = {
      remainingTransactions match {
        case Nil => result
        case head +: tail =>
          val transactionsWithPattern = findSimilarTransactionWithPattern(head, tail, Seq(head))

          if (transactionsWithPattern.size >= 3) {
            inner(
              tail.filter(tx => !transactionsWithPattern.contains(tx)),
              result.incl(RecurringTransactionFromModel.fromTransactions(transactionsWithPattern))
            )
          } else {
            inner(tail, result)
          }
      }
    }

    inner(transactions.sortBy(_.timestamp), Set())
  }

  private def getNextAvailableTransferRangeAfter(date: Instant, periodToAdd: TemporalAmount): (Instant, Instant) = {
    val expectedNextDate      = date plus periodToAdd
    var expectedBeginNextDate = expectedNextDate.minus(1, DAYS).truncatedTo(DAYS).atZone(ZoneId.of("UTC"))

    while (expectedBeginNextDate.getDayOfWeek == DayOfWeek.SATURDAY
           || expectedBeginNextDate.getDayOfWeek == DayOfWeek.SUNDAY
           || banksClosed.contains(MonthDay.of(expectedBeginNextDate.getMonthValue, expectedBeginNextDate.getDayOfMonth))) {
      expectedBeginNextDate = expectedBeginNextDate.minus(1, DAYS)
    }

    var expectedEndNextDate = expectedNextDate.plus(2, DAYS).truncatedTo(DAYS).atZone(ZoneId.of("UTC"))

    while (expectedEndNextDate.getDayOfWeek == DayOfWeek.SATURDAY
           || expectedEndNextDate.getDayOfWeek == DayOfWeek.SUNDAY
           || banksClosed.contains(MonthDay.of(expectedEndNextDate.getMonthValue, expectedEndNextDate.getDayOfMonth))) {
      expectedEndNextDate = expectedEndNextDate.plus(1, DAYS)
    }

    (expectedBeginNextDate.toInstant, expectedEndNextDate.toInstant)
  }
}

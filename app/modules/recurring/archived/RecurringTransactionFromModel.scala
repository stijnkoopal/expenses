package modules.recurring.archived

import java.time.Instant
import java.time.temporal.ChronoUnit

import modules.core.projections.{Transaction, TransactionParty}

case class RecurringTransactionFromModel(
    id: types.RecurringTransactionId,
    startDate: Instant,
    endDate: Option[Instant],
    amount: BigDecimal,
    payer: Option[TransactionParty],
    payee: Option[TransactionParty],
    monetaryAccountId: types.MonetaryAccountId,
    transactions: Seq[Transaction]
)

object RecurringTransactionFromModel {
  def fromTransactions(txs: Seq[Transaction]): RecurringTransactionFromModel = {
    val start      = txs.head
    val end        = txs.last
    val secondLast = txs(txs.size - 2)
    val difference = ChronoUnit.DAYS.between(end.timestamp, secondLast.timestamp)

    RecurringTransactionFromModel(
      id = start.id,
      startDate = start.timestamp,
      endDate =
        if (ChronoUnit.DAYS.between(Instant.now(), end.timestamp).abs > difference.abs)
          Some(end.timestamp)
        else
          None,
      amount = end.amount,
      payer = end.payer.map(payer => TransactionParty(iban = payer.iban, name = payer.name)),
      payee = end.payee.map(payee => TransactionParty(iban = payee.iban, name = payee.name)),
      monetaryAccountId = end.monetaryAccountId,
      transactions = txs
    )
  }
}

package modules.recurring.archived

import java.time.{Duration => JDuration}

import javax.inject.{Inject, Singleton}
import modules.core.projections.Transaction

import scala.concurrent.duration.{Duration, _}
import scala.language.postfixOps

@Singleton
class SummingAndAveragingRecurringTransactionFinder @Inject() (val similarityChecker: TransactionSimilarityChecker)
    extends RecurringTransactionFinder {
  override def findRecurringTransactions(transactions: Seq[Transaction]): Set[RecurringTransactionFromModel] = {
    val setsOfSimilarTransactions = findSimilarTransactions(transactions)

    (setsOfSimilarTransactions.filter(similarTransactions => hasFrequencyPatternOf(similarTransactions, 1 days, 0)) ++
      setsOfSimilarTransactions.filter(similarTransactions => hasFrequencyPatternOf(similarTransactions, 7 days, 0.08)) ++
      setsOfSimilarTransactions.filter(similarTransactions => hasFrequencyPatternOf(similarTransactions, (365 / 12) days, 0.08)) ++
      setsOfSimilarTransactions.filter(similarTransactions => hasFrequencyPatternOf(similarTransactions, (365 / 6) days, 0.08)) ++
      setsOfSimilarTransactions.filter(similarTransactions => hasFrequencyPatternOf(similarTransactions, (365 / 4) days, 0.08)) ++
      setsOfSimilarTransactions.filter(similarTransactions => hasFrequencyPatternOf(similarTransactions, (365 / 3) days, 0.08)) ++
      setsOfSimilarTransactions.filter(similarTransactions => hasFrequencyPatternOf(similarTransactions, (365 / 2) days, 0.08)) ++
      setsOfSimilarTransactions.filter(similarTransactions => hasFrequencyPatternOf(similarTransactions, 365 days, 0.08)) ++
      setsOfSimilarTransactions.filter(similarTransactions => hasFrequencyPatternOf(similarTransactions, (365 * 2) days, 0.08)))
      .map(RecurringTransactionFromModel.fromTransactions)
  }

  private def hasFrequencyPatternOf(
      sortedSimilarTransactions: Seq[Transaction],
      frequency: Duration,
      frequencyMargin: Double
  ): Boolean = {
    if (sortedSimilarTransactions.size < 3) return false

    val durations = sortedSimilarTransactions
      .lazyZip(sortedSimilarTransactions.tail)
      .map {
        case (a, b) =>
          JDuration.ofMillis(
            1000 * b.timestamp
              .minus(JDuration.ofMillis(1000 * a.timestamp.getEpochSecond))
              .getEpochSecond
          )
      }
      .map(duration => Duration.fromNanos(duration.toNanos))

    val summedDuration     = durations.fold(Duration.Zero) { case (a, b) => a + b }
    val avgDuration        = summedDuration / durations.size
    val durationStdDevDays = Math.sqrt(durations.map(duration => Math.pow(duration.toDays - avgDuration.toDays, 2)).sum / durations.size)

    val startOfPeriod           = sortedSimilarTransactions.head.timestamp
    val endOfPeriod             = sortedSimilarTransactions.last.timestamp
    val numberOfPeriodsExpected = (endOfPeriod.getEpochSecond - startOfPeriod.getEpochSecond) / frequency.toSeconds

    avgDuration >= frequency * (1 - frequencyMargin) && avgDuration <= frequency * (1 + frequencyMargin) && durations.size == numberOfPeriodsExpected && durationStdDevDays <= frequency.toDays / 3
  }

  private def findSimilarTransactions(
      transactions: Seq[Transaction]
  ): Set[Seq[Transaction]] = {
    @scala.annotation.tailrec
    def inner(
        txs: Seq[Transaction],
        result: Seq[Seq[Transaction]]
    ): Set[Seq[Transaction]] = txs match {
      case Nil => result.toSet
      case head +: tail =>
        val similarTransactions = head +: tail.filter(tx => similarityChecker.isSimilarTransaction(head, tx))

        if (similarTransactions.size == 1) {
          inner(tail, result)
        } else {
          inner(tail.filter(tx => !similarTransactions.contains(tx)), result :+ similarTransactions.sortBy(_.timestamp))
        }
    }

    inner(transactions, Seq())
  }
}

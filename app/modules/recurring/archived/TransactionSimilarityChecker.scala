package modules.recurring.archived

import modules.core.projections.Transaction

trait TransactionSimilarityChecker {
  def isSimilarTransaction(tx1: Transaction, tx2: Transaction): Boolean
}

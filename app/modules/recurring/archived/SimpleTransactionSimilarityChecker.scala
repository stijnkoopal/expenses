package modules.recurring.archived

import javax.inject.Singleton
import modules.core.projections.Transaction

@Singleton
class SimpleTransactionSimilarityChecker extends TransactionSimilarityChecker {
  override def isSimilarTransaction(tx1: Transaction, tx2: Transaction): Boolean = {
    val percentDiff =
      if (tx1.amount == tx2.amount) BigDecimal(0) else (tx1.amount - tx2.amount).abs / ((tx1.amount.abs + tx2.amount.abs) / 2)
    percentDiff <= 0.01 && tx1.currency == tx2.currency && similar(tx1.payee.map(_.name), tx2.payee.map(_.name)) && similar(tx1.payer.map(_.name), tx2.payer.map(_.name))
  }

  private def similar[T](description1: Option[T], description2: Option[T]): Boolean =
    (description1, description2) match {
      case (Some(d1), Some(d2)) => d1 == d2
      case _                    => false
    }

  private def descriptionIsSimilar(description1: Option[String], description2: Option[String]): Boolean =
    (description1, description2) match {
      case (Some(d1), Some(d2)) => d1 == d2
      case _                    => false
    }
}

package modules.recurring

import akka.Done
import akka.actor.CoordinatedShutdown
import akka.stream.KillSwitches
import javax.inject.{Inject, Singleton}
import modules.recurring.projections.PostgresRecurringTransactionsProjection
import modules.recurring.streaming.reactors.{MessagesToCommandsReactor, PublishEventsReactor}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class RecurringStreamsStarter @Inject() (
    cs: CoordinatedShutdown,
    recurringTransactionsProjection: PostgresRecurringTransactionsProjection,
    documentMessagesToCommandsReactor: MessagesToCommandsReactor,
    publishEventsReactor: PublishEventsReactor
)(
    implicit ec: ExecutionContext
) {
  private val killSwitch = KillSwitches.shared("Transactions")

  private val futures: List[Future[Done]] = List(
    recurringTransactionsProjection.start(killSwitch),
    documentMessagesToCommandsReactor.start(killSwitch),
    publishEventsReactor.start(killSwitch)
  )

  cs.addTask(CoordinatedShutdown.PhaseServiceUnbind, "stop-transaction-streams") { () =>
    killSwitch.shutdown()
    Future.sequence(futures).map(_ => Done)
  }
}

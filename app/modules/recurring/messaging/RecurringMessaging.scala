package modules.recurring.messaging

import java.time.{Instant, Period}
import java.util.Currency

import eu.timepit.refined.types.string.NonEmptyString
import play.api.libs.json._
import serde.{PlayJsonDefaultDeserialization, PlayJsonDefaultSerialization}

case class TransactionPartyInMessage(
    iban: Option[types.IbanString],
    name: Option[NonEmptyString]
)

case class RecurringDirectDebitTransactionDocument(
    id: types.TransactionId,
    recurringTransactionId: types.RecurringTransactionId,
    userId: types.UserId,
    monetaryAccountId: types.MonetaryAccountId,
    currency: Currency,
    amount: BigDecimal,
    payee: TransactionPartyInMessage,
    payer: TransactionPartyInMessage,
    description: String,
    created: Instant,
    responded: Instant
)

object RecurringDirectDebitTransactionDocument {
  val payloadType = "materializedRecurringTransactionDocument"
}

case class RecurringScheduleDocument(
    id: types.RecurringScheduleId,
    startDate: Instant,
    endDate: Option[Instant],
    frequency: Period,
    amount: BigDecimal,
    currency: Currency,
    monetaryAccountId: types.MonetaryAccountId,
    payee: TransactionPartyInMessage,
    payer: TransactionPartyInMessage,
    institution: types.Institution,
    description: String
)

object RecurringScheduleDocument {
  val payloadType = "recurringScheduleDocument"
}

case class NewInboundRecurringTransactionFoundMsg(
    id: types.RecurringTransactionId,
    monetaryAccountId: types.MonetaryAccountId,
    startDate: Instant,
    endDate: Option[Instant],
    frequency: Period,
    source: String,
    amount: BigDecimal,
    currency: Currency,
    payer: TransactionPartyInMessage
)
object NewInboundRecurringTransactionFoundMsg {
  val payloadType = "NewInboundRecurringTransactionFoundMsg"
}

case class NewOutboundRecurringTransactionFoundMsg(
    id: types.RecurringTransactionId,
    monetaryAccountId: types.MonetaryAccountId,
    startDate: Instant,
    endDate: Option[Instant],
    frequency: Period,
    source: String,
    amount: BigDecimal,
    currency: Currency,
    payee: TransactionPartyInMessage
)
object NewOutboundRecurringTransactionFoundMsg {
  val payloadType = "NewOutboundRecurringTransactionFoundMsg"
}

case class RecurringTransactionAmountChangedMsg(
    id: types.RecurringTransactionId,
    newAmount: BigDecimal
)
object RecurringTransactionAmountChangedMsg {
  val payloadType = "RecurringTransactionAmountChangedMsg"
}

case class RecurringTransactionFrequencyChangedMsg(
    id: types.RecurringTransactionId,
    newFrequency: Period
)
object RecurringTransactionFrequencyChangedMsg {
  val payloadType = "RecurringTransactionFrequencyChangedMsg"
}

case class RecurringTransactionEndDateChangedMsg(
    id: types.RecurringTransactionId,
    newEndDate: Option[Instant]
)
object RecurringTransactionEndDateChangedMsg {
  val payloadType = "RecurringTransactionEndDateChangedMsg"
}

case class RecurringTransactionStartDateChangedMsg(
    id: types.RecurringTransactionId,
    newStartDate: Instant
)
object RecurringTransactionStartDateChangedMsg {
  val payloadType = "RecurringTransactionStartDateChangedMsg"
}

case class RecurringTransactionMonetaryAccountChangedMsg(
    id: types.RecurringTransactionId,
    newMonetaryAccount: types.MonetaryAccountId
)
object RecurringTransactionMonetaryAccountChangedMsg {
  val payloadType = "RecurringTransactionMonetaryAccountChangedMsg"
}

case class RecurringTransactionEndedMsg(
    id: types.RecurringTransactionId,
    endDate: Instant
)
object RecurringTransactionEndedMsg {
  val payloadType = "RecurringTransactionEndedMsg"
}

case class RecurringTransactionReopenedMsg(
    id: types.RecurringTransactionId,
    newEndDate: Option[Instant]
)
object RecurringTransactionReopenedMsg {
  val payloadType = "RecurringTransactionReopenedMsg"
}

case class NewRecurringTransactionInstanceFoundMsg(
    recurringTransactionId: types.RecurringTransactionId,
    transactionId: types.TransactionId,
    amount: BigDecimal,
    transactionDate: Instant
)
object NewRecurringTransactionInstanceFoundMsg {
  val payloadType = "NewRecurringTransactionInstanceFoundMsg"
}

case class LinkedToTransactionOnOtherSideMsg(
    recurringTransactionId: types.RecurringTransactionId,
    recurringTransactionIdOnOtherSide: types.RecurringTransactionId,
    payee: types.MonetaryAccountId,
)
object LinkedToTransactionOnOtherSideMsg {
  val payloadType = "LinkedToTransactionOnOtherSideMsg"
}

object RecurringTransactionSerde extends PlayJsonDefaultDeserialization with PlayJsonDefaultSerialization {
  import de.cbley.refined.play.json._

  implicit val transactionPartyInMessageFormat: Format[TransactionPartyInMessage]                             = Json.format
  implicit val recurringDirectDebitTransactionDocumentFormat: Format[RecurringDirectDebitTransactionDocument] = Json.format
  implicit val scheduleDocumentFormat: Format[RecurringScheduleDocument]                                      = Json.format

  implicit val newInboundRecurringTransactionFoundMsgFormat: Format[NewInboundRecurringTransactionFoundMsg]               = Json.format
  implicit val newOutboundRecurringTransactionFoundMsgFormat: Format[NewOutboundRecurringTransactionFoundMsg]             = Json.format
  implicit val RecurringTransactionEndDateChangedMsgFormat: Format[RecurringTransactionEndDateChangedMsg]                 = Json.format
  implicit val RecurringTransactionStartDateChangedMsgFormat: Format[RecurringTransactionStartDateChangedMsg]             = Json.format
  implicit val RecurringTransactionAmountChangedMsgFormat: Format[RecurringTransactionAmountChangedMsg]                   = Json.format
  implicit val RecurringTransactionFrequencyChangedMsgFormat: Format[RecurringTransactionFrequencyChangedMsg]             = Json.format
  implicit val RecurringTransactionMonetaryAccountChangedMsgFormat: Format[RecurringTransactionMonetaryAccountChangedMsg] = Json.format
  implicit val RecurringTransactionEndedMsgFormat: Format[RecurringTransactionEndedMsg]                                   = Json.format
  implicit val RecurringTransactionReopenedMsgFormat: Format[RecurringTransactionReopenedMsg]                             = Json.format
  implicit val NewRecurringTransactionInstanceFoundMsgFormat: Format[NewRecurringTransactionInstanceFoundMsg]             = Json.format
  implicit val PayeeLinkedToRecurringTransactionMsgFormat: Format[LinkedToTransactionOnOtherSideMsg]                      = Json.format
}

object RecurringMessaging {
  val channel = "recurring"
}

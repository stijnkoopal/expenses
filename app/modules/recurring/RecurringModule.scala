package modules.recurring

import modules.recurring.domain.RecurringTransactionService
import modules.recurring.projections.{PostgresRecurringTransactionsProjection, RecurringTransactionsRepository}
import modules.recurring.services.{
  AkkaBackedRecurringTransactionService,
  RecurringTransactionEntityRepository,
  ShardedRecurringTransactionEntityRepository
}
import play.api.inject.{Binding, Module}
import play.api.{Configuration, Environment}

class RecurringModule extends Module {
  override def bindings(environment: Environment, configuration: Configuration): Seq[Binding[_]] = Seq(
    bind[RecurringTransactionsRepository].to[PostgresRecurringTransactionsProjection].eagerly(),
    bind[RecurringTransactionService].to[AkkaBackedRecurringTransactionService],
    bind[RecurringTransactionEntityRepository].to[ShardedRecurringTransactionEntityRepository],
    bind[RecurringStreamsStarter].toSelf.eagerly()
  )
}

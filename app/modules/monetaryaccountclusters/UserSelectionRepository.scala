package modules.monetaryaccountclusters

import javax.inject.{Inject, Singleton}
import org.reactivestreams.Publisher
import persistence.DatabaseExecutionContext
import persistence.ScalalikejdbcImplicits._
import play.api.Logging
import scalikejdbc.streams._
import scalikejdbc.{DB, SQLSyntax}

import scala.concurrent.Future

trait UserSelectionRepository {
  def fetchSelectedCluster(userId: types.UserId): Publisher[types.MonetaryAccountClusterId]
  def selectClusterForUser(userId: types.UserId, selectedCluster: types.MonetaryAccountClusterId): Future[Unit]
}

@Singleton
private[monetaryaccountclusters] class PostgresUserSelectionRepository @Inject() ()(implicit ec: DatabaseExecutionContext)
    extends UserSelectionRepository
    with Logging {

  val tableName     = "monetary_account_cluster_user_selection"
  private val table = SQLSyntax.createUnsafely(tableName)

  override def fetchSelectedCluster(userId: types.UserId): Publisher[types.MonetaryAccountClusterId] =
    DB.readOnlyStream {
      sqlr"select * from $table where $userId = $userId"
        .map(rs => rs.get[types.MonetaryAccountClusterId]("selected_cluster"))
        .iterator()
    }

  override def selectClusterForUser(userId: types.UserId, selectedClusterId: types.MonetaryAccountClusterId): Future[Unit] =
    DB.futureLocalTx { implicit session =>
      Future {
        sqlr"""
          INSERT INTO $table(user_id, selected_cluster)
          VALUES($userId, $selectedClusterId)
          ON CONFLICT (user_id)
          DO UPDATE SET selected_cluster = $selectedClusterId, updated = NOW()
        """.update.apply()
      }
    }
}

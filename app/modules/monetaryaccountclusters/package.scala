package modules

import eu.timepit.refined.api.{Refined, Validate}

package object monetaryaccountclusters {
  case class Type()

  object Type {
    implicit def MonetaryAccountClusterTypeValidate: Validate.Plain[String, Type] =
      Validate.fromPredicate(predicate, t => s"$t is a valid Type", Type())

    private val predicate: String => Boolean = s => {
      s == "All" || s == "UserDefined"
    }
  }

  type MonetaryAccountClusterType = String Refined Type
  object MonetaryAccountClusterType {
    val All: MonetaryAccountClusterType         = Refined.unsafeApply[String, Type]("All")
    val UserDefined: MonetaryAccountClusterType = Refined.unsafeApply[String, Type]("UserDefined")
  }
}

package modules.monetaryaccountclusters

import java.util.UUID

import akka.stream.Materializer
import akka.stream.scaladsl.Source
import eu.timepit.refined.api.Refined
import eu.timepit.refined.auto._
import eu.timepit.refined.types.string.NonEmptyString
import javax.inject.{Inject, Singleton}
import modules.core.projections.{MonetaryAccountRepository, MonetaryAccountRow}
import org.reactivestreams.Publisher
import streaming.AkkaStreamImplicits._

import scala.concurrent.Future

case class MonetaryAccountConfiguration(
    monetaryAccountId: types.MonetaryAccountId,
    joint: Boolean
)

case class MonetaryAccountCluster(
    id: types.MonetaryAccountClusterId,
    clusterType: MonetaryAccountClusterType,
    name: NonEmptyString,
    monetaryAccounts: Seq[MonetaryAccountConfiguration],
    selected: Boolean
)

/**
  * TODO: do not depend on MonetaryAccountRepository. Rather have an own projection listening for MonetaryAccount events
  */
@Singleton
class MonetaryAccountClusterService @Inject() (
    monetaryAccountRepository: MonetaryAccountRepository,
    userSelectionRepository: UserSelectionRepository,
    monetaryAccountClusterRepository: MonetaryAccountClusterRepository
)(implicit mat: Materializer) {
  def selectMonetaryAccountCluster(userId: types.UserId, clusterId: types.MonetaryAccountClusterId): Future[Unit] =
    userSelectionRepository.selectClusterForUser(userId, clusterId)

  def fetchAllByUser(userId: types.UserId): Publisher[MonetaryAccountCluster] = {
    val selectedCluster = userSelectionRepository.fetchSelectedCluster(userId).map(clusterId => Some(clusterId)).orElse(Source.single(None))

    monetaryAccountRepository
      .fetchAllByUser(userId)
      .grouped(Int.MaxValue)
      .zip(selectedCluster)
      .zip(
        monetaryAccountClusterRepository.fetchByUser(userId).grouped(Int.MaxValue).orElse(Source.single(Seq[MonetaryAccountClusterRow]()))
      )
      .map {
        case ((allAccounts, selectedCluster), clusters: Seq[MonetaryAccountClusterRow]) =>
          allAccountsCluster(allAccounts, selectedCluster) +: buildClusters(clusters, selectedCluster)
      }
      .mapConcat(x => x)
  }

  private def allAccountsCluster(
      allAccounts: Seq[MonetaryAccountRow],
      selectedCluster: Option[types.MonetaryAccountClusterId]
  ): MonetaryAccountCluster = {
    val id: types.MonetaryAccountClusterId = Refined.unsafeApply(UUID.nameUUIDFromBytes("all".getBytes()).toString)

    MonetaryAccountCluster(
      id = id,
      clusterType = MonetaryAccountClusterType.All,
      name = "All",
      selected = selectedCluster.contains(id) || selectedCluster.isEmpty,
      monetaryAccounts = allAccounts.map(account => configFor(account.id, joint = account.joint))
    )
  }

  private def buildClusters(
      clusters: Seq[MonetaryAccountClusterRow],
      selectedCluster: Option[types.MonetaryAccountClusterId]
  ): Seq[MonetaryAccountCluster] =
    clusters.map(cluster =>
      MonetaryAccountCluster(
        id = cluster.id,
        clusterType = MonetaryAccountClusterType.UserDefined,
        name = cluster.name,
        selected = selectedCluster.contains(cluster.id),
        monetaryAccounts = cluster.configurations
          .map(c =>
            MonetaryAccountConfiguration(
              monetaryAccountId = c.monetaryAccountId,
              joint = c.joint
            )
          )
          .toSeq
      )
    )

  private def configFor(accountUuid: String, joint: Boolean) =
    MonetaryAccountConfiguration(
      monetaryAccountId = Refined.unsafeApply(UUID.fromString(accountUuid).toString),
      joint = joint
    )
}

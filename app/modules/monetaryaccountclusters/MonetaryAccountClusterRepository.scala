package modules.monetaryaccountclusters

import eu.timepit.refined.api.Refined
import eu.timepit.refined.boolean.And
import eu.timepit.refined.collection.{MaxSize, NonEmpty}
import javax.inject.{Inject, Singleton}
import org.reactivestreams.Publisher
import persistence.DatabaseExecutionContext
import persistence.ScalalikejdbcImplicits._
import play.api.Logging
import play.api.libs.json.{Format, Json}
import scalikejdbc.streams._
import scalikejdbc.{DB, SQLSyntax, WrappedResultSet}
import serde.{Deserializable, Serializable}

case class MonetaryAccountClusterConfiguration(
    monetaryAccountId: types.MonetaryAccountId,
    joint: Boolean
)
object MonetaryAccountClusterConfiguration {
  import de.cbley.refined.play.json._
  implicit val format: Format[MonetaryAccountClusterConfiguration] = Json.format
}

case class MonetaryAccountClusterRow(
    id: types.MonetaryAccountClusterId,
    userId: types.UserId,
    name: String Refined And[NonEmpty, MaxSize[60]],
    configurations: Set[MonetaryAccountClusterConfiguration]
)

object MonetaryAccountClusterRow {
  import MonetaryAccountClusterConfiguration._
  import serde.PlayJsonDeserialization._

  def fromResultSet(rs: WrappedResultSet): MonetaryAccountClusterRow = MonetaryAccountClusterRow(
    id = rs.get("id"),
    name = rs.get("name"),
    userId = rs.get("user_id"),
    configurations = Deserializable
      .deserialize[Set[MonetaryAccountClusterConfiguration]](rs.string("configurations").getBytes())
      .getOrElse(Set[MonetaryAccountClusterConfiguration]())
  )
}

trait MonetaryAccountClusterRepository {
  def fetchByUser(userId: types.UserId): Publisher[MonetaryAccountClusterRow]
  def upsertForUser(userId: types.UserId, row: MonetaryAccountClusterRow): Unit
}

@Singleton
private[monetaryaccountclusters] class PostgresMonetaryAccountClusterRepository @Inject() ()(implicit ec: DatabaseExecutionContext)
    extends MonetaryAccountClusterRepository
    with Logging {

  val tableName     = "monetary_account_cluster"
  private val table = SQLSyntax.createUnsafely(tableName)

  override def fetchByUser(userId: types.UserId): Publisher[MonetaryAccountClusterRow] =
    DB.readOnlyStream {
      sqlr"select * from $table where user_id = $userId"
        .map(MonetaryAccountClusterRow.fromResultSet)
        .iterator()
    }

  override def upsertForUser(userId: types.UserId, row: MonetaryAccountClusterRow): Unit = DB.localTx { implicit session =>
    import MonetaryAccountClusterConfiguration._
    import serde.PlayJsonSerialization._

    sqlr"""
      INSERT INTO $table(id, user_id, name, configurations, created)
      VALUES(${row.id}, ${row.userId}, ${row.name}, ${Serializable.serialize(row.configurations)}, now())
      ON CONFLICT (id)
      DO UPDATE SET
        name = ${row.name},
        configurations = ${Serializable.serialize(row.configurations)},
        updated = now()""".update
      .apply()
  }
}

package modules.monetaryaccountclusters

import play.api.inject.{Binding, Module}
import play.api.{Configuration, Environment}

class MonetaryAccountClustersModule extends Module {
  override def bindings(environment: Environment, configuration: Configuration): Seq[Binding[_]] = Seq(
    bind[MonetaryAccountClusterService].toSelf,
    bind[UserSelectionRepository].to[PostgresUserSelectionRepository],
    bind[MonetaryAccountClusterRepository].to[PostgresMonetaryAccountClusterRepository]
  )
}

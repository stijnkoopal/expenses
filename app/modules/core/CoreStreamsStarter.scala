package modules.core

import akka.Done
import akka.actor.CoordinatedShutdown
import akka.stream.KillSwitches
import javax.inject.{Inject, Singleton}
import modules.core.projections.{PostgresBalanceProjection, PostgresMonetaryAccountProjection, PostgresTransactionsProjection}
import modules.core.streaming.reactors.{MessagesToCommandsReactor, PublishEventsReactor, RefreshOnConnectReactor}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class CoreStreamsStarter @Inject() (
    monetaryAccountProjection: PostgresMonetaryAccountProjection,
    balanceProjection: PostgresBalanceProjection,
    documentMessagesToCommandsReactor: MessagesToCommandsReactor,
    publishEventsReactor: PublishEventsReactor,
    transactionsProjection: PostgresTransactionsProjection,
    refreshOnConnectReactor: RefreshOnConnectReactor,
    cs: CoordinatedShutdown
)(implicit ec: ExecutionContext) {
  private val killSwitch = KillSwitches.shared("MonetaryAccounts")

  private val futures = List(
    monetaryAccountProjection.start(killSwitch),
    balanceProjection.start(killSwitch),
    documentMessagesToCommandsReactor.start(killSwitch),
    publishEventsReactor.start(killSwitch),
    transactionsProjection.start(killSwitch),
    refreshOnConnectReactor.start(killSwitch)
  )

  cs.addTask(CoordinatedShutdown.PhaseServiceUnbind, "stop-monetary-account-streams") { () =>
    killSwitch.shutdown()
    Future.sequence(futures).map(_ => Done)
  }
}

package modules.core.domain

import eu.timepit.refined.types.string.NonEmptyString
import eu.timepit.refined.auto._
import javax.inject.Singleton
import modules.core.domain

case class MonetaryAccountConfiguration(
    monetaryAccountId: types.MonetaryAccountId,
    joint: Boolean,
    iban: Option[types.IbanString]
)

case class MinimalTransaction(
    payeeIban: Option[types.IbanString],
    payeeName: Option[NonEmptyString],
    payerIban: Option[types.IbanString],
    payerName: Option[NonEmptyString],
    monetaryAccountId: types.MonetaryAccountId,
    direction: types.TransactionDirection,
    description: String
)

trait TransactionVisibilityResolver {
  def resolveDirection(transaction: MinimalTransaction, accounts: Set[MonetaryAccountConfiguration]): Option[types.TransactionDirection]
}

@Singleton
private[core] class SimpleTransactionVisibilityResolver extends TransactionVisibilityResolver {
  private val categorizers = Seq[Categorizer](
    InternalCategorizer,
    ExternalCategorizer,
    DegiroCategorizer,
    EyevestorCategorizer,
    BinckBankCategorizer
  )

  override def resolveDirection(
      transaction: domain.MinimalTransaction,
      accounts: Set[MonetaryAccountConfiguration]
  ): Option[types.TransactionDirection] = {
    val payeeAccount = Categorizer.findPayee(transaction, accounts)
    val payerAccount = Categorizer.findPayer(transaction, accounts)

    val hidden =
      categorizers.foldLeft(false)((isHidden, categorizer) => isHidden || categorizer.isHidden(transaction, payeeAccount, payerAccount))

    if (hidden) None
    else
      (payeeAccount, payerAccount) match {
        case (Some(_), Some(_)) => None
        case (Some(_), None)    => Some(types.TransactionDirection.Inbound)
        case (None, Some(_))    => Some(types.TransactionDirection.Outbound)
        case _                  => Some(transaction.direction)
      }
  }
}

trait Categorizer {
  def isHidden(
      transaction: domain.MinimalTransaction,
      payeeAccount: Option[MonetaryAccountConfiguration],
      payerAccount: Option[MonetaryAccountConfiguration]
  ): Boolean
}

object Categorizer {
  def findPayee(transaction: domain.MinimalTransaction, accounts: Set[MonetaryAccountConfiguration]): Option[MonetaryAccountConfiguration] =
    if (transaction.direction == types.TransactionDirection.Inbound) {
      accounts.find(_.monetaryAccountId == transaction.monetaryAccountId)
    } else {
      transaction.payeeIban
        .flatMap(iban =>
          accounts.find(_.iban match {
            case Some(x) if x == iban => true
            case _                    => false
          }))
    }

  def findPayer(
      transaction: domain.MinimalTransaction,
      accounts: Set[MonetaryAccountConfiguration]
  ): Option[MonetaryAccountConfiguration] = {
    if (transaction.direction == types.TransactionDirection.Outbound) {
      accounts.find(_.monetaryAccountId == transaction.monetaryAccountId)
    } else {
      transaction.payerIban
        .flatMap(iban =>
          accounts.find(_.iban match {
            case Some(x) if x == iban => true
            case _                    => false
          }))
    }
  }
}

object InternalCategorizer extends Categorizer {
  override def isHidden(
      transaction: domain.MinimalTransaction,
      payeeAccount: Option[MonetaryAccountConfiguration],
      payerAccount: Option[MonetaryAccountConfiguration]
  ): Boolean = {
    val payeeIsInternal = payeeAccount.isDefined
    val payerIsInternal = payerAccount.isDefined

    payeeIsInternal && payerIsInternal
  }
}

object ExternalCategorizer extends Categorizer {
  override def isHidden(
      transaction: domain.MinimalTransaction,
      payeeAccount: Option[MonetaryAccountConfiguration],
      payerAccount: Option[MonetaryAccountConfiguration]
  ): Boolean = {
    val payeeIsExternal = payeeAccount.isEmpty
    val payerIsExternal = payerAccount.isEmpty

    payeeIsExternal && payerIsExternal
  }
}

object DegiroCategorizer extends Categorizer {
  override def isHidden(
      transaction: domain.MinimalTransaction,
      payeeAccount: Option[MonetaryAccountConfiguration],
      payerAccount: Option[MonetaryAccountConfiguration]
  ): Boolean = {
    val payeeName = transaction.payeeName.map(_.toLowerCase).getOrElse("")
    val payerName = transaction.payerName.map(_.toLowerCase).getOrElse("")

    val payeeDegiro = payeeName.contains("stichting de giro") ||
      payeeName.contains("stichting degiro") ||
      (payeeName.contains("flatex bank") && transaction.description.toLowerCase.contains("cash order"))

    val payerDegiro = payerName.contains("stichting de giro") ||
      payerName.contains("stichting degiro") ||
      (payeeName.contains("flatex bank") && transaction.description.toLowerCase.contains("cash order"))

    payeeDegiro || payerDegiro
  }
}

object EyevestorCategorizer extends Categorizer {
  override def isHidden(
      transaction: domain.MinimalTransaction,
      payeeAccount: Option[MonetaryAccountConfiguration],
      payerAccount: Option[MonetaryAccountConfiguration]
  ): Boolean = {
    val payeeName = transaction.payeeName.map(_.toLowerCase).getOrElse("")
    val payerName = transaction.payerName.map(_.toLowerCase).getOrElse("")

    val payeeEyevestor = payeeName.contains("eyevestor")
    val payerEyevestor = payerName.contains("eyevestor")

    payeeEyevestor || payerEyevestor
  }
}

object BinckBankCategorizer extends Categorizer {
  override def isHidden(
      transaction: MinimalTransaction,
      payeeAccount: Option[MonetaryAccountConfiguration],
      payerAccount: Option[MonetaryAccountConfiguration]
  ): Boolean = {
    val payeeName = transaction.payeeName.map(_.toLowerCase).getOrElse("")
    val payerName = transaction.payerName.map(_.toLowerCase).getOrElse("")

    val payeeBinck = payeeName.contains("binckbank")
    val payerBinck = payerName.contains("binckbank")

    payeeBinck || payerBinck
  }
}

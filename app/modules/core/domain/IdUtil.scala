package modules.core.domain

import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.time.{Instant, ZoneId}
import java.util.UUID

import eu.timepit.refined.api.Refined
import eu.timepit.refined.auto._

object IdUtil {
  def calculateTransactionId(
      payerIban: Option[types.IbanString],
      payeeIban: Option[types.IbanString],
      amount: BigDecimal,
      description: String,
      transactionDate: Instant
  ): types.TransactionId = {
    val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm").withZone(ZoneId.of("UTC"))
    val truncated     = truncateDateTime(transactionDate)

    val seed =
      s"${payerIban.getOrElse("")}-${payeeIban.getOrElse("")}-${amount.toString}-$description-${dateFormatter.format(truncated)}"
    Refined.unsafeApply(UUID.nameUUIDFromBytes(seed.getBytes("UTF-8")).toString)
  }

  def calculateMonetaryAccountId(
      institution: types.Institution,
      idAtInstitution: Any
  ): types.MonetaryAccountId = {
    val seed = institution + idAtInstitution.toString
    Refined.unsafeApply(UUID.nameUUIDFromBytes(seed.getBytes("UTF-8")).toString)
  }

  def calculateRecurringTransactionId(
      institution: types.Institution,
      amount: BigDecimal,
      creditSchemeId: String
  ): types.RecurringTransactionId = {
    val seed = s"$institution-$creditSchemeId-$amount"
    Refined.unsafeApply(UUID.nameUUIDFromBytes(seed.getBytes("UTF-8")).toString)
  }

  def calculateRecurringScheduleId(
      institution: types.Institution,
      monetaryAccountId: types.MonetaryAccountId,
      amount: BigDecimal,
      payeeIban: Option[types.IbanString],
      description: String
  ): types.RecurringScheduleId = {
    val seed = s"$institution-$monetaryAccountId-$amount-${payeeIban.getOrElse("")}-$description"
    Refined.unsafeApply(UUID.nameUUIDFromBytes(seed.getBytes("UTF-8")).toString)
  }

  private def round(i: Long, v: Int): Long = (i.toFloat / v.toFloat).round * v

  private def truncateDateTime(time: Instant): Instant =
    Instant.ofEpochSecond(round(time.truncatedTo(ChronoUnit.SECONDS).getEpochSecond, 10))
}

package modules.core.domain

import modules.core.messaging.{MonetaryAccountDocument, TransactionDocument}

trait MonetaryAccountService {
  def processMonetaryAccountDocument(document: MonetaryAccountDocument): Unit
  def processTransactionDocument(document: TransactionDocument): Unit
  def updateBalance(monetaryAccountId: types.MonetaryAccountId, newBalance: BigDecimal): Unit
}

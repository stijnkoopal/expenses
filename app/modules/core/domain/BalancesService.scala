package modules.core.domain

import java.time.Instant

import modules.core.projections.Balance
import org.reactivestreams.Publisher

trait BalancesService {
  def fetchMonthlyBalancesBetween(
      userId: types.UserId,
      monetaryAccountIds: Seq[types.MonetaryAccountId],
      startDate: Instant,
      endDate: Instant
  ): Publisher[Balance]

  def fetchLatestFor(monetaryAccountIds: Set[types.MonetaryAccountId]): Publisher[Balance]
}

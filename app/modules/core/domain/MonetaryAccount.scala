package modules.core.domain

import java.time.Instant
import java.util.Currency

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import akka.cluster.sharding.typed.scaladsl.EntityTypeKey
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.{Effect, EventSourcedBehavior}
import eu.timepit.refined.types.string.NonEmptyString
import play.api.Logging
import serde.{CommandSerializable, EventSerializable, SnapshotSerializable}
import types._

object MonetaryAccount {

  val TypeKey: EntityTypeKey[Command] =
    EntityTypeKey[Command]("MonetaryAccount")

  def apply(entityId: MonetaryAccountId): Behavior[Command] = {
    val persistenceId = PersistenceId(entityTypeHint = TypeKey.name, entityId = entityId.toString)
    Behaviors.setup { context =>
      new MonetaryAccount(persistenceId, entityId).start()
    }
  }

  trait Command extends CommandSerializable

  case class ProcessMonetaryAccount(
      iban: IbanString,
      joint: Option[Boolean],
      ownerUserId: UserId,
      alias: Option[NonEmptyString],
      institution: Institution,
      currency: Currency,
      balance: Option[BigDecimal],
      fetchDateTime: Instant
  ) extends Command

  sealed trait ProcessTransaction extends Command {
    def id: TransactionId
  }

  case class ProcessInboundTransaction(
      id: TransactionId,
      amount: BigDecimal,
      currency: Currency,
      description: String,
      transactionDate: Instant,
      payee: TransactionParty,
      payer: TransactionParty,
      balanceAfterMutation: Option[BigDecimal]
  ) extends ProcessTransaction

  case class ProcessOutboundTransaction(
      id: TransactionId,
      amount: BigDecimal,
      currency: Currency,
      description: String,
      partOfScheduleId: Option[RecurringScheduleId],
      transactionDate: Instant,
      payee: TransactionParty,
      payer: TransactionParty,
      balanceAfterMutation: Option[BigDecimal]
  ) extends ProcessTransaction

  case class UpdateBalanceForNonAutomatedAccount(
      newBalance: BigDecimal,
      balanceTimestamp: Instant
  ) extends Command

  trait Event extends EventSerializable {
    def accountId: MonetaryAccountId
  }

  case class NewMonetaryAccountFound(
      accountId: MonetaryAccountId,
      iban: IbanString,
      joint: Option[Boolean],
      ownerUserId: UserId,
      alias: Option[NonEmptyString],
      institution: Institution,
      currency: Currency,
      balance: Option[BigDecimal]
  ) extends Event

  case class MonetaryAccountBecameJoint(
      accountId: MonetaryAccountId
  ) extends Event

  case class MonetaryAccountBecameSingular(
      accountId: MonetaryAccountId
  ) extends Event

  case class MonetaryAccountAliasUpdated(
      accountId: MonetaryAccountId,
      newAlias: Option[NonEmptyString]
  ) extends Event
  case class MonetaryAccountBalanceChanged(
      accountId: MonetaryAccountId,
      newBalance: BigDecimal,
      balanceTimestamp: Instant
  ) extends Event

  case class MonetaryAccountUserAdded(
      accountId: MonetaryAccountId,
      userId: UserId
  ) extends Event

  sealed trait NewTransactionFound extends Event

  case class NewInboundTransactionFound(
      id: TransactionId,
      accountId: MonetaryAccountId,
      amount: BigDecimal,
      currency: Currency,
      description: String,
      payer: TransactionParty,
      transactionDate: Instant,
      balanceAfterMutation: Option[BigDecimal]
  ) extends NewTransactionFound

  case class NewOutboundTransactionFound(
      id: TransactionId,
      accountId: MonetaryAccountId,
      amount: BigDecimal,
      currency: Currency,
      description: String,
      partOfScheduleId: Option[RecurringScheduleId],
      payee: TransactionParty,
      payer: TransactionParty,
      transactionDate: Instant,
      balanceAfterMutation: Option[BigDecimal]
  ) extends NewTransactionFound

  sealed trait Transaction {
    def id: TransactionId
  }

  case class InboundTransaction(
      id: TransactionId,
      amount: BigDecimal,
      currency: Currency,
      description: String,
      transactionDate: Instant,
      payer: TransactionParty
  ) extends Transaction

  case class OutboundTransaction(
      id: TransactionId,
      amount: BigDecimal,
      currency: Currency,
      description: String,
      partOfScheduleId: Option[RecurringScheduleId],
      transactionDate: Instant,
      payee: TransactionParty
  ) extends Transaction

  case class MonetaryAccountData(
      iban: IbanString,
      joint: Option[Boolean],
      alias: Option[NonEmptyString],
      automated: Boolean,
      institution: Institution,
      currency: Currency,
      found: Instant
  )

  case class BalanceInPast(
      balance: BigDecimal,
      timestamp: Instant
  )

  case class State(
      id: MonetaryAccountId,
      data: Option[MonetaryAccountData],
      ownerUserIds: Set[UserId],
      transactions: Seq[Transaction],
      balanceHistory: Seq[BalanceInPast]
  ) extends SnapshotSerializable {
    def latestBalance: Option[BalanceInPast] = balanceHistory.lastOption
  }

  object State {
    def empty(id: MonetaryAccountId): State = State(
      id = id,
      data = None,
      transactions = Seq(),
      ownerUserIds = Set(),
      balanceHistory = Seq()
    )
  }

  case class TransactionParty(iban: Option[IbanString], name: Option[NonEmptyString])
}

class MonetaryAccount(persistenceId: PersistenceId, entityId: types.MonetaryAccountId) extends Logging {
  import MonetaryAccount._

  private def start(): Behavior[Command] =
    EventSourcedBehavior(persistenceId, emptyState = State.empty(entityId), commandHandler, eventHandler)

  val commandHandler: (State, Command) => Effect[Event, State] = { (state, cmd) =>
    cmd match {
      case cmd: ProcessMonetaryAccount              => processCommand(state, cmd)
      case cmd: ProcessInboundTransaction           => processCommand(state, cmd)
      case cmd: ProcessOutboundTransaction          => processCommand(state, cmd)
      case cmd: UpdateBalanceForNonAutomatedAccount => processCommand(state, cmd)
    }
  }

  val eventHandler: (State, Event) => State = { (state, evt) =>
    evt match {
      case evt: NewMonetaryAccountFound       => processEvent(state, evt)
      case evt: MonetaryAccountBecameJoint    => processEvent(state, evt)
      case evt: MonetaryAccountBecameSingular => processEvent(state, evt)
      case evt: MonetaryAccountAliasUpdated   => processEvent(state, evt)
      case evt: MonetaryAccountBalanceChanged => processEvent(state, evt)
      case evt: MonetaryAccountUserAdded      => processEvent(state, evt)
      case evt: NewTransactionFound           => processEvent(state, evt)
    }
  }

  def processCommand(state: State, cmd: ProcessMonetaryAccount): Effect[Event, State] = {
    val newFoundAccountEvent = state.data match {
      case None =>
        Some(
          NewMonetaryAccountFound(
            accountId = state.id,
            iban = cmd.iban,
            joint = cmd.joint,
            institution = cmd.institution,
            ownerUserId = cmd.ownerUserId,
            alias = cmd.alias,
            currency = cmd.currency,
            balance = cmd.balance
          )
        )

      case _ => None
    }

    // TOOD: required?
    val balanceUpdatedEvent = cmd.balance.map(
      balance =>
        MonetaryAccountBalanceChanged(
          accountId = state.id,
          newBalance = balance,
          balanceTimestamp = cmd.fetchDateTime
      ))

    val becameJointEvent = (state.data.flatMap(_.joint), cmd.joint) match {
      case (Some(stateJoint), Some(cmdJoint)) if stateJoint && !cmdJoint =>
        Some(
          MonetaryAccountBecameSingular(
            accountId = state.id
          )
        )

      case (Some(stateJoint), Some(cmdJoint)) if !stateJoint && cmdJoint =>
        Some(
          MonetaryAccountBecameJoint(
            accountId = state.id
          )
        )

      case (None, Some(cmdJoint)) if cmdJoint =>
        Some(
          MonetaryAccountBecameJoint(
            accountId = state.id
          )
        )

      case (None, Some(cmdJoint)) if !cmdJoint =>
        Some(
          MonetaryAccountBecameSingular(
            accountId = state.id
          )
        )

      case _ => None
    }

    val aliasUpdatedEvent = state.data.map(_.alias) match {
      case Some(alias) if alias != cmd.alias =>
        Some(
          MonetaryAccountAliasUpdated(
            accountId = state.id,
            newAlias = cmd.alias
          )
        )

      case None =>
        Some(
          MonetaryAccountAliasUpdated(
            accountId = state.id,
            newAlias = cmd.alias
          )
        )

      case _ => None
    }

    val newUserIdAttached =
      if (state.ownerUserIds.contains(cmd.ownerUserId)) None
      else
        Some(
          MonetaryAccountUserAdded(
            accountId = state.id,
            userId = cmd.ownerUserId
          )
        )

    val events = newFoundAccountEvent.toSeq ++ balanceUpdatedEvent.toSeq ++ becameJointEvent.toSeq ++ aliasUpdatedEvent.toSeq ++ newUserIdAttached.toSeq

    Effect.persist(events).thenNoReply()
  }

  def processCommand(state: State, cmd: ProcessTransaction): Effect[Event, State] = {
    if (state.transactions.exists(_.id == cmd.id)) {
      return Effect.none
    }

    val event = cmd match {
      case cmd: ProcessInboundTransaction =>
        NewInboundTransactionFound(
          id = cmd.id,
          accountId = state.id,
          amount = cmd.amount,
          currency = cmd.currency,
          description = cmd.description,
          transactionDate = cmd.transactionDate,
          payer = cmd.payer,
          balanceAfterMutation = cmd.balanceAfterMutation
        )

      case cmd: ProcessOutboundTransaction =>
        NewOutboundTransactionFound(
          id = cmd.id,
          accountId = state.id,
          amount = cmd.amount,
          currency = cmd.currency,
          description = cmd.description,
          partOfScheduleId = cmd.partOfScheduleId,
          transactionDate = cmd.transactionDate,
          payee = cmd.payee,
          payer = cmd.payer,
          balanceAfterMutation = cmd.balanceAfterMutation
        )
    }

    Effect.persist(event).thenNoReply()
  }

  def processCommand(state: State, cmd: UpdateBalanceForNonAutomatedAccount): Effect[Event, State] = {
    if (state.data.exists(_.automated)) {
      Effect.none
    } else {
      val event = MonetaryAccountBalanceChanged(
        accountId = state.id,
        newBalance = cmd.newBalance,
        balanceTimestamp = cmd.balanceTimestamp
      )

      Effect.persist(event).thenNoReply()
    }
  }

  def processEvent(state: State, evt: NewMonetaryAccountFound): State =
    state.copy(
      ownerUserIds = Set(evt.ownerUserId),
      data = Some(
        MonetaryAccountData(
          iban = evt.iban,
          joint = evt.joint,
          alias = evt.alias,
          currency = evt.currency,
          institution = evt.institution,
          found = Instant.now(), // TODO: clock
          automated = true
        )
      )
    )

  def processEvent(state: State, evt: MonetaryAccountBecameJoint): State =
    state.copy(
      data = state.data.map(data => data.copy(joint = Some(true)))
    )

  def processEvent(state: State, evt: MonetaryAccountBecameSingular): State =
    state.copy(
      data = state.data.map(data => data.copy(joint = Some(false)))
    )

  def processEvent(state: State, evt: MonetaryAccountAliasUpdated): State =
    state.copy(
      data = state.data.map(data => data.copy(alias = evt.newAlias))
    )

  def processEvent(state: State, evt: MonetaryAccountBalanceChanged): State = {
    val newHistory = state.balanceHistory :+ BalanceInPast(balance = evt.newBalance, timestamp = evt.balanceTimestamp)

    state.copy(
      balanceHistory = newHistory.sortBy(_.timestamp)
    )
  }

  def processEvent(state: State, evt: MonetaryAccountUserAdded): State =
    state.copy(
      ownerUserIds = state.ownerUserIds + evt.userId
    )

  def processEvent(state: State, evt: NewTransactionFound): State = {
    val transaction = evt match {
      case evt: NewInboundTransactionFound =>
        InboundTransaction(
          id = evt.id,
          amount = evt.amount,
          currency = evt.currency,
          description = evt.description,
          transactionDate = evt.transactionDate,
          payer = evt.payer
        )

      case evt: NewOutboundTransactionFound =>
        OutboundTransaction(
          id = evt.id,
          amount = evt.amount,
          currency = evt.currency,
          description = evt.description,
          partOfScheduleId = evt.partOfScheduleId,
          transactionDate = evt.transactionDate,
          payee = evt.payee
        )
    }

    state.copy(
      transactions = state.transactions :+ transaction
    )
  }
}

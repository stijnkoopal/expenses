package modules.core.messaging

import java.time.Instant
import java.util.{Currency, UUID}

import eu.timepit.refined.types.string.NonEmptyString
import play.api.libs.json.{Format, Json}
import serde.{PlayJsonDefaultDeserialization, PlayJsonDefaultSerialization}
import types._

case class MonetaryAccountRefreshStarted(
    id: MonetaryAccountId
)
object MonetaryAccountRefreshStarted {
  val payloadType = "monetaryAccountRefreshStarted"
}

case class MonetaryAccountRefreshDone(
    id: MonetaryAccountId,
    lastTransactionSync: Instant
)
object MonetaryAccountRefreshDone {
  val payloadType = "monetaryAccountRefreshDone"
}

case class AllMonetaryAccountsRefreshRequested(
    userId: UserId,
    timestamp: Instant
)

object AllMonetaryAccountsRefreshRequested {
  val payloadType = "allMonetaryAccountsRefreshRequested"
}

case class MonetaryAccountDocument(
    id: MonetaryAccountId,
    connectionId: UUID,
    userId: UserId,
    iban: Option[IbanString],
    institution: Institution,
    alias: Option[NonEmptyString],
    currency: String,
    balance: Option[BigDecimal],
    joint: Option[Boolean],
    fetchDateTime: Instant
)

object MonetaryAccountDocument {
  val payloadType = "monetaryAccountDocument"
}

case class NewMonetaryAccountFoundMsg(
    id: MonetaryAccountId,
    iban: IbanString,
    userId: UserId,
    institution: Institution,
    currency: Currency,
    balance: Option[BigDecimal],
    joint: Option[Boolean]
)
object NewMonetaryAccountFoundMsg {
  val payloadType = "newMonetaryAccountFoundMsg"
}

case class MonetaryAccountBecameJointMsg(
    monetaryAccountId: MonetaryAccountId
)
object MonetaryAccountBecameJointMsg {
  val payloadType = "monetaryAccountBecameJointMsg"
}

case class MonetaryAccountBecameSingularMsg(
    monetaryAccountId: MonetaryAccountId
)
object MonetaryAccountBecameSingularMsg {
  val payloadType = "MonetaryAccountBecameSingularMsg"
}

case class MonetaryAccountAliasUpdatedMsg(
    monetaryAccountId: MonetaryAccountId,
    newAlias: Option[NonEmptyString]
)
object MonetaryAccountAliasUpdatedMsg {
  val payloadType = "MonetaryAccountAliasUpdatedMsg"
}

case class MonetaryAccountBalanceChangedMsg(
    monetaryAccountId: MonetaryAccountId,
    balance: BigDecimal,
    timestamp: Instant
)

object MonetaryAccountBalanceChangedMsg {
  val payloadType = "MonetaryAccountBalanceChangedMsg"
}

case class MonetaryAccountUserAddedMsg(
    monetaryAccountId: MonetaryAccountId,
    userId: UserId
)

object MonetaryAccountUserAddedMsg {
  val payloadType = "monetaryAccountUserAddedMsg"
}

object MonetaryAccountSerde extends PlayJsonDefaultSerialization with PlayJsonDefaultDeserialization {
  import de.cbley.refined.play.json._

  implicit val mapFormat: Format[Map[UUID, BigDecimal]]                                       = Format(customMapReads, customMapWrites())
  implicit val allAccountsRefreshRequestedFormat: Format[AllMonetaryAccountsRefreshRequested] = Json.format
  implicit val monetaryAccountDocumentFormat: Format[MonetaryAccountDocument]                 = Json.format
  implicit val accountRefreshStartedFormat: Format[MonetaryAccountRefreshStarted]             = Json.format
  implicit val accountRefreshDoneFormat: Format[MonetaryAccountRefreshDone]                   = Json.format

  implicit val newMonetaryAccountFoundMsg: Format[NewMonetaryAccountFoundMsg]                   = Json.format
  implicit val monetaryAccountBalanceChangedMsg: Format[MonetaryAccountBalanceChangedMsg]       = Json.format
  implicit val monetaryAccountBecameJointMsgFormat: Format[MonetaryAccountBecameJointMsg]       = Json.format
  implicit val monetaryAccountBecameSingularMsgFormat: Format[MonetaryAccountBecameSingularMsg] = Json.format
  implicit val monetaryAccountAliasUpdatedMsg: Format[MonetaryAccountAliasUpdatedMsg]           = Json.format
  implicit val monetaryAccountUserAddedMsg: Format[MonetaryAccountUserAddedMsg]                 = Json.format
}

object MonetaryAccountMessaging {
  val channel = "monetary-account"
}

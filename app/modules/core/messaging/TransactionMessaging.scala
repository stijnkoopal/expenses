package modules.core.messaging

import java.time.Instant
import java.util.Currency

import eu.timepit.refined.types.string.NonEmptyString
import play.api.libs.json._
import serde.{PlayJsonDefaultDeserialization, PlayJsonDefaultSerialization}
import types._

case class TransactionPartyInMessage(
    iban: Option[IbanString],
    name: Option[NonEmptyString]
)

case class TransactionDocument(
    id: types.TransactionId,
    userId: UserId,
    monetaryAccountId: MonetaryAccountId,
    currency: Currency,
    amount: BigDecimal,
    payer: TransactionPartyInMessage,
    payee: TransactionPartyInMessage,
    description: String,
    direction: types.TransactionDirection,
    scheduleId: Option[RecurringScheduleId],
    balanceAfterMutation: Option[BigDecimal],
    timestamp: Instant
)

object TransactionDocument {
  val payloadType = "transactionDocument"
}

sealed trait NewTransactionFoundMsg {
  def accountId: MonetaryAccountId
  def transactionDate: Instant
  def amount: BigDecimal
  def balanceAfterMutation: Option[BigDecimal]
}
case class NewInboundTransactionFoundMsg(
    id: TransactionId,
    accountId: MonetaryAccountId,
    amount: BigDecimal,
    currency: Currency,
    description: String,
    transactionDate: Instant,
    payer: TransactionPartyInMessage,
    balanceAfterMutation: Option[BigDecimal]
) extends NewTransactionFoundMsg
object NewInboundTransactionFoundMsg {
  val payloadType = "NewInboundTransactionFoundMsg"
}

case class NewOutboundTransactionFoundMsg(
    id: TransactionId,
    accountId: MonetaryAccountId,
    amount: BigDecimal,
    currency: Currency,
    description: String,
    partOfScheduleId: Option[RecurringScheduleId],
    transactionDate: Instant,
    payee: TransactionPartyInMessage,
    payer: TransactionPartyInMessage,
    balanceAfterMutation: Option[BigDecimal]
) extends NewTransactionFoundMsg
object NewOutboundTransactionFoundMsg {
  val payloadType = "NewOutboundTransactionFoundMsg"
}

object TransactionSerde extends PlayJsonDefaultSerialization with PlayJsonDefaultDeserialization {
  import de.cbley.refined.play.json._

  implicit val transactionPartyFormat: Format[TransactionPartyInMessage] = Json.format
  implicit val transactionDocumentFormat: Format[TransactionDocument]    = Json.format

  implicit val newInboundTransactionFoundMsgFormat: Format[NewInboundTransactionFoundMsg]   = Json.format
  implicit val newOutboundTransactionFoundMsgFormat: Format[NewOutboundTransactionFoundMsg] = Json.format
}

object TransactionMessaging {
  val channel = "transaction"
}

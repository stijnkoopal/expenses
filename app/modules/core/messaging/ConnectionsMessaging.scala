package modules.core.messaging

import java.time.LocalDateTime
import java.util.UUID

import play.api.libs.json.{Format, Json}
import serde.{PlayJsonDefaultDeserialization, PlayJsonDefaultSerialization}
import types.Institution

case class MonetaryAccountsOnConnectionRefreshRequested(
    connectionId: UUID,
    timestamp: LocalDateTime
)

object MonetaryAccountsOnConnectionRefreshRequested {
  val payloadType = "monetaryAccountsOnConnectionRefreshRequested"
}

case class ConnectionEstablished(
    id: UUID,
    institution: Institution
)

object ConnectionEstablished {
  val payloadType = "connectionEstablished"
}

object ConnectionSerde extends PlayJsonDefaultSerialization with PlayJsonDefaultDeserialization {
  import de.cbley.refined.play.json._

  implicit val connectionEstablishedFormat: Format[ConnectionEstablished]                                               = Json.format
  implicit val monetaryAccountsOnConnectionRefreshRequestedFormat: Format[MonetaryAccountsOnConnectionRefreshRequested] = Json.format
}

object ConnectionMessaging {
  val channel = "connection"
}

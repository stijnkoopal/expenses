package modules.core.streaming.reactors

import java.time.{Clock, LocalDateTime}

import akka.Done
import akka.stream.scaladsl.{Flow, Keep, Sink}
import akka.stream.{Materializer, SharedKillSwitch}
import javax.inject.{Inject, Singleton}
import messaging.{MessagePublisher, MessageReceiver}
import modules.core.messaging.{ConnectionEstablished, ConnectionMessaging, MonetaryAccountsOnConnectionRefreshRequested}
import play.api.Logging
import streaming.AkkaStreamImplicits._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
private[core] class RefreshOnConnectReactor @Inject() (
    clock: Clock,
    receiver: MessageReceiver,
    publisher: MessagePublisher
)(implicit val ec: ExecutionContext, mat: Materializer)
    extends Logging {
  import modules.core.messaging.ConnectionSerde._
  import serde.PlayJsonDeserialization._
  import serde.PlayJsonSerialization._

  logger.info(s"Starting to listen for 'ConnectionEstablished' messages on '${ConnectionMessaging.channel}'")
  private val source = receiver
    .receive[ConnectionEstablished](channel = ConnectionMessaging.channel, payloadType = ConnectionEstablished.payloadType)

  private val publishRequestRefresh = Flow[ConnectionEstablished]
    .map(connection => MonetaryAccountsOnConnectionRefreshRequested(connection.id, LocalDateTime.now(clock)))
    .to(
      publisher.publishTo[MonetaryAccountsOnConnectionRefreshRequested](
        ConnectionMessaging.channel,
        MonetaryAccountsOnConnectionRefreshRequested.payloadType
      )
    )

  def start(killSwitch: SharedKillSwitch): Future[Done] =
    source
      .log("Connection established")
      .alsoTo(publishRequestRefresh)
      .via(killSwitch.flow)
      .toMat(Sink.ignore)(Keep.right)
      .run()
}

package modules.core.streaming.reactors

import akka.Done
import akka.stream.scaladsl.{Flow, Keep, Sink}
import akka.stream.{Materializer, SharedKillSwitch}
import javax.inject.{Inject, Singleton}
import messaging.MessageReceiver
import modules.core.messaging.{MonetaryAccountDocument, MonetaryAccountMessaging, TransactionDocument, TransactionMessaging}
import modules.core.services.AkkaBackedMonetaryAccountService
import play.api.Logging
import streaming.AkkaStreamImplicits._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
private[core] class MessagesToCommandsReactor @Inject()(receiver: MessageReceiver, monetaryAccountService: AkkaBackedMonetaryAccountService)(
    implicit mat: Materializer, ec: ExecutionContext
) extends Logging {
  import modules.core.messaging.MonetaryAccountSerde._
  import modules.core.messaging.TransactionSerde._
  import serde.PlayJsonDeserialization._

  logger.info(s"Starting to listen for 'TransactionDocument' messages on '${TransactionMessaging.channel}'")
  private val transactionSource = receiver
    .receive[TransactionDocument](channel = TransactionMessaging.channel, payloadType = TransactionDocument.payloadType)

  logger.info(s"Starting to listen for 'MonetaryAccountDocument' messages on '${MonetaryAccountMessaging.channel}'")
  private val monetaryAccountSource = receiver
    .receive[MonetaryAccountDocument](channel = MonetaryAccountMessaging.channel, payloadType = MonetaryAccountDocument.payloadType)

  private val processTransactionDocument = Flow[TransactionDocument]
    .to(Sink.foreach(document => monetaryAccountService.processTransactionDocument(document)))

  private val processMonetaryAccountDocument = Flow[MonetaryAccountDocument]
    .to(Sink.foreach(document => monetaryAccountService.processMonetaryAccountDocument(document)))

  def start(killSwitch: SharedKillSwitch): Future[Done] = {
    val transactionFuture = transactionSource
      .alsoTo(processTransactionDocument)
      .via(killSwitch.flow)
      .toMat(Sink.ignore)(Keep.right)
      .run()

    val monetaryAccountFuture = monetaryAccountSource
      .alsoTo(processMonetaryAccountDocument)
      .via(killSwitch.flow)
      .toMat(Sink.ignore)(Keep.right)
      .run()

    Future.sequence(Seq(transactionFuture, monetaryAccountFuture)).map(_ => Done)
  }
}

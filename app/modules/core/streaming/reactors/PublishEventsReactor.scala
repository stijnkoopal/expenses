package modules.core.streaming.reactors

import akka._
import akka.actor.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity, EntityTypeKey}
import akka.persistence.jdbc.query.scaladsl.JdbcReadJournal
import akka.persistence.query.{EventEnvelope, PersistenceQuery}
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.{Effect, EventSourcedBehavior}
import akka.stream.scaladsl.{Keep, Sink, Source}
import akka.stream.{Materializer, SharedKillSwitch}
import akka.util.Timeout
import javax.inject.{Inject, Singleton}
import messaging.MessagePublisher
import modules.core.domain.MonetaryAccount
import modules.core.domain.MonetaryAccount._
import modules.core.messaging._
import modules.core.streaming.reactors.SequenceNumberStore.{GetSequenceNumber, GetSequenceNumberResponse, UpdateSequenceNumber}
import play.api.Logging
import serde.{CommandSerializable, EventSerializable, SnapshotSerializable}
import streaming.AkkaStreamImplicits._

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps

object SequenceNumberStore {

  val TypeKey: EntityTypeKey[SequenceStoreCommand] =
    EntityTypeKey[SequenceStoreCommand]("SequenceNumberStore")

  def apply(entityId: String, persistenceId: PersistenceId): Behavior[SequenceStoreCommand] = {
    Behaviors.setup { context =>
      EventSourcedBehavior(persistenceId, emptyState = State.empty(entityId), commandHandler, eventHandler)
    }
  }

  trait SequenceStoreCommand                               extends CommandSerializable
  case class UpdateSequenceNumber(newSequenceNumber: Long) extends SequenceStoreCommand

  case class GetSequenceNumber(replyTo: ActorRef[GetSequenceNumberResponse]) extends SequenceStoreCommand
  case class GetSequenceNumberResponse(sequenceNumber: Long)                 extends CommandSerializable

  case class SequenceNumberUpdated(newSequenceNumber: Long) extends EventSerializable
  case class State(storeId: String, sequenceNumber: Long)   extends SnapshotSerializable
  object State {
    def empty(storeId: String): State = State(storeId = storeId, sequenceNumber = 0)
  }

  private val commandHandler: (State, SequenceStoreCommand) => Effect[SequenceNumberUpdated, State] = { (state, cmd) =>
    cmd match {
      case UpdateSequenceNumber(newSequenceNumber) if newSequenceNumber > state.sequenceNumber =>
        Effect.persist(SequenceNumberUpdated(newSequenceNumber)).thenNoReply()

      case GetSequenceNumber(replyTo) =>
        Effect.reply(replyTo)(GetSequenceNumberResponse(sequenceNumber = state.sequenceNumber))

      case _ =>
        Effect.noReply
    }
  }

  private val eventHandler: (State, SequenceNumberUpdated) => State = { (state, evt) =>
    State(storeId = state.storeId, sequenceNumber = evt.newSequenceNumber)
  }
}

@Singleton()
private[core] class PublishEventsReactor @Inject()(
    system: ActorSystem,
    publisher: MessagePublisher,
    sharding: ClusterSharding
)(implicit mat: Materializer)
    extends Logging {
  import modules.core.messaging.MonetaryAccountSerde._
  import modules.core.messaging.TransactionSerde._
  import serde.PlayJsonSerialization._

  implicit val timeout: Timeout = 300 seconds

  sharding.init(Entity(typeKey = SequenceNumberStore.TypeKey) { entityContext =>
    SequenceNumberStore(
      entityContext.entityId,
      PersistenceId(entityTypeHint = entityContext.entityTypeKey.name, entityId = entityContext.entityId)
    )
  })

  val readJournal: JdbcReadJournal = PersistenceQuery(system).readJournalFor[JdbcReadJournal](JdbcReadJournal.Identifier)

  val source: Source[Any, NotUsed] =
    readJournal
      .persistenceIds()
      .filter(persistenceId => persistenceId.startsWith(MonetaryAccount.TypeKey.name))
      .flatMapConcat(persistenceId => {
        val entity = sharding.entityRefFor(SequenceNumberStore.TypeKey, persistenceId.replace("|", ""))
        Source
          .future(entity.ask[GetSequenceNumberResponse](ref => GetSequenceNumber(ref)))
          .map(response => (persistenceId, response))
      })
      .flatMapMerge(
        100, {
          case (persistenceId, startSequence) =>
            readJournal
              .eventsByPersistenceId(persistenceId, startSequence.sequenceNumber + 1, Long.MaxValue)
        }
      )
      .flatMapConcat {
        case EventEnvelope(_, persistenceId, sequenceNr, evt: NewMonetaryAccountFound) =>
          publish(buildMessage(evt), MonetaryAccountMessaging.channel, NewMonetaryAccountFoundMsg.payloadType, persistenceId, sequenceNr)

        case EventEnvelope(_, persistenceId, sequenceNr, evt: MonetaryAccountBalanceChanged) =>
          publish(buildMessage(evt),
                  MonetaryAccountMessaging.channel,
                  MonetaryAccountBalanceChangedMsg.payloadType,
                  persistenceId,
                  sequenceNr)

        case EventEnvelope(_, persistenceId, sequenceNr, evt: MonetaryAccountBecameJoint) =>
          publish(buildMessage(evt), MonetaryAccountMessaging.channel, MonetaryAccountBecameJointMsg.payloadType, persistenceId, sequenceNr)

        case EventEnvelope(_, persistenceId, sequenceNr, evt: MonetaryAccountBecameSingular) =>
          publish(buildMessage(evt),
                  MonetaryAccountMessaging.channel,
                  MonetaryAccountBecameSingularMsg.payloadType,
                  persistenceId,
                  sequenceNr)

        case EventEnvelope(_, persistenceId, sequenceNr, evt: MonetaryAccountAliasUpdated) =>
          publish(buildMessage(evt),
                  MonetaryAccountMessaging.channel,
                  MonetaryAccountAliasUpdatedMsg.payloadType,
                  persistenceId,
                  sequenceNr)

        case EventEnvelope(_, persistenceId, sequenceNr, evt: MonetaryAccountUserAdded) =>
          publish(buildMessage(evt), MonetaryAccountMessaging.channel, MonetaryAccountUserAddedMsg.payloadType, persistenceId, sequenceNr)

        case EventEnvelope(_, persistenceId, sequenceNr, evt: NewInboundTransactionFound) =>
          publish(buildMessage(evt), TransactionMessaging.channel, NewInboundTransactionFoundMsg.payloadType, persistenceId, sequenceNr)

        case EventEnvelope(_, persistenceId, sequenceNr, evt: NewOutboundTransactionFound) =>
          publish(buildMessage(evt), TransactionMessaging.channel, NewOutboundTransactionFoundMsg.payloadType, persistenceId, sequenceNr)

        case _ =>
          Source.empty
      }

  def start(killSwitch: SharedKillSwitch)(implicit ec: ExecutionContext): Future[Done] = {
    logger.info(s"Starting to publish some events from MonetaryAccount")
    source
      .via(killSwitch.flow)
      .toMat(Sink.ignore)(Keep.right)
      .run()
  }

  private def publish[T: serde.Serializable](message: T,
                                             channel: String,
                                             payloadType: String,
                                             persistenceId: String,
                                             sequenceNumber: Long) =
    Source
      .single(message)
      .alsoTo(publisher.publishTo[T](channel, payloadType))
      .alsoTo(
        Sink.foreach(_ => {
          val entity = sharding.entityRefFor(SequenceNumberStore.TypeKey, persistenceId.replace("|", ""))
          entity ! UpdateSequenceNumber(newSequenceNumber = sequenceNumber) // TODO: use actorRefWithBackpressure
        })
      )

  private def buildMessage(evt: MonetaryAccountBalanceChanged): MonetaryAccountBalanceChangedMsg =
    MonetaryAccountBalanceChangedMsg(
      monetaryAccountId = evt.accountId,
      balance = evt.newBalance,
      timestamp = evt.balanceTimestamp
    )

  private def buildMessage(evt: NewMonetaryAccountFound): NewMonetaryAccountFoundMsg =
    NewMonetaryAccountFoundMsg(
      id = evt.accountId,
      iban = evt.iban,
      userId = evt.ownerUserId,
      institution = evt.institution,
      currency = evt.currency,
      balance = evt.balance,
      joint = evt.joint
    )

  private def buildMessage(evt: MonetaryAccountBecameJoint): MonetaryAccountBecameJointMsg = MonetaryAccountBecameJointMsg(
    monetaryAccountId = evt.accountId
  )

  private def buildMessage(evt: MonetaryAccountBecameSingular): MonetaryAccountBecameSingularMsg = MonetaryAccountBecameSingularMsg(
    monetaryAccountId = evt.accountId
  )

  private def buildMessage(evt: MonetaryAccountAliasUpdated): MonetaryAccountAliasUpdatedMsg = MonetaryAccountAliasUpdatedMsg(
    monetaryAccountId = evt.accountId,
    newAlias = evt.newAlias
  )

  private def buildMessage(evt: MonetaryAccountUserAdded): MonetaryAccountUserAddedMsg = MonetaryAccountUserAddedMsg(
    monetaryAccountId = evt.accountId,
    userId = evt.userId
  )

  private def buildMessage(evt: NewInboundTransactionFound): NewInboundTransactionFoundMsg = NewInboundTransactionFoundMsg(
    id = evt.id,
    accountId = evt.accountId,
    amount = evt.amount,
    currency = evt.currency,
    description = evt.description,
    transactionDate = evt.transactionDate,
    payer = TransactionPartyInMessage(iban = evt.payer.iban, name = evt.payer.name),
    balanceAfterMutation = evt.balanceAfterMutation
  )

  private def buildMessage(evt: NewOutboundTransactionFound): NewOutboundTransactionFoundMsg = NewOutboundTransactionFoundMsg(
    id = evt.id,
    accountId = evt.accountId,
    amount = evt.amount,
    currency = evt.currency,
    description = evt.description,
    partOfScheduleId = evt.partOfScheduleId,
    transactionDate = evt.transactionDate,
    payee = TransactionPartyInMessage(iban = evt.payee.iban, name = evt.payee.name),
    payer = TransactionPartyInMessage(iban = evt.payer.iban, name = evt.payer.name),
    balanceAfterMutation = evt.balanceAfterMutation
  )

}

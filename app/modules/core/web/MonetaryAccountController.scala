package modules.core.web

import java.time.{Clock, Instant}

import akka.stream.Materializer
import akka.stream.scaladsl.Source
import app.auth.AuthEnforcedAction
import javax.inject.{Inject, Singleton}
import messaging.MessagePublisher
import modules.core.messaging.{AllMonetaryAccountsRefreshRequested, MonetaryAccountMessaging}
import org.reactivestreams.Subscriber
import play.api.mvc.{Action, AnyContent, InjectedController}
import streaming.AkkaStreamImplicits._

import scala.concurrent.Future

@Singleton
class MonetaryAccountController @Inject() (clock: Clock, publisher: MessagePublisher, secureAction: AuthEnforcedAction)(
    implicit val mat: Materializer
) extends InjectedController {
  import modules.core.messaging.MonetaryAccountSerde._
  import serde.PlayJsonSerialization._

  def refresh: Action[AnyContent] = secureAction.async { request =>
    Source
      .single(AllMonetaryAccountsRefreshRequested(request.userId, Instant.now(clock)))
      .runWith(refreshRequestedPublisher)

    Future.successful(Accepted("OK"))
  }

  private def refreshRequestedPublisher: Subscriber[AllMonetaryAccountsRefreshRequested] =
    publisher
      .publishTo[AllMonetaryAccountsRefreshRequested](MonetaryAccountMessaging.channel, AllMonetaryAccountsRefreshRequested.payloadType)
}

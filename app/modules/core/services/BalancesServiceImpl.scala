package modules.core.services

import java.time.Instant

import akka.stream.Materializer
import akka.stream.scaladsl.Sink
import javax.inject.{Inject, Singleton}
import modules.core.domain.BalancesService
import modules.core.projections.{Balance, BalanceHistoryRepository, MonetaryAccountRepository}
import org.reactivestreams.Publisher
import streaming.AkkaStreamImplicits._

@Singleton
private[core] class BalancesServiceImpl @Inject() (
    balancesRepository: BalanceHistoryRepository,
    backAccountRepository: MonetaryAccountRepository
)(
    implicit mat: Materializer
) extends BalancesService {
  override def fetchMonthlyBalancesBetween(
      userId: types.UserId,
      monetaryAccountIds: Seq[types.MonetaryAccountId],
      startDate: Instant,
      endDate: Instant
  ): Publisher[Balance] =
    backAccountRepository
      .fetchByUserAndIds(userId, monetaryAccountIds)
      .fold(Set[types.MonetaryAccountId]()) { (result, monetaryAccount) =>
        result + monetaryAccount.id
      }
      .flatMapMerge(1, accountIds => balancesRepository.fetchMonthlyBalanceHistoryForAccounts(accountIds, startDate, endDate))
      .runWith(Sink.asPublisher(true))

  override def fetchLatestFor(monetaryAccountIds: Set[types.MonetaryAccountId]): Publisher[Balance] =
    balancesRepository.fetchLatestForAccounts(monetaryAccountIds)
}

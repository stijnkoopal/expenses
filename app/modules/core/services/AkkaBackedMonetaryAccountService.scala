package modules.core.services

import java.time.Instant
import java.util.Currency

import akka.actor.typed.scaladsl.Behaviors
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity}
import akka.util.Timeout
import eu.timepit.refined.refineV
import eu.timepit.refined.string.Uuid
import javax.inject.{Inject, Singleton}
import modules.core.domain.{MonetaryAccount, MonetaryAccountService}
import modules.core.messaging.{MonetaryAccountDocument, TransactionDocument}
import play.api.Logging

import scala.concurrent.duration._

@Singleton
private[core] class AkkaBackedMonetaryAccountService @Inject()(sharding: ClusterSharding) extends MonetaryAccountService with Logging {
  private implicit val askTimeout: Timeout = Timeout(5.seconds)

  sharding.init(Entity(typeKey = MonetaryAccount.TypeKey) { entityContext =>
    {
      refineV[Uuid](entityContext.entityId) match {
        case Left(error) =>
          logger.warn(s"Could not refine ${entityContext.entityId} to MonetaryAccountId, not starting entity")
          logger.warn(error)
          Behaviors.empty

        case Right(monetaryAccountId) =>
          MonetaryAccount(entityId = monetaryAccountId)
      }
    }
  })

  def processMonetaryAccountDocument(document: MonetaryAccountDocument): Unit = {
    document.iban.foreach(iban => {
      val command = MonetaryAccount.ProcessMonetaryAccount(
        iban = iban,
        joint = document.joint,
        ownerUserId = document.userId,
        alias = document.alias,
        institution = document.institution,
        balance = document.balance,
        currency = Currency.getInstance(document.currency),
        fetchDateTime = document.fetchDateTime
      )

      sendCommandTo(command)(document.id)
    })
  }

  def processTransactionDocument(document: TransactionDocument): Unit = {
    val command = document.direction match {
      case types.TransactionDirection.Inbound =>
        MonetaryAccount.ProcessInboundTransaction(
          id = document.id,
          amount = document.amount,
          currency = document.currency,
          description = document.description,
          transactionDate = document.timestamp,
          payee = MonetaryAccount.TransactionParty(iban = document.payee.iban, name = document.payee.name),
          payer = MonetaryAccount.TransactionParty(iban = document.payer.iban, name = document.payer.name),
          balanceAfterMutation = document.balanceAfterMutation
        )

      case types.TransactionDirection.Outbound =>
        MonetaryAccount.ProcessOutboundTransaction(
          id = document.id,
          amount = document.amount,
          currency = document.currency,
          description = document.description,
          transactionDate = document.timestamp,
          partOfScheduleId = document.scheduleId,
          payee = MonetaryAccount.TransactionParty(iban = document.payee.iban, name = document.payee.name),
          payer = MonetaryAccount.TransactionParty(iban = document.payer.iban, name = document.payer.name),
          balanceAfterMutation = document.balanceAfterMutation
        )
    }

    sendCommandTo(command)(document.monetaryAccountId)
  }

  def updateBalance(monetaryAccountId: types.MonetaryAccountId, newBalance: BigDecimal): Unit = {
    val command = MonetaryAccount.UpdateBalanceForNonAutomatedAccount(newBalance = newBalance, balanceTimestamp = Instant.now())
    sendCommandTo(command)(monetaryAccountId)
  }

  private def sendCommandTo(command: MonetaryAccount.Command)(monetaryAccountId: types.MonetaryAccountId): Unit = {
    val entityRef = sharding.entityRefFor(MonetaryAccount.TypeKey, monetaryAccountId.toString)
    entityRef ! command
  }
}

package modules.core.projections

import java.time.Instant
import java.time.temporal.ChronoUnit

import akka.Done
import akka.stream.scaladsl.{Merge, Sink, Source}
import akka.stream.{Materializer, SharedKillSwitch}
import javax.inject.{Inject, Singleton}
import messaging.MessageReceiver
import modules.core.messaging._
import org.reactivestreams.Publisher
import persistence.DatabaseExecutionContext
import persistence.ScalalikejdbcImplicits._
import play.api.Logging
import scalikejdbc.streams._
import scalikejdbc.{DB, SQLSyntax, WrappedResultSet}
import streaming.AkkaStreamImplicits._

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps

case class Balance(
    monetaryAccount: types.MonetaryAccountId,
    timestamp: Instant,
    balance: BigDecimal
)

case object Balance {
  def fromResultSet(rs: WrappedResultSet): Balance = Balance(
    monetaryAccount = rs.get("monetary_account"),
    timestamp = rs.get("timestamp"),
    balance = rs.get("balance")
  )
}

trait BalanceHistoryRepository {
  def fetchMonthlyBalanceHistoryForAccounts(
      accountIds: Set[types.MonetaryAccountId],
      startDate: Instant,
      endDate: Instant
  ): Publisher[Balance]

  def fetchLatestForAccounts(accountIds: Set[types.MonetaryAccountId]): Publisher[Balance]
}

@Singleton
private[core] class PostgresBalanceProjection @Inject() (receiver: MessageReceiver)(
    implicit ec: DatabaseExecutionContext,
    mat: Materializer
) extends BalanceHistoryRepository
    with Logging {
  import modules.core.messaging.MonetaryAccountSerde._
  import modules.core.messaging.TransactionSerde._
  import serde.PlayJsonDeserialization._

  val tableName     = "balance_history"
  private val table = SQLSyntax.createUnsafely(tableName)

  def start(killSwitch: SharedKillSwitch): Future[Done] = {
    logger.info(s"Starting to listen for 'MonetaryAccountBalanceChangedMsg' messages on '${MonetaryAccountMessaging.channel}'")
    receiver
      .receive[MonetaryAccountBalanceChangedMsg](
        channel = MonetaryAccountMessaging.channel,
        payloadType = MonetaryAccountBalanceChangedMsg.payloadType
      )
      .via(killSwitch.flow)
      .map(buildBalance)
      .runWith(Sink.foreachAsync(1)(store))

    Source
      .combine(
        receiver.receive[NewInboundTransactionFoundMsg](
          channel = TransactionMessaging.channel,
          payloadType = NewInboundTransactionFoundMsg.payloadType
        ),
        receiver.receive[NewOutboundTransactionFoundMsg](
          channel = TransactionMessaging.channel,
          payloadType = NewOutboundTransactionFoundMsg.payloadType
        )
      )(Merge(_))
      .via(killSwitch.flow)
      .filter(_.balanceAfterMutation.isDefined)
      .map(buildBalance)
      .groupedWithin(100, 3 seconds)
      .log("Storing balance", balances => s"#${balances.length}")
      .runWith(Sink.foreachAsync(1)(store))
  }

  override def fetchMonthlyBalanceHistoryForAccounts(
      accountIds: Set[types.MonetaryAccountId],
      startDate: Instant,
      endDate: Instant
  ): Publisher[Balance] =
    if (accountIds.isEmpty) {
      Source
        .empty[Balance]
        .runWith(Sink.asPublisher(false))
    } else {
      DB.readOnlyStream {
        sqlr"""
          WITH latest AS (
            SELECT MAX(timestamp) AS max_timestamp, monetary_account
            FROM $table
            WHERE timestamp BETWEEN 
              ${startDate.truncatedTo(ChronoUnit.DAYS)} 
              AND 
              ${endDate.plus(1, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS)}
            AND monetary_account IN ($accountIds)
            GROUP BY monetary_account, TO_CHAR(timestamp, 'MM'), TO_CHAR(timestamp, 'YYYY')
          )
          SELECT *
          FROM $table
          INNER JOIN latest ON
            latest.monetary_account = $table.monetary_account AND
            latest.max_timestamp = $table.timestamp
        """
          .map(Balance.fromResultSet)
          .iterator()
      }
    }

  override def fetchLatestForAccounts(accountIds: Set[types.MonetaryAccountId]): Publisher[Balance] =
    if (accountIds.isEmpty) {
      Source
        .empty[Balance]
        .runWith(Sink.asPublisher(false))
    } else {
      DB.readOnlyStream {
          sqlr"""
          WITH latest AS (
            SELECT MAX(timestamp) AS max_timestamp, monetary_account
            FROM $table
            WHERE monetary_account IN ($accountIds)
            GROUP BY monetary_account
          )
          SELECT *
          FROM $table
          INNER JOIN latest ON
            latest.monetary_account = $table.monetary_account AND
            latest.max_timestamp = $table.timestamp
        """.map(Balance.fromResultSet)
            .iterator()
        }
        .grouped(Int.MaxValue)
        .orElse(Source.single(Nil))
        .mapConcat(balances =>
          balances ++ accountIds
            .filter(accountId => !balances.exists(_.monetaryAccount == accountId))
            .map(accountId => Balance(accountId, Instant.EPOCH, 0))
        )
    }

  private def store(balance: Balance): Future[Unit] = store(Seq(balance))

  private def store(balances: Seq[Balance]): Future[Unit] = DB.futureLocalTx { implicit session =>
    Future {
      val batch: Seq[Seq[(Symbol, Any)]] = balances
        .map(balance =>
          Seq(
            Symbol("monetary_account") -> balance.monetaryAccount,
            Symbol("timestamp")        -> balance.timestamp,
            Symbol("balance")          -> balance.balance
          )
        )
        .toSeq

      sqlr"""
        INSERT INTO $table(monetary_account, timestamp, balance)
        VALUES({monetary_account}, {timestamp}, {balance})
        ON CONFLICT (monetary_account, timestamp) DO UPDATE
        SET balance = {balance}
      """
        .batchByNameRefined(batch: _*)
        .apply()
    }
  }

  private def buildBalance(snapshot: MonetaryAccountBalanceChangedMsg) =
    Balance(
      monetaryAccount = snapshot.monetaryAccountId,
      timestamp = snapshot.timestamp,
      balance = snapshot.balance
    )

  private def buildBalance(snapshot: NewTransactionFoundMsg) =
    Balance(
      monetaryAccount = snapshot.accountId,
      timestamp = snapshot.transactionDate,
      balance = snapshot.balanceAfterMutation.get
    )
}

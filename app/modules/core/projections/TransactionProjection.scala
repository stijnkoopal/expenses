package modules.core.projections

import java.time.Instant

import akka.Done
import akka.stream.scaladsl.{Merge, Sink, Source}
import akka.stream.{Materializer, SharedKillSwitch}
import eu.timepit.refined.types.string.NonEmptyString
import javax.inject.{Inject, Singleton}
import messaging.MessageReceiver
import modules.core.domain
import modules.core.messaging._
import org.reactivestreams.Publisher
import persistence.DatabaseExecutionContext
import persistence.ScalalikejdbcImplicits._
import play.api.Logging
import scalikejdbc.streams._
import scalikejdbc.{DB, SQLSyntax, WrappedResultSet}
import streaming.AkkaStreamImplicits._
import streaming.AkkaStreamsUtil

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps

case class TransactionParty(
    iban: Option[types.IbanString],
    name: Option[NonEmptyString]
)

case class Transaction(
    id: types.TransactionId,
    monetaryAccountId: types.MonetaryAccountId,
    amount: BigDecimal,
    currency: String,
    direction: types.TransactionDirection,
    payer: Option[TransactionParty],
    payee: Option[TransactionParty],
    description: String,
    timestamp: Instant
) {
  def toMinimalTransaction: domain.MinimalTransaction = domain.MinimalTransaction(
    payeeIban = payee.flatMap(_.iban),
    payeeName = payee.flatMap(_.name),
    payerIban = payer.flatMap(_.iban),
    payerName = payer.flatMap(_.name),
    monetaryAccountId = monetaryAccountId,
    direction = direction,
    description = description
  )
}

object Transaction {
  def fromResultSet(rs: WrappedResultSet): Transaction = Transaction(
    id = rs.get("id"),
    monetaryAccountId = rs.get("monetary_account_id"),
    amount = rs.get("amount"),
    currency = rs.get("currency"),
    direction = rs.get("direction"),
    payer = partyFrom(rs.get("payer_iban"), rs.get("payer_name")),
    payee = partyFrom(rs.get("payee_iban"), rs.get("payee_name")),
    description = rs.get("description"),
    timestamp = rs.get("timestamp")
  )

  private def partyFrom(iban: Option[types.IbanString], name: Option[NonEmptyString]): Option[TransactionParty] = (iban, name) match {
    case (None, None) => None
    case _            => Some(TransactionParty(iban = iban, name = name))
  }
}

trait TransactionRepository {
  def fetchByIds(ids: Set[types.TransactionId]): Publisher[Transaction]
  def fetchByMonetaryAccounts(ids: Set[types.MonetaryAccountId]): Publisher[Transaction]
  def fetchByMonetaryAccountsBetween(
      ids: Set[types.MonetaryAccountId],
      startDate: Instant,
      endDate: Instant
  ): Publisher[Transaction]
}

@Singleton
private[core] class PostgresTransactionsProjection @Inject()(receiver: MessageReceiver)(
    implicit ec: DatabaseExecutionContext,
    mat: Materializer
) extends TransactionRepository
    with Logging {
  import TransactionSerde._
  import serde.PlayJsonDeserialization._

  val tableName     = "transaction"
  private val table = SQLSyntax.createUnsafely(tableName)

  def start(killSwitch: SharedKillSwitch): Future[Done] = {
    logger.info(s"Starting to listen for 'NewInboundTransactionFoundMsg' messages on '${TransactionMessaging.channel}'")
    val sourceInbound = receiver
      .receive[NewInboundTransactionFoundMsg](TransactionMessaging.channel, NewInboundTransactionFoundMsg.payloadType)
      .map(buildTransaction)

    val sourceOutbound = receiver
      .receive[NewOutboundTransactionFoundMsg](TransactionMessaging.channel, NewOutboundTransactionFoundMsg.payloadType)
      .map(buildTransaction)

    Source
      .combine(sourceInbound, sourceOutbound)(Merge(_))
      .groupBy(25, _.monetaryAccountId, allowClosedSubstreamRecreation = true)
      .groupedWithin(100, 3 seconds)
      .log(
        "Storing transactions",
        txs => s"#${txs.length}${txs.headOption.map(head => s" transactions for account ${head.monetaryAccountId}").getOrElse("")}"
      )
      .mergeSubstreams
      .via(killSwitch.flow)
      .runWith(Sink.foreach(insertOrIgnoreOnDuplicate))
  }

  override def fetchByIds(ids: Set[types.TransactionId]): Publisher[Transaction] =
    if (ids.isEmpty)
      AkkaStreamsUtil.emptyPublisher()
    else
      DB.readOnlyStream {
        sqlr"select * from $table WHERE id IN($ids)"
          .map(Transaction.fromResultSet)
          .iterator()
      }

  override def fetchByMonetaryAccounts(ids: Set[types.MonetaryAccountId]): Publisher[Transaction] =
    if (ids.isEmpty)
      AkkaStreamsUtil.emptyPublisher()
    else
      DB.readOnlyStream {
        sqlr"select * from $table WHERE monetary_account_id IN($ids)"
          .map(Transaction.fromResultSet)
          .iterator()
      }

  override def fetchByMonetaryAccountsBetween(
      ids: Set[types.MonetaryAccountId],
      startDate: Instant,
      endDate: Instant
  ): Publisher[Transaction] =
    if (ids.isEmpty)
      AkkaStreamsUtil.emptyPublisher()
    else
      DB.readOnlyStream {
        sqlr"select * from $table WHERE monetary_account_id IN($ids) AND timestamp BETWEEN $startDate and $endDate"
          .map(Transaction.fromResultSet)
          .iterator()
      }

  private def insertOrIgnoreOnDuplicate(transactions: Seq[Transaction]): Unit = DB.localTx { implicit session =>
    val batch: Seq[Seq[(Symbol, Any)]] = transactions
      .map(
        tx =>
          Seq(
            Symbol("id")                  -> tx.id,
            Symbol("monetary_account_id") -> tx.monetaryAccountId,
            Symbol("amount")              -> tx.amount,
            Symbol("currency")            -> tx.currency,
            Symbol("direction")           -> tx.direction,
            Symbol("payer_iban")          -> tx.payer.flatMap(_.iban),
            Symbol("payer_name")          -> tx.payer.flatMap(_.name),
            Symbol("payee_iban")          -> tx.payee.flatMap(_.iban),
            Symbol("payee_name")          -> tx.payee.flatMap(_.name),
            Symbol("description")         -> tx.description,
            Symbol("timestamp")           -> tx.timestamp
        ))

    sqlr"""
          INSERT INTO $table(id, monetary_account_id, amount, currency, direction, payer_iban, payer_name, payee_iban, payee_name, description, timestamp)
          VALUES ({id}, {monetary_account_id}, {amount}, {currency}, {direction}, {payer_iban}, {payer_name}, {payee_iban}, {payee_name}, {description}, {timestamp})
          ON CONFLICT DO NOTHING
        """
      .batchByNameRefined(batch: _*)
      .apply()
  }

  private def buildTransaction(transactionMsg: NewInboundTransactionFoundMsg) =
    Transaction(
      id = transactionMsg.id,
      monetaryAccountId = transactionMsg.accountId,
      amount = transactionMsg.amount,
      currency = transactionMsg.currency.getCurrencyCode,
      direction = types.TransactionDirection.Inbound,
      payer = Some(TransactionParty(transactionMsg.payer.iban, transactionMsg.payer.name)),
      payee = None,
      description = transactionMsg.description,
      timestamp = transactionMsg.transactionDate
    )

  private def buildTransaction(transactionMsg: NewOutboundTransactionFoundMsg) =
    Transaction(
      id = transactionMsg.id,
      monetaryAccountId = transactionMsg.accountId,
      amount = transactionMsg.amount,
      currency = transactionMsg.currency.getCurrencyCode,
      direction = types.TransactionDirection.Outbound,
      payer = None,
      payee = Some(TransactionParty(transactionMsg.payee.iban, transactionMsg.payee.name)),
      description = transactionMsg.description,
      timestamp = transactionMsg.transactionDate
    )
}

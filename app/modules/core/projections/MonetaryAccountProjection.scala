package modules.core.projections

import java.time.{Clock, LocalDateTime}

import akka.stream.scaladsl.{Merge, Sink, Source}
import akka.stream.{Materializer, SharedKillSwitch}
import akka.{Done, NotUsed}
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.types.string.NonEmptyString
import javax.inject.{Inject, Singleton}
import messaging.MessageReceiver
import modules.core.messaging._
import org.reactivestreams.Publisher
import persistence.DatabaseExecutionContext
import persistence.ScalalikejdbcImplicits._
import play.api.Logging
import scalikejdbc.streams._
import scalikejdbc.{DB, SQLSyntax, WrappedResultSet}
import serde.Deserializable
import streaming.AkkaStreamImplicits._
import streaming.AkkaStreamsUtil
import types._

import scala.concurrent.Future

case class MonetaryAccountRow(
    id: MonetaryAccountId,
    userIds: Set[UserId],
    iban: Option[IbanString],
    institution: Institution,
    alias: Option[NonEmptyString],
    currency: String,
    joint: Boolean = false,
    created: LocalDateTime,
    updated: Option[LocalDateTime] = None,
    autoUpdated: Boolean = true
)

case object MonetaryAccountRow {
  def fromResultSet(rs: WrappedResultSet): MonetaryAccountRow = MonetaryAccountRow(
    id = rs.get("id"),
    userIds = rs.array("user_ids").getArray.asInstanceOf[Array[String]].map(Refined.unsafeApply[String, NonEmpty]).toSet,
    iban = rs.get("iban"),
    institution = rs.get("institution"),
    alias = rs.get("alias"),
    currency = rs.get("currency"),
    joint = rs.get("joint"),
    created = rs.get("created"),
    updated = rs.get("created"),
    autoUpdated = rs.get("auto_updated")
  )
}

trait MonetaryAccountRepository {
  def fetchAllByUser(userId: types.UserId): Publisher[MonetaryAccountRow]
  def fetchAllWithSimilarUser(monetaryAccountId: types.MonetaryAccountId): Publisher[MonetaryAccountRow]
  def fetchByUserAndIds(userId: types.UserId, ids: Seq[types.MonetaryAccountId]): Publisher[MonetaryAccountRow]
  def fetchByIds(ids: Set[types.MonetaryAccountId]): Publisher[MonetaryAccountRow]
}

@Singleton
private[core] class PostgresMonetaryAccountProjection @Inject()(receiver: MessageReceiver, clock: Clock)(
    implicit ec: DatabaseExecutionContext,
    mat: Materializer
) extends MonetaryAccountRepository
    with Logging {
  import MonetaryAccountSerde._
  import serde.PlayJsonDeserialization._

  val tableName     = "monetary_account"
  private val table = SQLSyntax.createUnsafely(tableName)

  def start(killSwitch: SharedKillSwitch): Future[Done] = {
    val listeners = Seq(
      listenFor[NewMonetaryAccountFoundMsg](NewMonetaryAccountFoundMsg.payloadType),
      listenFor[MonetaryAccountBecameJointMsg](MonetaryAccountBecameJointMsg.payloadType),
      listenFor[MonetaryAccountAliasUpdatedMsg](MonetaryAccountAliasUpdatedMsg.payloadType),
      listenFor[MonetaryAccountUserAddedMsg](MonetaryAccountUserAddedMsg.payloadType)
    )

    Source
      .combine(listeners.head, listeners(1), listeners(2), listeners(3))(Merge(_))
      .via(killSwitch.flow)
      .runWith(Sink.foreachAsync(1)(handle))
  }

  private def listenFor[MsgType: Deserializable](payloadType: String): Source[Any, NotUsed] = {
    logger.info(s"Starting to listen for '$payloadType' messages on '${MonetaryAccountMessaging.channel}'")
    receiver
      .receive[MsgType](channel = MonetaryAccountMessaging.channel, payloadType = payloadType)
  }

  override def fetchAllByUser(userId: types.UserId): Publisher[MonetaryAccountRow] =
    DB.readOnlyStream {
      sqlr"select * from $table where $userId = any(user_ids) order by alias asc"
        .map(MonetaryAccountRow.fromResultSet)
        .iterator()
    }

  override def fetchAllWithSimilarUser(monetaryAccountId: types.MonetaryAccountId): Publisher[MonetaryAccountRow] =
    DB.readOnlyStream {
      sqlr"select * from $table where (select unnest(user_ids) from monetary_account where id = $monetaryAccountId) = any(user_ids)"
        .map(MonetaryAccountRow.fromResultSet)
        .iterator()
    }

  override def fetchByUserAndIds(userId: types.UserId, ids: Seq[types.MonetaryAccountId]): Publisher[MonetaryAccountRow] =
    if (ids.isEmpty) AkkaStreamsUtil.emptyPublisher()
    else
      DB.readOnlyStream {
        sqlr"select * from $table where $userId = any(user_ids) and id in ($ids) order by alias asc"
          .map(MonetaryAccountRow.fromResultSet)
          .iterator()
      }

  override def fetchByIds(ids: Set[types.MonetaryAccountId]): Publisher[MonetaryAccountRow] =
    if (ids.isEmpty) AkkaStreamsUtil.emptyPublisher()
    else
      DB.readOnlyStream {
        sqlr"select * from $table where id IN ($ids) order by alias asc"
          .map(MonetaryAccountRow.fromResultSet)
          .iterator()
      }

  private def handle(a: Any): Future[Unit] = a match {
    case a: NewMonetaryAccountFoundMsg       => handle(a)
    case a: MonetaryAccountBecameJointMsg    => handle(a)
    case a: MonetaryAccountBecameSingularMsg => handle(a)
    case a: MonetaryAccountAliasUpdatedMsg   => handle(a)
    case a: MonetaryAccountUserAddedMsg      => handle(a)
    case _                                   => throw new Error(s"MonetaryAccountProjection doesnt nkow how to handle $a")
  }

  private def handle(a: NewMonetaryAccountFoundMsg): Future[Unit] = DB.futureLocalTx { implicit session =>
    Future {
      sqlr"""
        INSERT INTO $table(id, user_ids, iban, institution, currency, joint, created, auto_updated)
        VALUES(${a.id}, ARRAY[${a.userId}], ${a.iban}, ${a.institution}, ${a.currency.getCurrencyCode}, ${a.joint}, now(), true)
        ON CONFLICT DO NOTHING
      """.update.apply()
    }
  }

  private def handle(a: MonetaryAccountBecameJointMsg): Future[Unit] = DB.futureLocalTx { implicit session =>
    Future {
      sqlr"""
        UPDATE $table SET joint = true, updated = now() WHERE id = ${a.monetaryAccountId}
      """.update.apply()
    }
  }

  private def handle(a: MonetaryAccountBecameSingularMsg): Future[Unit] = DB.futureLocalTx { implicit session =>
    Future {
      sqlr"""
        UPDATE $table SET joint = false, updated = now() WHERE id = ${a.monetaryAccountId}
      """.update.apply()
    }
  }

  private def handle(a: MonetaryAccountAliasUpdatedMsg): Future[Unit] = DB.futureLocalTx { implicit session =>
    Future {
      sqlr"""
        UPDATE $table SET alias = ${a.newAlias}, updated = now() WHERE id = ${a.monetaryAccountId}
      """.update.apply()
    }
  }

  private def handle(a: MonetaryAccountUserAddedMsg): Future[Unit] = DB.futureLocalTx { implicit session =>
    Future {
      val currentStored =
        sqlr"""SELECT * FROM $table where id = ${a.monetaryAccountId}""".map(MonetaryAccountRow.fromResultSet).single.apply()

      currentStored match {
        case Some(account) =>
          val newUsers = account.userIds + a.userId

          sqlr"""
            UPDATE $table SET user_ids = ARRAY[$newUsers], updated = now() WHERE id = ${a.monetaryAccountId}
          """.update.apply()

        case None =>
          ()
      }
    }
  }
}

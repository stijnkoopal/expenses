package modules.core

import modules.core.domain.{BalancesService, MonetaryAccountService, SimpleTransactionVisibilityResolver, TransactionVisibilityResolver}
import modules.core.services._
import modules.core.projections._
import play.api.inject.{Binding, Module}
import play.api.{Configuration, Environment}

class CoreModule extends Module {
  override def bindings(environment: Environment, configuration: Configuration): Seq[Binding[_]] = Seq(
    bind[CoreStreamsStarter].toSelf.eagerly(),
    bind[MonetaryAccountRepository].to[PostgresMonetaryAccountProjection],
    bind[BalanceHistoryRepository].to[PostgresBalanceProjection],
    bind[BalancesService].to[BalancesServiceImpl],
    bind[TransactionRepository].to[PostgresTransactionsProjection],
    bind[TransactionVisibilityResolver].to[SimpleTransactionVisibilityResolver],
    bind[MonetaryAccountService].to[AkkaBackedMonetaryAccountService]
  )
}

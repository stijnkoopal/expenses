package modules.allocation

import akka.Done
import akka.actor.CoordinatedShutdown
import akka.stream.KillSwitches
import javax.inject.{Inject, Singleton}
import modules.allocation.projections.PostgresCounterpartyProjection

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AllocationStreamsStarter @Inject()(cs: CoordinatedShutdown, counterpartyProjection: PostgresCounterpartyProjection)(
    implicit ec: ExecutionContext
) {
  private val killSwitch = KillSwitches.shared("Allocation")

  private val futures = List(
    counterpartyProjection.start(killSwitch)
  )

  cs.addTask(CoordinatedShutdown.PhaseServiceUnbind, "stop-allocation-streams") { () =>
    killSwitch.shutdown()
    Future.sequence(futures).map(_ => Done)
  }
}

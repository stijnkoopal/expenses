package modules.allocation

import modules.allocation.domain.TransactionCategoryRepository
import modules.allocation.projections.{CouterpartyRepository, InMemoryTransactionCategoryRepository, PostgresCounterpartyProjection}
import play.api.inject.{Binding, Module}
import play.api.{Configuration, Environment}

class AllocationModule extends Module {
  override def bindings(environment: Environment, configuration: Configuration): Seq[Binding[_]] = Seq(
    bind[AllocationStreamsStarter].toSelf.eagerly(),
    bind[CouterpartyRepository].to[PostgresCounterpartyProjection],
    bind[TransactionCategoryRepository].to[InMemoryTransactionCategoryRepository],
  )
}

package modules.allocation.projections

import akka.stream.Materializer
import akka.stream.scaladsl.Source
import javax.inject.{Inject, Singleton}
import modules.allocation.domain.{TransactionCategory, TransactionCategoryRepository}
import org.reactivestreams.Publisher
import streaming.AkkaStreamImplicits._

@Singleton
class InMemoryTransactionCategoryRepository @Inject() ()(implicit mat: Materializer) extends TransactionCategoryRepository {

  private val inboundCategories = Set(
    TransactionCategory("salary", "Salary"),
    TransactionCategory("gift", "Gift"),
    TransactionCategory("pension", "Pension"),
    TransactionCategory("alimony", "Alimony"),
    TransactionCategory("household-share", "Household share"),
    TransactionCategory("repayment", "Repayment"),
    TransactionCategory("benefits-allowances", "Benefits and allowances"),
    TransactionCategory("tax", "Tax"),
    TransactionCategory("from-capital", "From capital"),
    TransactionCategory("inbound_transfers", "Transfers"),
    TransactionCategory("inbound_savings-investing", "Transfers"),
    TransactionCategory("inbound_other", "Other income")
  )

  private val outboundCategories = Set(
    TransactionCategory("living", "Living"),
    TransactionCategory("living_rent", "Rent", Some("living")),
    TransactionCategory("living_mortgage", "Mortgage", Some("living")),
    TransactionCategory("living_tv-internet-tel", "Tv, internet and telephone", Some("living")),
    TransactionCategory("living_utilities", "Utilities", Some("living")),
    TransactionCategory("living_insurance", "Insurance", Some("living")),
    TransactionCategory("living_services", "Services", Some("living")),
    TransactionCategory("living_other", "Other", Some("living")),
    // -------------------------------------------------------------------
    TransactionCategory("house-garden", "House/Garden"),
    TransactionCategory("house-garden_investments", "House investments", Some("house-garden")),
    TransactionCategory("house-garden_repairs", "Repairs", Some("house-garden")),
    TransactionCategory("house-garden_furnishing", "Furnishing", Some("house-garden")),
    TransactionCategory("house-garden_garden", "Garden", Some("house-garden")),
    TransactionCategory("house-garden_other", "Other", Some("house-garden")),
    // -------------------------------------------------------------------
    TransactionCategory("eating-drinking", "Eating/drinking"),
    TransactionCategory("eating-drinking_groceries", "Groceries", Some("eating-drinking")),
    TransactionCategory("eating-drinking_restaurant", "Restaurant", Some("eating-drinking")),
    TransactionCategory("eating-drinking_eating-in", "Eating in", Some("eating-drinking")),
    TransactionCategory("eating-drinking_to-go", "To go", Some("eating-drinking")),
    TransactionCategory("eating-drinking_coffee-lunch", "Coffee/lunch", Some("eating-drinking")),
    TransactionCategory("eating-drinking_alcohol-tobacco", "Alcohol/tobacco", Some("eating-drinking")),
    TransactionCategory("eating-drinking_bars", "Bars", Some("eating-drinking")),
    TransactionCategory("eating-drinking_other", "Other", Some("eating-drinking")),
    // -------------------------------------------------------------------
    TransactionCategory("financials", "Financials"),
    TransactionCategory("financials_withdrawals-fees", "Withdrawals/fees", Some("financials")),
    TransactionCategory("financials_loans", "Loans", Some("financials")),
    TransactionCategory("financials_advance", "Advance", Some("financials")),
    TransactionCategory("financials_tax", "Tax", Some("financials")),
    TransactionCategory("financials_lottery", "Lottery", Some("financials")),
    // -------------------------------------------------------------------
    TransactionCategory("transportation", "Transportation"),
    TransactionCategory("transportation_car-fuel", "Car/fuel", Some("transportation")),
    TransactionCategory("transportation_public-transport", "Public transportation", Some("transportation")),
    TransactionCategory("transportation_tickets", "Tickets", Some("transportation")),
    TransactionCategory("transportation_taxi", "Taxi", Some("transportation")),
    TransactionCategory("transportation_other", "Other", Some("transportation")),
    // -------------------------------------------------------------------
    TransactionCategory("shopping", "Shopping"),
    TransactionCategory("shopping_clothing-accessories", "Clothing/accessories", Some("shopping")),
    TransactionCategory("shopping_electronics", "Electronics", Some("shopping")),
    TransactionCategory("shopping_books-papers", "Books/papers", Some("shopping")),
    TransactionCategory("shopping_gifts", "Gifts", Some("shopping")),
    TransactionCategory("shopping_other", "Other", Some("shopping")),
    // -------------------------------------------------------------------
    TransactionCategory("leisure", "Leisure"),
    TransactionCategory("leisure_day-trip", "Day trip", Some("leisure")),
    TransactionCategory("leisure_hobby-sport", "Hobby/sport", Some("leisure")),
    TransactionCategory("leisure_overnight-stay", "Overnight stay", Some("leisure")),
    TransactionCategory("leisure_holiday", "Holiday", Some("leisure")),
    TransactionCategory("leisure_other", "Other", Some("leisure")),
    // -------------------------------------------------------------------
    TransactionCategory("health-beauty", "Health/beauty"),
    TransactionCategory("health-beauty_healthcare-pharmacy", "Healthcare/pharmacy", Some("health-beauty")),
    TransactionCategory("health-beauty_hygiene", "Hygiene", Some("health-beauty")),
    TransactionCategory("health-beauty_eye-care", "Eye care", Some("health-beauty")),
    TransactionCategory("health-beauty_beauty", "Beauty", Some("health-beauty")),
    TransactionCategory("health-beauty_other", "Other", Some("health-beauty")),
    // -------------------------------------------------------------------
    TransactionCategory("family", "Family"),
    TransactionCategory("family_day-care", "Day care", Some("family")),
    TransactionCategory("family_kids", "Kids", Some("family")),
    TransactionCategory("family_pets", "Pets", Some("family")),
    TransactionCategory("family_other", "Other", Some("family")),
    // -------------------------------------------------------------------
    TransactionCategory("outbound_other", "Other"),
    TransactionCategory("outbound_other_business", "Business", Some("other")),
    TransactionCategory("outbound_other_education", "Education", Some("other")),
    TransactionCategory("outbound_other_charity", "Charity", Some("other")),
    TransactionCategory("outbound_other_other", "Other", Some("other")),
    // -------------------------------------------------------------------
    TransactionCategory("outbound_transfers", "Transfers"),
    // -------------------------------------------------------------------
    TransactionCategory("outbound_saving-investing", "Saving/investing")
  )

  override def findOutboundCategories(): Publisher[TransactionCategory] = Source.fromIterator(() => outboundCategories.iterator)
  override def findInboundCategories(): Publisher[TransactionCategory]  = Source.fromIterator(() => inboundCategories.iterator)
  override def findByIds(ids: Seq[String]): Publisher[TransactionCategory] =
    Source.fromIterator(() => (inboundCategories ++ outboundCategories).filter(category => ids.contains(category.id)).iterator)
}

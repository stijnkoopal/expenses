package modules.allocation.projections

import java.nio.charset.StandardCharsets
import java.time.LocalDateTime
import java.util.UUID

import akka.Done
import akka.stream.scaladsl.{Merge, Sink, Source}
import akka.stream.{Materializer, SharedKillSwitch}
import eu.timepit.refined.api.Refined
import eu.timepit.refined.types.string.NonEmptyString
import javax.inject.{Inject, Singleton}
import messaging.MessageReceiver
import modules.core.messaging.{NewInboundTransactionFoundMsg, NewOutboundTransactionFoundMsg, TransactionMessaging}
import org.reactivestreams.Publisher
import persistence.DatabaseExecutionContext
import persistence.ScalalikejdbcImplicits._
import play.api.Logging
import scalikejdbc.streams._
import scalikejdbc.{DB, SQLSyntax, WrappedResultSet}
import streaming.AkkaStreamImplicits._

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps

case class Counterparty(
    id: types.CounterpartyId,
    iban: Option[types.IbanString],
    name: Option[NonEmptyString],
    categoriesWhenPayee: Option[Set[String]] = None,
    categoriesWhenPayer: Option[Set[String]] = None,
    partialMatchOn: Option[String] = None,
    updated: Option[LocalDateTime] = None
)

case object Counterparty {
  def fromResultSet(rs: WrappedResultSet): Counterparty = Counterparty(
    id = rs.get("id"),
    iban = rs.get("iban"),
    name = rs.get("name"),
    categoriesWhenPayee = rs.arrayOpt("categories_when_payee").map(_.getArray.asInstanceOf[Array[String]].toSet),
    categoriesWhenPayer = rs.arrayOpt("categories_when_payer").map(_.getArray.asInstanceOf[Array[String]].toSet),
    partialMatchOn = rs.get("partial_match_on"),
    updated = rs.get("updated")
  )
}

trait CouterpartyRepository {
  def fetchAll(): Publisher[Counterparty]
}

@Singleton
private[allocation] class PostgresCounterpartyProjection @Inject()(
    receiver: MessageReceiver
)(
    implicit ec: DatabaseExecutionContext,
    mat: Materializer
) extends CouterpartyRepository
    with Logging {

  import modules.core.messaging.TransactionSerde._
  import serde.PlayJsonDeserialization._

  val tableName     = "allocation_counterparties"
  private val table = SQLSyntax.createUnsafely(tableName)

  def start(killSwitch: SharedKillSwitch): Future[Done] = {
    logger.info(s"Starting to listen for 'NewTransactionFoundMsg' messages on '${TransactionMessaging.channel}'")
    val source1 = receiver
      .receive[NewInboundTransactionFoundMsg](
        channel = TransactionMessaging.channel,
        payloadType = NewInboundTransactionFoundMsg.payloadType
      )
      .mapConcat(buildCounterparties)

    val source2 = receiver
      .receive[NewOutboundTransactionFoundMsg](
        channel = TransactionMessaging.channel,
        payloadType = NewOutboundTransactionFoundMsg.payloadType
      )
      .mapConcat(buildCounterparties)

    Source
      .combine(source1, source2)(Merge(_))
      .groupedWithin(100, 3 seconds)
      .via(killSwitch.flow)
      .runWith(Sink.foreach(insertOrIgnoreOnDuplicate))
  }

  override def fetchAll(): Publisher[Counterparty] = DB.readOnlyStream {
    sqlr"select * from $table ORDER BY name ASC, IBAN asc"
      .map(Counterparty.fromResultSet)
      .iterator()
  }

  private def insertOrIgnoreOnDuplicate(counterparties: Seq[Counterparty]): Unit = DB.localTx { implicit session =>
    val batch: Seq[Seq[(Symbol, Any)]] = counterparties
      .map(
        counterparty =>
          Seq(
            Symbol("id")   -> counterparty.id.value,
            Symbol("iban") -> counterparty.iban,
            Symbol("name") -> counterparty.name
        ))

    sqlr"""
          INSERT INTO $table(id, iban, name)
          VALUES ({id}, {iban}, {name})
          ON CONFLICT DO NOTHING
        """
      .batchByNameRefined(batch: _*)
      .apply()
  }

  private def buildCounterparties(transactionMessage: NewInboundTransactionFoundMsg): Seq[Counterparty] = {
    val counterparty = if (transactionMessage.payer.name.isDefined) {
      val payer = transactionMessage.payer
      val payerId = UUID.nameUUIDFromBytes(
        (payer.iban.map(_.toString()).getOrElse("") + payer.name.map(_.toString).getOrElse("")).getBytes(StandardCharsets.UTF_8)
      )

      Some(
        Counterparty(
          id = Refined.unsafeApply(payerId.toString),
          iban = payer.iban,
          name = payer.name
        )
      )
    } else {
      None
    }

    counterparty.toSeq
  }

  private def buildCounterparties(transactionMessage: NewOutboundTransactionFoundMsg): Seq[Counterparty] = {
    val counterparty = if (transactionMessage.payee.name.isDefined) {
      val payee = transactionMessage.payee
      val payeeId = UUID.nameUUIDFromBytes(
        (payee.iban.map(_.toString()).getOrElse("") + payee.name.map(_.toString).getOrElse("")).getBytes(StandardCharsets.UTF_8)
      )

      Some(
        Counterparty(
          id = Refined.unsafeApply(payeeId.toString),
          iban = payee.iban,
          name = payee.name
        )
      )
    } else {
      None
    }

    counterparty.toSeq
  }
}

package modules.allocation.domain

import org.reactivestreams.Publisher

case class TransactionCategory(
    id: String,
    label: String,
    parentId: Option[String] = None
)

trait TransactionCategoryRepository {
  def findInboundCategories(): Publisher[TransactionCategory]
  def findOutboundCategories(): Publisher[TransactionCategory]
  def findByIds(ids: Seq[String]): Publisher[TransactionCategory]
}

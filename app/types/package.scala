import eu.timepit.refined.api.{Refined, Validate}
import eu.timepit.refined.string.Uuid
import eu.timepit.refined.types.string.NonEmptyString
import org.iban4j.IbanUtil

import scala.util.Try

package object types
    extends CounterpartyIdSpec
    with IbanSpec
    with InstitutionSpec
    with MonetaryAccountIdSpec
    with MonetaryAccountClusterIdSpec
    with RecurringTransactionIdSpec
    with RecurringScheduleIdSpec
    with TransactionIdSpec
    with TransactionDirectionSpec
    with UserIdSpec {}

trait CounterpartyIdSpec {
  type CounterpartyId = String Refined Uuid
}

trait MonetaryAccountClusterIdSpec {
  type MonetaryAccountClusterId = String Refined Uuid
}

trait IbanSpec {
  case class Iban()

  object Iban {
    implicit def ibanValidate: Validate.Plain[String, Iban] =
      Validate.fromPredicate(predicate, t => s"$t is a valid Iban", Iban())

    private val predicate: String => Boolean = s => {
      Try {
        IbanUtil.validate(s)
      }.map(_ => true).getOrElse(false)
    }
  }

  type IbanString = String Refined Iban
}

trait InstitutionSpec {
  type Institution = NonEmptyString

  object Institution {
    val Bunq: Institution = Refined.unsafeApply("Bunq")
  }
}

trait MonetaryAccountIdSpec {
  type MonetaryAccountId = String Refined Uuid
}

trait RecurringScheduleIdSpec {
  type RecurringScheduleId = String Refined Uuid
}

trait RecurringTransactionIdSpec {
  type RecurringTransactionId = String Refined Uuid
}

trait TransactionDirectionSpec {
  case class Direction()

  object Direction {
    implicit def directionValidate: Validate.Plain[String, Direction] =
      Validate.fromPredicate(predicate, t => s"$t is a valid Direction", Direction())

    private val predicate: String => Boolean = s => {
      s == "Inbound" || s == "Outbound"
    }
  }

  type TransactionDirection = String Refined Direction
  object TransactionDirection {
    val Inbound: TransactionDirection  = Refined.unsafeApply[String, Direction]("Inbound")
    val Outbound: TransactionDirection = Refined.unsafeApply[String, Direction]("Outbound")
  }
}

trait TransactionIdSpec {
  type TransactionId = String Refined Uuid
}

trait UserIdSpec {
  type UserId = NonEmptyString
}

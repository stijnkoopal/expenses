package graphql

import java.time.ZoneOffset.UTC

import akka.stream.Materializer
import akka.stream.scaladsl.Sink
import javax.inject.{Inject, Singleton}
import modules.allocation.projections.{Counterparty, CouterpartyRepository}
import streaming.AkkaStreamImplicits._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AllocationCounterpartyGraphqlAdapter @Inject() (
    transactionRepository: CouterpartyRepository
)(implicit val mat: Materializer, ec: ExecutionContext)
    extends graphql.AllocationCounterpartyRepository {
  override def fetchAll(): Future[Seq[graphql.AllocationCounterparty]] =
    transactionRepository
      .fetchAll()
      .map(mapCounterparty)
      .runWith(Sink.seq)

  private def mapCounterparty(counterparty: Counterparty): AllocationCounterparty = AllocationCounterparty(
    id = counterparty.id,
    name = counterparty.name,
    iban = counterparty.iban,
    categoriesWhenPayee = counterparty.categoriesWhenPayee.map(_.toSeq),
    categoriesWhenPayer = counterparty.categoriesWhenPayer.map(_.toSeq),
    partialMatchOn = counterparty.partialMatchOn,
    updated = counterparty.updated.map(_.toInstant(UTC))
  )
}

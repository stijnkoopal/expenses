package graphql

import java.time.{Instant, Period, ZoneOffset}

import app.auth.SecurityContext
import eu.timepit.refined.types.string.NonEmptyString
import graphql.schema.{DateTimeDefinitions, RefinedDefinitions, UUIDDefinitions}
import graphql.security.{AuthContext, AuthenticatedTag}
import org.reactivestreams.Publisher
import play.api.libs.json._
import sangria.execution.deferred._
import sangria.macros.derive._
import sangria.marshalling.playJson._
import sangria.schema._

import scala.concurrent.{ExecutionContext, Future}

case class MonetaryAccount(
    id: types.MonetaryAccountId,
    iban: Option[types.IbanString],
    alias: Option[NonEmptyString],
    currency: String,
    joint: Boolean,
    autoUpdated: Boolean,
    lastUpdated: Instant,
    institution: types.Institution
)

trait MonetaryAccountClusterType
object MonetaryAccountClusterType {
  case object All         extends MonetaryAccountClusterType
  case object UserDefined extends MonetaryAccountClusterType
}

case class MonetaryAccountCluster(
    id: types.MonetaryAccountClusterId,
    clusterType: MonetaryAccountClusterType,
    name: NonEmptyString,
    monetaryAccounts: Seq[MonetaryAccountConfiguration],
    selected: Boolean = false
)

case class SelectMonetaryAccountClusterInput(
    id: types.MonetaryAccountClusterId
)

case class MonetaryAccountConfiguration(
    monetaryAccountId: types.MonetaryAccountId,
    joint: Boolean
)

object MonetaryAccount {
  implicit val hasId: HasId[MonetaryAccount, types.MonetaryAccountId] =
    HasId[MonetaryAccount, types.MonetaryAccountId](_.id)
}

case class TransactionParty(
    iban: Option[types.IbanString],
    name: Option[NonEmptyString]
)

case class Transaction(
    id: types.TransactionId,
    monetaryAccountId: types.MonetaryAccountId,
    payer: Option[TransactionParty],
    payee: Option[TransactionParty],
    amount: BigDecimal,
    description: String,
    timestamp: Instant
)

object Transaction {
  implicit val hasId: HasId[Transaction, types.TransactionId] = HasId[Transaction, types.TransactionId](_.id)
}

case class MonthlyCashflow(
    inbound: BigDecimal,
    outbound: BigDecimal,
    recurringOutbound: BigDecimal,
    recurringInbound: BigDecimal,
    monthStart: Instant
)

case class MonthlyCashflowAverages(
    inbound: BigDecimal,
    outbound: BigDecimal
)

case class RecurringSummed(
    inbound: BigDecimal,
    outbound: BigDecimal
)

case class LatestBalance(
    monetaryAccountId: types.MonetaryAccountId,
    balance: BigDecimal,
    latestUpdate: Instant
)

object LatestBalance {
  implicit val hasId: HasId[LatestBalance, types.MonetaryAccountId] = HasId[LatestBalance, types.MonetaryAccountId](_.monetaryAccountId)
}

case class MonthlyBalance(
    monthStart: Instant,
    monetaryAccountId: types.MonetaryAccountId,
    balance: BigDecimal
)

case class SingleBalanceSnapshot(
    monetaryAccount: types.MonetaryAccountId,
    balance: BigDecimal
)

case class BalanceSnapshotBatch(
    balances: Seq[SingleBalanceSnapshot]
)

case class RecurringTransaction(
    id: types.RecurringTransactionId,
    startDate: Instant,
    endDate: Option[Instant],
    frequency: Period,
    amount: BigDecimal,
    payer: TransactionParty,
    payee: TransactionParty,
    monetaryAccountId: types.MonetaryAccountId
)

case class LeftOverUntil(
    leftOver: BigDecimal,
    from: Instant,
    until: Instant,
    projectedInbound: BigDecimal,
    outboundSoFar: BigDecimal,
    recurringOutbound: BigDecimal
)

case class AllocationCounterparty(
    id: types.CounterpartyId,
    iban: Option[types.IbanString],
    name: Option[NonEmptyString],
    categoriesWhenPayee: Option[Seq[String]],
    categoriesWhenPayer: Option[Seq[String]],
    partialMatchOn: Option[String],
    updated: Option[Instant]
)
object AllocationCounterparty {
  implicit val hasId: HasId[AllocationCounterparty, types.CounterpartyId] =
    HasId[AllocationCounterparty, types.CounterpartyId](_.id)
}

case class TransactionCategory(
    id: String,
    parentId: Option[String],
    label: String,
    icon: String
)
object TransactionCategory {
  implicit val hasId: HasId[TransactionCategory, String] = HasId[TransactionCategory, String](_.id)
}

case class TransactionCategoriesResponse(
    inbound: Seq[TransactionCategory],
    outbound: Seq[TransactionCategory]
)

case class GraphqlContext(
    securityContext: SecurityContext,
    monetaryAccountService: MonetaryAccountService,
    monetaryAccountClusterService: MonetaryAccountClusterService,
    transactionRepo: TransactionRepository,
    recurringTransactionRepo: RecurringTransactionRepository,
    cashflowRepo: CashflowRepository,
    balancesRepo: BalancesRepository,
    allocationCounterpartyRepository: AllocationCounterpartyRepository,
    transactionCategoryRepository: TransactionCategoryRepository,
    budgettingService: BudgettingService
) extends AuthContext

class SchemaDefinition()(implicit val ec: ExecutionContext) extends UUIDDefinitions with DateTimeDefinitions with RefinedDefinitions {
  import de.cbley.refined.play.json._

  private lazy val LatestBalanceType: ObjectType[GraphqlContext, LatestBalance] = deriveObjectType()

  private lazy val MonetaryAccountType: ObjectType[GraphqlContext, MonetaryAccount] = deriveObjectType(
    AddFields[GraphqlContext, MonetaryAccount](
      Field(
        "transactions",
        ListType(TransactionType),
        resolve = ctx => transactionsFetcher.deferRelSeq(transactionsByMonetaryAccountRelation, ctx.value.id)
      ),
      Field(
        "latestBalance",
        LatestBalanceType,
        resolve = ctx => latestBalancesFetcher.defer(ctx.value.id)
      )
    )
  )

  private val monetaryAccountsFetcher =
    Fetcher((ctx: GraphqlContext, ids: Seq[types.MonetaryAccountId]) => ctx.monetaryAccountService.fetchMultipleById(ids))

  private val latestBalancesFetcher =
    Fetcher((ctx: GraphqlContext, ids: Seq[types.MonetaryAccountId]) => ctx.balancesRepo.fetchLatestFor(ids))

  private implicit lazy val TransactionPartyType: ObjectType[Unit, TransactionParty] = deriveObjectType()

  private lazy val TransactionType: ObjectType[Unit, Transaction] = deriveObjectType(
    ObjectTypeDescription("Transaction description"),
    ReplaceField(
      "monetaryAccountId",
      Field(
        "monetaryAccount",
        MonetaryAccountType,
        resolve = c => monetaryAccountsFetcher.defer(c.value.monetaryAccountId)
      )
    )
  )

  private val transactionsByMonetaryAccountRelation =
    Relation[Transaction, types.MonetaryAccountId]("byMonetaryAccount", transaction => Seq(transaction.monetaryAccountId))

  private val transactionsFetcher = Fetcher.rel(
    (ctx: GraphqlContext, ids: Seq[types.TransactionId]) => ctx.transactionRepo.fetchMultipleById(ids),
    (ctx: GraphqlContext, ids: RelationIds[Transaction]) =>
      ctx.transactionRepo.fetchMultipleById(ids(transactionsByMonetaryAccountRelation))
  )

  private lazy val MonthlyCashflowType: ObjectType[Unit, MonthlyCashflow]                           = deriveObjectType()
  private lazy val RecurringSummedType: ObjectType[Unit, RecurringSummed]                           = deriveObjectType()
  private lazy val MonthlyCashflowAveragesType: ObjectType[GraphqlContext, MonthlyCashflowAverages] = deriveObjectType()

  private lazy val MonthlyBalanceType: ObjectType[Unit, MonthlyBalance] = deriveObjectType(
    ObjectTypeDescription("Monthly balance"),
    ReplaceField(
      "monetaryAccountId",
      Field("monetaryAccount", MonetaryAccountType, resolve = c => monetaryAccountsFetcher.defer(c.value.monetaryAccountId))
    )
  )

  private lazy val RecurringTransactionType: ObjectType[Unit, RecurringTransaction] = deriveObjectType(
    ObjectTypeDescription("Recurring transaction"),
    ReplaceField(
      "monetaryAccountId",
      Field(
        "monetaryAccount",
        MonetaryAccountType,
        resolve = c => monetaryAccountsFetcher.defer(c.value.monetaryAccountId)
      )
    ),
    AddFields(
      Field(
        "transactions",
        ListType(TransactionType),
        resolve = c => Seq() // TODO
      )
    )
  )

  private lazy val transactionDirectionType = EnumType(
    "TransactionDirection",
    values = List(
      EnumValue("Inbound", value = types.TransactionDirection.Inbound),
      EnumValue("Outbound", value = types.TransactionDirection.Outbound)
    )
  )

  private lazy implicit val monetaryAccountClusterTypeType = EnumType(
    "MonetaryAccountClusterType",
    values = List(
      EnumValue("All", value = MonetaryAccountClusterType.All),
      EnumValue("UserDefined", value = MonetaryAccountClusterType.UserDefined)
    )
  )

  private lazy val LeftOverUntilType: ObjectType[Unit, LeftOverUntil] = deriveObjectType()

  private lazy val AllocationCounterpartyType: ObjectType[Unit, AllocationCounterparty] = deriveObjectType()

  private implicit val MonetaryAccountConfigurationFormat: Format[MonetaryAccountConfiguration] = Json.format
  private lazy implicit val MonetaryAccountConfigurationType: ObjectType[GraphqlContext, MonetaryAccountConfiguration] =
    deriveObjectType(
      ObjectTypeName("MonetaryAccountConfig"),
      ReplaceField(
        "monetaryAccountId",
        Field(
          "monetaryAccount",
          MonetaryAccountType,
          resolve = c => monetaryAccountsFetcher.defer(c.value.monetaryAccountId)
        )
      )
    )

  private lazy implicit val MonetaryAccountConfigurationInputType: InputObjectType[MonetaryAccountConfiguration] = deriveInputObjectType()
  private lazy val MonetaryAccountClusterObjectType: ObjectType[GraphqlContext, MonetaryAccountCluster]          = deriveObjectType()

  private val transactionCategoryFetcher =
    Fetcher((ctx: GraphqlContext, ids: Seq[String]) => ctx.transactionCategoryRepository.fetchMultipleById(ids))

  private lazy implicit val TransactionCategoryType: ObjectType[GraphqlContext, TransactionCategory] = deriveObjectType(
    ObjectTypeDescription("Transaction category"),
    ReplaceField(
      "parentId",
      Field(
        "parent",
        OptionType(TransactionCategoryType),
        resolve = c => transactionCategoryFetcher.deferOpt(c.value.parentId)
      )
    )
  )

  private lazy val TransactionCategoriesResponseType = deriveObjectType[GraphqlContext, TransactionCategoriesResponse]()

  private val StartDateArg            = Argument("startDate", LocalDateType)
  private val EndDateArg              = Argument("endDate", LocalDateType)
  private val TransactionDirectionArg = Argument("direction", transactionDirectionType)
  private val MonetaryAccountConfigurationsArg =
    Argument("monetaryAccounts", ListInputType(MonetaryAccountConfigurationInputType))

  private val QueryType = ObjectType(
    "Query",
    fields[GraphqlContext, Unit](
      Field(
        "monetaryAccounts",
        ListType(MonetaryAccountType),
        description = Some("Returns all monetary accounts"),
        arguments = MonetaryAccountConfigurationsArg :: Nil,
        resolve =
          c => c.ctx.monetaryAccountService.fetchByConfigurations(AuthContext(c).userId, c.arg(MonetaryAccountConfigurationsArg).toSet),
        tags = AuthenticatedTag :: Nil
      ),
      Field(
        "monetaryAccountClusters",
        ListType(MonetaryAccountClusterObjectType),
        description = Some("Configured slices on monetary accounts and therefore data"),
        resolve = c => c.ctx.monetaryAccountClusterService.fetchAllByUser(AuthContext(c).userId),
        tags = AuthenticatedTag :: Nil
      ),
      Field(
        "monthlyCashflow",
        ListType(MonthlyCashflowType),
        description = Some(""),
        arguments = StartDateArg :: EndDateArg :: MonetaryAccountConfigurationsArg :: Nil,
        resolve = c =>
          c.ctx.cashflowRepo
            .fetchMonthlyBetween(
              AuthContext(c).userId,
              c.arg(MonetaryAccountConfigurationsArg).toSet,
              c.arg(StartDateArg).atStartOfDay().toInstant(ZoneOffset.UTC),
              c.arg(EndDateArg).atStartOfDay().toInstant(ZoneOffset.UTC)
          ),
        tags = AuthenticatedTag :: Nil
      ),
      Field(
        "monthlyCashflowAverages",
        MonthlyCashflowAveragesType,
        description = Some(""),
        arguments = StartDateArg :: EndDateArg :: MonetaryAccountConfigurationsArg :: Nil,
        resolve = c =>
          c.ctx.cashflowRepo
            .fetchMonthlyAverages(
              AuthContext(c).userId,
              c.arg(MonetaryAccountConfigurationsArg).toSet,
              c.arg(StartDateArg).atStartOfDay().toInstant(ZoneOffset.UTC),
              c.arg(EndDateArg).atStartOfDay().toInstant(ZoneOffset.UTC)
          ),
        tags = AuthenticatedTag :: Nil
      ),
      Field(
        "monthlyBalances",
        ListType(MonthlyBalanceType),
        description = Some(""),
        arguments = StartDateArg :: EndDateArg :: MonetaryAccountConfigurationsArg :: Nil,
        resolve = c => {
          c.ctx.balancesRepo
            .fetchMonthlyBetween(
              AuthContext(c).userId,
              c.arg(MonetaryAccountConfigurationsArg).toSet,
              c.arg(StartDateArg).atStartOfDay().toInstant(ZoneOffset.UTC),
              c.arg(EndDateArg).atStartOfDay().toInstant(ZoneOffset.UTC)
            )
        },
        tags = AuthenticatedTag :: Nil
      ),
      Field(
        "recurringTransactions",
        ListType(RecurringTransactionType),
        description = Some(""),
        arguments = TransactionDirectionArg :: MonetaryAccountConfigurationsArg :: Nil,
        resolve = c =>
          c.ctx.recurringTransactionRepo
            .fetchActiveByUserId(AuthContext(c).userId, c.arg(MonetaryAccountConfigurationsArg).toSet, c.arg(TransactionDirectionArg)),
        tags = AuthenticatedTag :: Nil
      ),
      Field(
        "recurringSummed",
        RecurringSummedType,
        description = Some(""),
        arguments = MonetaryAccountConfigurationsArg :: Nil,
        resolve = c =>
          c.ctx.recurringTransactionRepo
            .fetchSummedByUser(AuthContext(c).userId, c.arg(MonetaryAccountConfigurationsArg).toSet),
        tags = AuthenticatedTag :: Nil
      ),
      Field(
        "leftOverUntil",
        LeftOverUntilType,
        description = Some(""),
        arguments = MonetaryAccountConfigurationsArg :: Nil,
        resolve = c =>
          c.ctx.budgettingService
            .fetchLeftOverUntil(AuthContext(c).userId, c.arg(MonetaryAccountConfigurationsArg).toSet),
        tags = AuthenticatedTag :: Nil
      ),
      Field(
        "allocationCounterparties",
        ListType(AllocationCounterpartyType),
        resolve = c => c.ctx.allocationCounterpartyRepository.fetchAll(),
        tags = AuthenticatedTag :: Nil
      ),
      Field(
        "transactionCategories",
        TransactionCategoriesResponseType,
        resolve = _.ctx.transactionCategoryRepository.fetchInboundAndOutboundCategories(),
        tags = AuthenticatedTag :: Nil
      )
    )
  )

  private implicit val SingleBalanceSnapshotType: InputObjectType[SingleBalanceSnapshot] = deriveInputObjectType()
  private lazy val BalanceSnapshotBatchType: InputObjectType[BalanceSnapshotBatch]       = deriveInputObjectType()

  private implicit val singleBalanceSnapshotFormat: Format[SingleBalanceSnapshot] = Json.format
  private implicit val balanceSnapshotFormat: Format[BalanceSnapshotBatch]        = Json.format
  private val BalanceSnapshotBatchArg                                             = Argument("balanceSnapshotBatch", BalanceSnapshotBatchType)

  private val ClusterIdArg = Argument("clusterId", StringUuidType)

  private val Mutation = ObjectType(
    "Mutation",
    fields[GraphqlContext, Unit](
      Field(
        "provideBalanceSnapshotBatch",
        StringType,
        arguments = BalanceSnapshotBatchArg :: Nil,
        resolve = c => {
          // TODO: allow only for monetary accounts on the given account
          c.ctx.monetaryAccountService.storeBalanceSnapshotBatch(c.arg(BalanceSnapshotBatchArg))
          "OK"
        },
        tags = AuthenticatedTag :: Nil
      ),
      Field(
        "selectMonetaryAccountCluster",
        StringType,
        arguments = ClusterIdArg :: Nil,
        resolve = c => {
          c.ctx.monetaryAccountClusterService
            .selectMonetaryAccountCluster(AuthContext(c).userId, c.arg(ClusterIdArg))
          "OK"
        },
        tags = AuthenticatedTag :: Nil
      ),
      Field(
        "setCategoryOnAllocationCounterparty", // TODO: leftoff
        StringType,
        arguments = BalanceSnapshotBatchArg :: Nil,
        resolve = c => {
          // TODO: allow only for ibans in the given account
          c.ctx.monetaryAccountService.storeBalanceSnapshotBatch(c.arg(BalanceSnapshotBatchArg))
          "OK"
        },
        tags = AuthenticatedTag :: Nil
      )
    )
  )

  val Resolver: DeferredResolver[GraphqlContext] = DeferredResolver.fetchers(
    monetaryAccountsFetcher,
    transactionsFetcher,
    latestBalancesFetcher,
    transactionCategoryFetcher
  )
  val AppSchema: Schema[GraphqlContext, Unit] = Schema(QueryType, Some(Mutation))
}

trait MonetaryAccountService {
  def fetchMultipleById(ids: Seq[types.MonetaryAccountId]): Future[Seq[MonetaryAccount]]
  def fetchByConfigurations(userId: types.UserId, configs: Set[MonetaryAccountConfiguration]): Future[Seq[MonetaryAccount]]

  def storeBalanceSnapshotBatch(batch: BalanceSnapshotBatch): Unit
}

trait TransactionRepository {
  def fetchMultipleById(ids: Seq[types.TransactionId]): Future[Seq[Transaction]]
}

trait CashflowRepository {
  def fetchMonthlyBetween(
      userId: types.UserId,
      monetaryAccounts: Set[MonetaryAccountConfiguration],
      startDate: Instant,
      endDate: Instant
  ): Future[Seq[MonthlyCashflow]]

  def fetchMonthlyAverages(
      userId: types.UserId,
      monetaryAccounts: Set[MonetaryAccountConfiguration],
      startDate: Instant,
      endDate: Instant
  ): Future[MonthlyCashflowAverages]
}

trait BalancesRepository {
  def fetchMonthlyBetween(
      userId: types.UserId,
      monetaryAccount: Set[MonetaryAccountConfiguration],
      startDate: Instant,
      endDate: Instant
  ): Future[Seq[MonthlyBalance]]

  def fetchLatestFor(monetaryAccountId: Seq[types.MonetaryAccountId]): Future[Seq[LatestBalance]]
}

trait RecurringTransactionRepository {
  def fetchActiveByUserId(
      userId: types.UserId,
      monetaryAccount: Set[MonetaryAccountConfiguration],
      direction: types.TransactionDirection
  ): Future[Seq[RecurringTransaction]]

  def fetchSummedByUser(
      userId: types.UserId,
      monetaryAccount: Set[MonetaryAccountConfiguration]
  ): Future[RecurringSummed]
}

trait AllocationCounterpartyRepository {
  def fetchAll(): Future[Seq[AllocationCounterparty]]
}

trait TransactionCategoryRepository {
  def fetchInboundCategories(): Future[Seq[TransactionCategory]]
  def fetchOutboundCategories(): Future[Seq[TransactionCategory]]
  def fetchMultipleById(ids: Seq[String]): Future[Seq[TransactionCategory]]

  def fetchInboundAndOutboundCategories()(implicit ec: ExecutionContext): Future[TransactionCategoriesResponse] =
    fetchOutboundCategories()
      .zip(fetchInboundCategories())
      .map {
        case (outbound, inbound) =>
          TransactionCategoriesResponse(
            outbound = outbound,
            inbound = inbound
          )
      }
}

trait MonetaryAccountClusterService {
  def fetchAllByUser(userId: types.UserId): Future[Seq[MonetaryAccountCluster]]
  def selectMonetaryAccountCluster(userId: types.UserId, clusterId: types.MonetaryAccountClusterId): Future[Unit]
}

trait BudgettingService {
  def fetchLeftOverUntil(userId: types.UserId, monetaryAccount: Set[MonetaryAccountConfiguration]): Future[LeftOverUntil]
}

package graphql
import akka.stream.Materializer
import akka.stream.scaladsl.Sink
import javax.inject.{Inject, Singleton}
import modules.monetaryaccountclusters
import streaming.AkkaStreamImplicits._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class MonetaryAccountClusterGraphqlAdapter @Inject() (monetaryAccountClusterService: monetaryaccountclusters.MonetaryAccountClusterService)(
    implicit ec: ExecutionContext,
    mat: Materializer
) extends MonetaryAccountClusterService {
  override def fetchAllByUser(userId: types.UserId): Future[Seq[MonetaryAccountCluster]] =
    monetaryAccountClusterService
      .fetchAllByUser(userId)
      .map(mapToGraphModel)
      .runWith(Sink.seq)

  override def selectMonetaryAccountCluster(userId: types.UserId, clusterId: types.MonetaryAccountClusterId): Future[Unit] =
    monetaryAccountClusterService.selectMonetaryAccountCluster(userId, clusterId)

  private def mapToGraphModel(cluster: monetaryaccountclusters.MonetaryAccountCluster): MonetaryAccountCluster = {
    MonetaryAccountCluster(
      id = cluster.id,
      clusterType = cluster.clusterType match {
        case monetaryaccountclusters.MonetaryAccountClusterType.All         => MonetaryAccountClusterType.All
        case monetaryaccountclusters.MonetaryAccountClusterType.UserDefined => MonetaryAccountClusterType.UserDefined
      },
      name = cluster.name,
      monetaryAccounts = cluster.monetaryAccounts.map(account =>
        MonetaryAccountConfiguration(
          monetaryAccountId = account.monetaryAccountId,
          joint = account.joint
        )
      ),
      selected = cluster.selected
    )
  }
}

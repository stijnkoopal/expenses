package graphql

import java.time.temporal.ChronoUnit._
import java.time.{Instant, YearMonth, ZoneId, ZonedDateTime}

import akka.NotUsed
import akka.stream.Materializer
import akka.stream.scaladsl.{Sink, Source}
import eu.timepit.refined.W
import eu.timepit.refined.api.Refined
import eu.timepit.refined.auto._
import eu.timepit.refined.boolean.And
import eu.timepit.refined.numeric.{Greater, Less}
import javax.inject.{Inject, Singleton}
import modules.core.domain.TransactionVisibilityResolver
import modules.core.projections.{MonetaryAccountRepository, TransactionRepository}
import modules.core.{domain, projections}
import modules.recurring.projections.{RecurringMaterializedTransaction, RecurringTransactionRow, RecurringTransactionsRepository}
import streaming.AkkaStreamImplicits._

import scala.concurrent.Future

/**
  * TODO: very unclear code + fetches accounts twice
  */
@Singleton
class BudgettingGraphqlAdapter @Inject() (
    directionResolver: TransactionVisibilityResolver,
    recurringTransactionRepository: RecurringTransactionsRepository,
    transactionRepository: TransactionRepository,
    accountRepository: MonetaryAccountRepository
)(implicit mat: Materializer)
    extends BudgettingService {
  def fetchLeftOverUntil(userId: types.UserId, configs: Set[MonetaryAccountConfiguration]): Future[LeftOverUntil] = {
    val startDate = getStartDate(24)
    val endDate   = startDate.plus(1, MONTHS)

    val outboundTransactions = fetchTransactions(userId, configs, startDate.toInstant, endDate.toInstant)
      .map(txs => txs.collect { case (tx, direction) if direction == types.TransactionDirection.Outbound => tx })

    val recurringOutboundTransactionInstances =
      fetchOutboundRecurringTransactionInstances(userId, configs, startDate.toInstant, endDate.toInstant)
    val recurringOutboundTransactions = fetchOutboundRecurringTransactions(userId, configs)
    val incomeInPastYear = fetchAverageIncomeOverPeriod(
      userId = userId,
      monetaryAccounts = configs,
      startDate = ZonedDateTime.now().withDayOfMonth(1).minusYears(1).truncatedTo(DAYS),
      endDate = ZonedDateTime.now().withDayOfMonth(1).truncatedTo(DAYS)
    )

    val zipped = Source.zipN(
      Seq(
        outboundTransactions,
        recurringOutboundTransactionInstances,
        recurringOutboundTransactions,
        incomeInPastYear
      )
    )

    zipped
      .map {
        case Vector(
            outboundTransactions: Seq[projections.Transaction],
            recurringOutboundTransactionInstances: Seq[RecurringMaterializedTransaction],
            recurringOutboundTransactions: Seq[RecurringTransactionRow],
            avgIncomeInPastYear: BigDecimal
            ) =>
          val outboundTxWithoutRecurring = outboundTransactions.filter(tx => !recurringOutboundTransactionInstances.exists(_.id == tx.id))

          val outboundSoFar = outboundTxWithoutRecurring.map(tx => -1 * tx.amount.abs * findMultiplier(configs, tx.monetaryAccountId)).sum
          val recurringOutbound =
            recurringOutboundTransactions.map(tx => -1 * tx.amount.abs * findMultiplier(configs, tx.monetaryAccountId)).sum

          LeftOverUntil(
            leftOver = avgIncomeInPastYear + outboundSoFar + recurringOutbound,
            from = startDate.toInstant,
            until = endDate.toInstant,
            outboundSoFar = outboundSoFar,
            recurringOutbound = recurringOutbound,
            projectedInbound = avgIncomeInPastYear
          )
      }
      .orElse(Source.single(emptyLeftOver(startDate.toInstant, endDate.toInstant)))
      .runWith(Sink.head)
  }

  private def getStartDate(startFromDayOfMonth: Int Refined And[Greater[W.`0`.T], Less[W.`31`.T]]): ZonedDateTime = {
    val now = Instant.now().atZone(ZoneId.of("UTC"))
    if (now.getDayOfMonth >= startFromDayOfMonth) now.withDayOfMonth(startFromDayOfMonth)
    else now.minusMonths(1).withDayOfMonth(startFromDayOfMonth)
  }

  private def fetchMonetaryAccounts(
      userId: types.UserId,
      configs: Set[MonetaryAccountConfiguration]
  ): Source[Seq[domain.MonetaryAccountConfiguration], NotUsed] =
    accountRepository
      .fetchByUserAndIds(userId, configs.map(_.monetaryAccountId).toSeq)
      .map(account =>
        domain.MonetaryAccountConfiguration(
          monetaryAccountId = account.id,
          joint = configs.find(_.monetaryAccountId == account.id).exists(_.joint),
          iban = account.iban
        )
      )
      .grouped(Int.MaxValue)

  private def fetchTransactions(
      userId: types.UserId,
      monetaryAccounts: Set[MonetaryAccountConfiguration],
      startDate: Instant,
      endDate: Instant
  ): Source[Seq[(projections.Transaction, types.TransactionDirection)], NotUsed] =
    fetchMonetaryAccounts(userId, monetaryAccounts)
      .flatMapConcat(accounts => {
        val transactions = transactionRepository.fetchByMonetaryAccountsBetween(
          ids = accounts.map(_.monetaryAccountId).toSet,
          startDate = startDate.truncatedTo(DAYS),
          endDate = endDate.plus(1, DAYS).truncatedTo(DAYS)
        )

        transactions
          .map(transaction => (transaction, directionResolver.resolveDirection(transaction.toMinimalTransaction, accounts.toSet)))
          .collect { case (transaction, Some(direction)) => (transaction, direction) }
      })
      .grouped(Int.MaxValue)

  private def fetchOutboundRecurringTransactionInstances(
      userId: types.UserId,
      monetaryAccounts: Set[MonetaryAccountConfiguration],
      startDate: Instant,
      endDate: Instant
  ): Source[Seq[RecurringMaterializedTransaction], NotUsed] =
    recurringTransactionRepository
      .fetchActiveOutboundInstancesByMonetaryAccountIdsInPeriod(monetaryAccounts.map(_.monetaryAccountId).toSeq, startDate, endDate)
      .grouped(Int.MaxValue)

  private def fetchOutboundRecurringTransactions(
      userId: types.UserId,
      monetaryAccounts: Set[MonetaryAccountConfiguration]
  ): Source[Seq[RecurringTransactionRow], NotUsed] =
    fetchMonetaryAccounts(userId, monetaryAccounts)
      .flatMapConcat(accounts => {
        recurringTransactionRepository
          .fetchActiveOutboundByMonetaryAccountIds(monetaryAccounts.map(_.monetaryAccountId).toSeq)
          .map(transaction => (transaction, directionResolver.resolveDirection(transaction.toMinimalTransaction, accounts.toSet)))
          .collect { case (transaction, Some(_)) => transaction }
          .grouped(Int.MaxValue)
      })

  def fetchAverageIncomeOverPeriod(
      userId: types.UserId,
      monetaryAccounts: Set[MonetaryAccountConfiguration],
      startDate: ZonedDateTime,
      endDate: ZonedDateTime
  ): Source[BigDecimal, NotUsed] = {
    val numberOfMonths =
      MONTHS.between(YearMonth.from(startDate), YearMonth.from(endDate))

    fetchTransactions(userId, monetaryAccounts, startDate.toInstant, endDate.toInstant)
      .map(txs =>
        txs
          .collect { case (tx, direction) if direction == types.TransactionDirection.Inbound => tx }
          .map(tx => tx.amount.abs * findMultiplier(monetaryAccounts, tx.monetaryAccountId))
          .sum / numberOfMonths
      )
      .orElse(Source.single(BigDecimal(0)))
  }

  private def findMultiplier(accounts: Set[MonetaryAccountConfiguration], monetaryAccountId: types.MonetaryAccountId): BigDecimal =
    accounts
      .find(a => monetaryAccountId == a.monetaryAccountId)
      .map(c => BigDecimal(if (c.joint) 0.5 else 1))
      .getOrElse(BigDecimal(1))

  private def emptyLeftOver(startDate: Instant, endDate: Instant): LeftOverUntil = LeftOverUntil(
    leftOver = 0,
    from = startDate,
    until = endDate,
    outboundSoFar = 0,
    recurringOutbound = 0,
    projectedInbound = 0
  )
}

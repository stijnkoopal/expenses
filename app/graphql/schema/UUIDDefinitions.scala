package graphql
package schema

import java.util.UUID

import sangria.schema.{ScalarAlias, StringType}
import sangria.validation.ValueCoercionViolation

trait UUIDDefinitions {
  case object IDViolation extends ValueCoercionViolation("Invalid ID")

  implicit val UUIDType: ScalarAlias[UUID, String] = ScalarAlias[UUID, String](
    StringType,
    toScalar = _.toString,
    fromScalar = idString =>
      try Right(UUID.fromString(idString))
      catch {
        case _: IllegalArgumentException => Left(IDViolation)
      }
  )
}

package graphql
package schema

import java.time._
import java.time.format.DateTimeFormatter

import sangria.ast.StringValue
import sangria.schema.ScalarType
import sangria.validation.ValueCoercionViolation

import scala.util.{Failure, Success, Try}

trait DateTimeDefinitions {
  case object LocalDateTimeCoercionViolation extends ValueCoercionViolation("Datetime value (eg 2011-12-03T10:15:30) expected")
  case object LocalDateCoercionViolation     extends ValueCoercionViolation("Date value (eg 2011-12-03+01:00) expected")
  case object InstantCoercionViolation       extends ValueCoercionViolation("Date value (eg 2007-12-03T10:15:30.00Z) expected")
  case object PeriodCoercionViolation        extends ValueCoercionViolation("Period value (eg P10D) expected")

  implicit val LocalDateTimeType: ScalarType[LocalDateTime] = ScalarType[LocalDateTime](
    "LocalDateTime",
    coerceOutput = (value, _) => value.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
    coerceUserInput = {
      case s: String => parseLocalDateTime(s)
      case _         => Left(LocalDateTimeCoercionViolation)
    },
    coerceInput = {
      case strVal: StringValue => parseLocalDateTime(strVal.value)
      case _                   => Left(LocalDateTimeCoercionViolation)
    }
  )

  implicit val LocalDateType: ScalarType[LocalDate] = ScalarType[LocalDate](
    "LocalDate",
    coerceOutput = (value, _) => value.format(DateTimeFormatter.ISO_DATE),
    coerceUserInput = {
      case s: String => parseLocalDate(s)
      case _         => Left(LocalDateCoercionViolation)
    },
    coerceInput = {
      case strVal: StringValue => parseLocalDate(strVal.value)
      case _                   => Left(LocalDateCoercionViolation)
    }
  )

  implicit val InstantType: ScalarType[Instant] = ScalarType[Instant](
    "Instant",
    coerceOutput = (value, _) => DateTimeFormatter.ISO_LOCAL_DATE_TIME.withZone(ZoneId.of("UTC")).format(value),
    coerceUserInput = {
      case s: String => parseInstant(s)
      case _         => Left(InstantCoercionViolation)
    },
    coerceInput = {
      case strVal: StringValue => parseInstant(strVal.value)
      case _                   => Left(InstantCoercionViolation)
    }
  )

  implicit val PeriodType: ScalarType[Period] = ScalarType[Period](
    "Period",
    coerceOutput = (value, _) => value.toString,
    coerceUserInput = {
      case s: String => parsePeriod(s)
      case _         => Left(PeriodCoercionViolation)
    },
    coerceInput = {
      case strVal: StringValue => parsePeriod(strVal.value)
      case _                   => Left(PeriodCoercionViolation)
    }
  )

  private def parseLocalDateTime(s: String) = Try(LocalDateTime.parse(s)) match {
    case Success(date) => Right(date)
    case Failure(_)    => Left(LocalDateTimeCoercionViolation)
  }

  private def parseLocalDate(s: String) = Try(LocalDate.parse(s)) match {
    case Success(date) => Right(date)
    case Failure(_)    => Left(LocalDateCoercionViolation)
  }

  private def parseInstant(s: String) = Try(Instant.parse(s)) match {
    case Success(date) => Right(date)
    case Failure(_)    => Left(InstantCoercionViolation)
  }

  private def parsePeriod(s: String) = Try(Period.parse(s)) match {
    case Success(period) => Right(period)
    case Failure(_)      => Left(PeriodCoercionViolation)
  }
}

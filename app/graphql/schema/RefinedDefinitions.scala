package graphql.schema

import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.types.string.NonEmptyString
import eu.timepit.refined.refineV
import eu.timepit.refined.string.Uuid
import sangria.schema.{ScalarAlias, StringType}
import sangria.validation.ValueCoercionViolation

trait RefinedDefinitions {
  case class RefineViolation(error: String) extends ValueCoercionViolation(error)

  implicit val NonEmptyStringType: ScalarAlias[NonEmptyString, String] =
    ScalarAlias[NonEmptyString, String](StringType, _.value, i => refineV[NonEmpty](i).left.map(RefineViolation))

  implicit val StringUuidType: ScalarAlias[String Refined Uuid, String] =
    ScalarAlias[String Refined Uuid, String](StringType, _.value, i => refineV[Uuid](i).left.map(RefineViolation))

  implicit val IbanStringType: ScalarAlias[types.IbanString, String] =
    ScalarAlias[types.IbanString, String](StringType, _.value, i => refineV[types.Iban](i).left.map(RefineViolation))
}

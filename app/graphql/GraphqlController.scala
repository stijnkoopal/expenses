package graphql

import akka.actor.ActorSystem
import app.auth.{ActionWithOptionalAuth, SecurityContext}
import graphql.security.{AuthenticationEnforcer, AuthenticationException, AuthorisationException}
import javax.inject.{Inject, Singleton}
import play.api.libs.json._
import play.api.mvc._
import sangria.execution._
import sangria.marshalling.playJson._
import sangria.parser.{QueryParser, SyntaxError}
import sangria.renderer.SchemaRenderer

import scala.concurrent.Future
import scala.util.{Failure, Success}

@Singleton
class GraphqlController @Inject() (
    system: ActorSystem,
    monetaryAccountGraphqlAdapter: MonetaryAccountGraphqlAdapter,
    transactionGraphqlAdapter: TransactionGraphqlAdapter,
    recurringTransactionAdapter: RecurringTransactionGraphqlAdapter,
    cashflowGraphqlAdapter: CashflowGraphqlAdapter,
    balancesAdapter: BalancesGraphqlAdapter,
    maybeAuthAction: ActionWithOptionalAuth,
    allocationCounterpartyGraphqlAdapter: AllocationCounterpartyGraphqlAdapter,
    transactionCategoryRepository: TransactionCategoryGraphqlAdapter,
    monetaryAccountClusterGraphqlAdapter: MonetaryAccountClusterGraphqlAdapter,
    budgettingGraphqlAdapter: BudgettingGraphqlAdapter
) extends InjectedController {
  import system.dispatcher

  private val schema = new SchemaDefinition()

  def graphqlWithQueryParams(query: String, variables: Option[String], operation: Option[String]): Action[AnyContent] =
    maybeAuthAction.async { request =>
      executeQuery(request.securityContext, query, variables map parseVariables, operation)
    }

  def graphqlWithBody: Action[JsValue] = maybeAuthAction.async(parse.json) { request =>
    val query     = (request.body \ "query").as[String]
    val operation = (request.body \ "operationName").asOpt[String]

    val variables = (request.body \ "variables").toOption.flatMap {
      case JsString(vars) => Some(parseVariables(vars))
      case obj: JsObject  => Some(obj)
      case _              => None
    }

    executeQuery(request.securityContext, query, variables, operation)
  }

  private def parseVariables(variables: String) =
    if (variables.trim == "" || variables.trim == "null")
      Json.obj()
    else
      Json.parse(variables).as[JsObject]

  private def executeQuery(securityContext: SecurityContext, query: String, variables: Option[JsObject], operation: Option[String]) =
    QueryParser.parse(query) match {
      case Success(queryAst) =>
        Executor
          .execute(
            schema = schema.AppSchema,
            queryAst = queryAst,
            userContext = contextFor(securityContext),
            operationName = operation,
            variables = variables getOrElse Json.obj(),
            exceptionHandler = exceptionHandler,
            deferredResolver = schema.Resolver,
            middleware = AuthenticationEnforcer :: Nil,
            queryReducers = List(
              QueryReducer.rejectMaxDepth[GraphqlContext](15),
              QueryReducer.rejectComplexQueries[GraphqlContext](4000, (_, _) => TooComplexQueryError)
            )
          )
          .map(Ok(_))
          .recover {
            case error: QueryAnalysisError => BadRequest(error.resolveError)
            case error: ErrorWithResolver  => InternalServerError(error.resolveError)
          }

      // can't parse GraphQL query, return error
      case Failure(error: SyntaxError) =>
        Future.successful(
          BadRequest(
            Json.obj(
              "syntaxError" -> error.getMessage,
              "locations" -> Json.arr(
                Json.obj("line" -> error.originalError.position.line, "column" -> error.originalError.position.column)
              )
            )
          )
        )

      case Failure(error) =>
        throw error
    }

  def renderSchema: Action[AnyContent] = maybeAuthAction {
    Ok(SchemaRenderer.renderSchema(schema.AppSchema))
  }

  lazy val exceptionHandler: ExceptionHandler = ExceptionHandler {
    case (_, error @ TooComplexQueryError)         => HandledException(error.getMessage)
    case (_, error @ MaxQueryDepthReachedError(_)) => HandledException(error.getMessage)
    case (_, AuthenticationException(message))     => HandledException(message)
    case (_, AuthorisationException(message))      => HandledException(message)
  }

  case object TooComplexQueryError extends Exception("Query is too expensive.")

  private def contextFor(securityContext: SecurityContext) = GraphqlContext(
    securityContext = securityContext,
    monetaryAccountService = monetaryAccountGraphqlAdapter,
    transactionRepo = transactionGraphqlAdapter,
    recurringTransactionRepo = recurringTransactionAdapter,
    cashflowRepo = cashflowGraphqlAdapter,
    balancesRepo = balancesAdapter,
    allocationCounterpartyRepository = allocationCounterpartyGraphqlAdapter,
    transactionCategoryRepository = transactionCategoryRepository,
    monetaryAccountClusterService = monetaryAccountClusterGraphqlAdapter,
    budgettingService = budgettingGraphqlAdapter
  )
}

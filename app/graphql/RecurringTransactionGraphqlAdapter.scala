package graphql

import akka.NotUsed
import akka.stream.Materializer
import akka.stream.scaladsl.{Sink, Source}
import javax.inject.{Inject, Singleton}
import modules.core.domain
import modules.core.domain.TransactionVisibilityResolver
import modules.core.projections.MonetaryAccountRepository
import modules.recurring.projections.RecurringTransactionsRepository
import streaming.AkkaStreamImplicits._

import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps

@Singleton
class RecurringTransactionGraphqlAdapter @Inject() (
    monetaryAccountRepository: MonetaryAccountRepository,
    recurringTransactionRepository: RecurringTransactionsRepository,
    visibilityResolver: TransactionVisibilityResolver
)(implicit val mat: Materializer, ec: ExecutionContext)
    extends graphql.RecurringTransactionRepository {
  override def fetchActiveByUserId(
      userId: types.UserId,
      monetaryAccounts: Set[MonetaryAccountConfiguration],
      direction: types.TransactionDirection
  ): Future[Seq[RecurringTransaction]] = {
    fetchMonetaryAccounts(userId, monetaryAccounts)
      .flatMapConcat(accounts => {
        val transactions = fetchRecurringTransactions(accounts.toSet, direction)
        transactions.collect {
          case recurringTx if visibilityResolver.resolveDirection(recurringTx.toMinimalTransaction, accounts.toSet).isDefined => recurringTx
        }
      })
      .map(recurringTransaction =>
        RecurringTransaction(
          id = recurringTransaction.id,
          startDate = recurringTransaction.startDate,
          endDate = recurringTransaction.endDate,
          amount = recurringTransaction.amount * findMultiplier(monetaryAccounts, recurringTransaction.monetaryAccountId),
          payer = TransactionParty(
            iban = recurringTransaction.payer.iban,
            name = recurringTransaction.payer.name
          ),
          payee = TransactionParty(
            iban = recurringTransaction.payee.iban,
            name = recurringTransaction.payee.name
          ),
          frequency = recurringTransaction.frequency,
          monetaryAccountId = recurringTransaction.monetaryAccountId
        )
      )
      .runWith(Sink.seq)
  }

  override def fetchSummedByUser(
      userId: types.UserId,
      monetaryAccounts: Set[MonetaryAccountConfiguration]
  ): Future[RecurringSummed] = {
    val inbound = fetchMonetaryAccounts(userId, monetaryAccounts)
      .flatMapConcat(accounts => {
        val transactions = fetchRecurringTransactions(accounts.toSet, types.TransactionDirection.Inbound)
        transactions.collect {
          case recurringTx if visibilityResolver.resolveDirection(recurringTx.toMinimalTransaction, accounts.toSet).isDefined => recurringTx
        }
      })
      .grouped(Int.MaxValue)
      .map(
        _.map(recurringTransaction =>
          recurringTransaction.amount.abs * findMultiplier(monetaryAccounts, recurringTransaction.monetaryAccountId)
        ).sum
      )
      .orElse(Source.single(BigDecimal(0)))

    val outbound = fetchMonetaryAccounts(userId, monetaryAccounts)
      .flatMapConcat(accounts => {
        val transactions = fetchRecurringTransactions(accounts.toSet, types.TransactionDirection.Outbound)
        transactions.collect {
          case recurringTx if visibilityResolver.resolveDirection(recurringTx.toMinimalTransaction, accounts.toSet).isDefined => recurringTx
        }
      })
      .grouped(Int.MaxValue)
      .map(
        _.map(recurringTransaction =>
          -1 * recurringTransaction.amount.abs * findMultiplier(monetaryAccounts, recurringTransaction.monetaryAccountId)
        ).sum
      )
      .orElse(Source.single(BigDecimal(0)))

    Source
      .zipN(Seq(inbound, outbound))
      .map(seq =>
        RecurringSummed(
          inbound = seq.head,
          outbound = seq(1)
        )
      )
      .runWith(Sink.head)
  }

  private def fetchMonetaryAccounts(
      userId: types.UserId,
      configs: Set[MonetaryAccountConfiguration]
  ): Source[Seq[domain.MonetaryAccountConfiguration], NotUsed] =
    monetaryAccountRepository
      .fetchByUserAndIds(userId, configs.map(_.monetaryAccountId).toSeq)
      .map(account =>
        domain.MonetaryAccountConfiguration(
          monetaryAccountId = account.id,
          joint = configs.find(_.monetaryAccountId == account.id).exists(_.joint),
          iban = account.iban
        )
      )
      .grouped(Int.MaxValue)

  private def fetchRecurringTransactions(accounts: Set[domain.MonetaryAccountConfiguration], direction: types.TransactionDirection) =
    direction match {
      case types.TransactionDirection.Inbound =>
        recurringTransactionRepository
          .fetchActiveInboundByMonetaryAccountIds(accounts.map(_.monetaryAccountId).toSeq)
          .map(tx => tx.copy(amount = tx.amount.abs))

      case types.TransactionDirection.Outbound =>
        recurringTransactionRepository
          .fetchActiveOutboundByMonetaryAccountIds(accounts.map(_.monetaryAccountId).toSeq)
          .map(tx => tx.copy(amount = tx.amount.abs * -1))
    }

  private def findMultiplier(accounts: Set[MonetaryAccountConfiguration], monetaryAccountId: types.MonetaryAccountId): BigDecimal =
    accounts
      .find(a => monetaryAccountId == a.monetaryAccountId)
      .map(c => BigDecimal(if (c.joint) 0.5 else 1))
      .getOrElse(BigDecimal(1))

}

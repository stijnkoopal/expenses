package graphql.security

import app.auth.{Authenticated, SecurityContext}
import sangria.execution._
import sangria.schema.Context

case class AuthenticationException(message: String) extends Exception(message)
case class AuthorisationException(message: String)  extends Exception(message)

case object AuthenticatedTag extends FieldTag

case class GraphqlAuthenticated(delegate: Authenticated) extends MiddlewareAttachment

trait AuthContext {
  def securityContext: SecurityContext

  def ensureAuthenticated(): GraphqlAuthenticated = securityContext match {
    case x @ Authenticated(_, _) => GraphqlAuthenticated(x)
    // TODO: make sure we return a 401 n this case
    case _ => throw AuthenticationException("Not authenticated")
  }
}

object AuthContext {
  def apply(ctx: Context[_, _]): Authenticated =
    ctx.attachment[GraphqlAuthenticated] match {
      case Some(x) => x.delegate
      case None    => throw AuthenticationException("Did you forget the Authorised tag on the field?")
    }
}

object AuthenticationEnforcer extends Middleware[AuthContext] with MiddlewareBeforeField[AuthContext] {
  type QueryVal = Unit
  type FieldVal = Unit

  override def beforeQuery(context: MiddlewareQueryContext[AuthContext, _, _]): Unit                    = ()
  override def afterQuery(queryVal: QueryVal, context: MiddlewareQueryContext[AuthContext, _, _]): Unit = ()

  override def beforeField(
      queryVal: QueryVal,
      mctx: MiddlewareQueryContext[AuthContext, _, _],
      ctx: Context[AuthContext, _]
  ): BeforeFieldResult[AuthContext, Unit] = {
    val requireAuth = ctx.field.tags contains AuthenticatedTag
    val authContext = ctx.ctx

    if (requireAuth) {
      val auth = authContext.ensureAuthenticated()
      BeforeFieldResult(fieldVal = (), attachment = Some(auth))
    } else {
      BeforeFieldResult(fieldVal = (), attachment = None)
    }
  }
}

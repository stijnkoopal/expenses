package graphql

import java.time.ZoneOffset

import akka.stream.Materializer
import akka.stream.scaladsl.Sink
import javax.inject.{Inject, Singleton}
import modules.core.projections.MonetaryAccountRow
import modules.core.{domain, projections}
import streaming.AkkaStreamImplicits._

import scala.concurrent.Future

@Singleton
class MonetaryAccountGraphqlAdapter @Inject() (
    repo: projections.MonetaryAccountRepository,
    monetaryAccountService: domain.MonetaryAccountService
)(implicit val mat: Materializer)
    extends MonetaryAccountService {
  override def fetchMultipleById(ids: Seq[types.MonetaryAccountId]): Future[Seq[graphql.MonetaryAccount]] =
    repo
      .fetchByIds(ids.toSet)
      .map(mapToMonetaryAccount) // TODO: also add configuration to this thingy
      .runWith(Sink.seq)

  override def fetchByConfigurations(
      userId: types.UserId,
      configurations: Set[MonetaryAccountConfiguration]
  ): Future[Seq[graphql.MonetaryAccount]] =
    repo
      .fetchByUserAndIds(userId, configurations.map(_.monetaryAccountId).toSeq)
      .map(account => mapToMonetaryAccount(account, configurations))
      .runWith(Sink.seq)

  override def storeBalanceSnapshotBatch(batch: BalanceSnapshotBatch): Unit = {
    batch.balances.foreach(update =>
      monetaryAccountService.updateBalance(monetaryAccountId = update.monetaryAccount, newBalance = update.balance)
    )
  }

  private def mapToMonetaryAccount(account: MonetaryAccountRow): graphql.MonetaryAccount =
    MonetaryAccount(
      id = account.id,
      iban = account.iban,
      alias = account.alias,
      currency = account.currency,
      joint = account.joint,
      lastUpdated = account.updated.getOrElse(account.created).toInstant(ZoneOffset.UTC),
      autoUpdated = account.autoUpdated,
      institution = account.institution
    )

  private def mapToMonetaryAccount(
      account: MonetaryAccountRow,
      configurations: Set[MonetaryAccountConfiguration]
  ): graphql.MonetaryAccount = {
    val configuration = configurations.find(_.monetaryAccountId == account.id)

    MonetaryAccount(
      id = account.id,
      iban = account.iban,
      alias = account.alias,
      currency = account.currency,
      joint = configuration.map(_.joint).getOrElse(account.joint),
      lastUpdated = account.updated.getOrElse(account.created).toInstant(ZoneOffset.UTC),
      autoUpdated = account.autoUpdated,
      institution = account.institution
    )
  }
}

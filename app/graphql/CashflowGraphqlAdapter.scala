package graphql

import java.time.temporal.ChronoField._
import java.time.temporal.ChronoUnit
import java.time.temporal.ChronoUnit.DAYS
import java.time.{Instant, LocalDate, YearMonth, ZoneId, ZonedDateTime}

import akka.NotUsed
import akka.stream.Materializer
import akka.stream.scaladsl.{Sink, Source}
import javax.inject.{Inject, Singleton}
import modules.core.domain.TransactionVisibilityResolver
import modules.core.projections.{MonetaryAccountRepository, TransactionRepository}
import modules.core.{domain, projections}
import modules.recurring.projections.{RecurringMaterializedTransaction, RecurringTransactionsRepository}
import streaming.AkkaStreamImplicits._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class CashflowGraphqlAdapter @Inject()(
    directionResolver: TransactionVisibilityResolver,
    transactionRepository: TransactionRepository,
    recurringTransactionsRepository: RecurringTransactionsRepository,
    accountRepository: MonetaryAccountRepository
)(implicit val mat: Materializer, ec: ExecutionContext)
    extends graphql.CashflowRepository {

  override def fetchMonthlyBetween(
      userId: types.UserId,
      monetaryAccounts: Set[MonetaryAccountConfiguration],
      startDate: Instant,
      endDate: Instant
  ): Future[Seq[MonthlyCashflow]] = {
    val startOfMonth = 24

    val groupOf = (timestamp: ZonedDateTime) =>
      if (timestamp.getDayOfMonth >= startOfMonth) {
        val newTimestamp = timestamp.plus(1, ChronoUnit.MONTHS)
        s"${newTimestamp.get(YEAR)}-${newTimestamp.get(MONTH_OF_YEAR)}"
      } else {
        s"${timestamp.get(YEAR)}-${timestamp.get(MONTH_OF_YEAR)}"
    }

    fetchMonetaryAccounts(userId, monetaryAccounts)
      .flatMapConcat(accounts => {
        val transactions          = fetchTransactions(accounts.toSet, startDate, endDate)
        val recurringTransactions = fetchRecurringTransactions(accounts.toSet, startDate, endDate)

        val zipped = Source.zipN(Seq(transactions, recurringTransactions))

        zipped.map {
          case Vector(
              transactions: Seq[(projections.Transaction, types.TransactionDirection)],
              recurring: Seq[RecurringMaterializedTransaction]
              ) =>
            val filteredRecurring = recurring
              .map(recurringTx => transactions.find(tx => tx._1.id == recurringTx.id))
              .collect {
                case Some((tx, _)) if directionResolver.resolveDirection(tx.toMinimalTransaction, accounts.toSet).isDefined => tx
              }

            val perMonthTxs = transactions.groupBy {
              case (tx, _) => groupOf(tx.timestamp.atZone(ZoneId.of("UTC")))
            }

            val perMonthRecurring = filteredRecurring.groupBy(tx => groupOf(tx.timestamp.atZone(ZoneId.of("UTC"))))

            val months: Iterable[String] = (perMonthTxs.keys ++ perMonthRecurring.keys).toSet

            val result = months.foldLeft(Seq[MonthlyCashflow]()) {
              case (acc, month) =>
                val monthStart = LocalDate
                  .of(
                    month.substring(0, 4).toInt,
                    month.substring(5).toInt,
                    1
                  )
                  .atStartOfDay(ZoneId.of("UTC"))
                  .toInstant

                val thisMonth = MonthlyCashflow(
                  inbound = perMonthTxs
                    .getOrElse(month, Seq())
                    .collect {
                      case (tx, dir) if dir == types.TransactionDirection.Inbound =>
                        tx.amount.abs * findMultiplier(monetaryAccounts, tx.monetaryAccountId)
                    }
                    .sum,
                  outbound = perMonthTxs
                    .getOrElse(month, Seq())
                    .collect {
                      case (tx, dir) if dir == types.TransactionDirection.Outbound =>
                        tx.amount.abs * findMultiplier(monetaryAccounts, tx.monetaryAccountId)
                    }
                    .sum,
                  recurringOutbound = perMonthRecurring
                    .getOrElse(month, Seq())
                    .map(tx => tx.amount.abs * findMultiplier(monetaryAccounts, tx.monetaryAccountId))
                    .sum,
                  recurringInbound = 0,
                  monthStart = monthStart
                )

                acc :+ thisMonth
            }

            result.sortBy(_.monthStart)
        }
      })
      .runWith(Sink.headOption)
      .map {
        case Some(x) => x
        case _       => Seq()
      }
  }

  override def fetchMonthlyAverages(
      userId: types.UserId,
      monetaryAccounts: Set[MonetaryAccountConfiguration],
      startDate: Instant,
      endDate: Instant
  ): Future[MonthlyCashflowAverages] = {
    val numberOfMonths =
      ChronoUnit.MONTHS.between(YearMonth.from(startDate.atZone(ZoneId.of("UTC"))), YearMonth.from(endDate.atZone(ZoneId.of("UTC"))))

    fetchMonetaryAccounts(userId, monetaryAccounts)
      .flatMapConcat(accounts => fetchTransactions(accounts.toSet, startDate, endDate))
      .map(txs => {
        val groupedMap = txs
          .groupBy { case (_, direction) => direction }
          .view
          .map { case (direction, txs) => (direction, txs.map { case (tx, _) => tx }) }
          .toMap

        MonthlyCashflowAverages(
          inbound = groupedMap
            .getOrElse(types.TransactionDirection.Inbound, Seq())
            .map(tx => tx.amount.abs * findMultiplier(monetaryAccounts, tx.monetaryAccountId))
            .sum / numberOfMonths,
          outbound = groupedMap
            .getOrElse(types.TransactionDirection.Outbound, Seq())
            .map(tx => -1 * tx.amount.abs * findMultiplier(monetaryAccounts, tx.monetaryAccountId))
            .sum / numberOfMonths
        )
      })
      .orElse(Source.single(MonthlyCashflowAverages(0, 0)))
      .runWith(Sink.head)
  }

  private def fetchMonetaryAccounts(
      userId: types.UserId,
      configs: Set[MonetaryAccountConfiguration]
  ): Source[Seq[domain.MonetaryAccountConfiguration], NotUsed] =
    accountRepository
      .fetchByUserAndIds(userId, configs.map(_.monetaryAccountId).toSeq)
      .map(
        account =>
          domain.MonetaryAccountConfiguration(
            monetaryAccountId = account.id,
            joint = configs.find(_.monetaryAccountId == account.id).exists(_.joint),
            iban = account.iban
        ))
      .grouped(Int.MaxValue)

  private def fetchTransactions(
      accounts: Set[domain.MonetaryAccountConfiguration],
      startDate: Instant,
      endDate: Instant
  ): Source[Seq[(projections.Transaction, types.TransactionDirection)], NotUsed] = {
    val transactions = transactionRepository
      .fetchByMonetaryAccountsBetween(
        ids = accounts.map(_.monetaryAccountId),
        startDate = startDate.truncatedTo(DAYS),
        endDate.truncatedTo(DAYS)
      )

    transactions
      .map(transaction => (transaction, directionResolver.resolveDirection(transaction.toMinimalTransaction, accounts)))
      .collect { case (transaction, Some(direction)) => (transaction, direction) }
      .grouped(Int.MaxValue)
  }

  private def fetchRecurringTransactions(
      accounts: Set[domain.MonetaryAccountConfiguration],
      startDate: Instant,
      endDate: Instant
  ): Source[Seq[RecurringMaterializedTransaction], NotUsed] =
    recurringTransactionsRepository
      .fetchOutboundInstancesByMonetaryAccountIdsInPeriod(accounts.map(_.monetaryAccountId).toSeq, startDate, endDate)
      .grouped(Int.MaxValue)

  private def findMultiplier(accounts: Set[MonetaryAccountConfiguration], monetaryAccountId: types.MonetaryAccountId): BigDecimal =
    accounts
      .find(a => monetaryAccountId == a.monetaryAccountId)
      .map(c => BigDecimal(if (c.joint) 0.5 else 1))
      .getOrElse(BigDecimal(1))

}

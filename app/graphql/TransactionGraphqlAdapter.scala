package graphql

import akka.stream.Materializer
import akka.stream.scaladsl.Sink
import javax.inject.{Inject, Singleton}
import modules.core.{services, projections}
import streaming.AkkaStreamImplicits._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class TransactionGraphqlAdapter @Inject() (
    transactionRepository: projections.TransactionRepository
)(implicit val mat: Materializer, ec: ExecutionContext)
    extends graphql.TransactionRepository {
  override def fetchMultipleById(ids: Seq[types.TransactionId]): Future[Seq[graphql.Transaction]] =
    transactionRepository
      .fetchByIds(ids.toSet)
      .map(mapToTransaction)
      .runWith(Sink.seq)

  private def mapToTransaction(transaction: projections.Transaction) =
    graphql.Transaction(
      id = transaction.id,
      monetaryAccountId = transaction.monetaryAccountId,
      payer = transaction.payer.map(payer => TransactionParty(payer.iban, payer.name)),
      payee = transaction.payee.map(payee => TransactionParty(payee.iban, payee.name)),
      amount = transaction.amount,
      description = transaction.description,
      timestamp = transaction.timestamp
    )
}

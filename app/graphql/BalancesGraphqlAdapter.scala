package graphql

import java.time.Instant

import akka.stream.Materializer
import akka.stream.scaladsl.Sink
import javax.inject.{Inject, Singleton}
import modules.core.domain.BalancesService
import streaming.AkkaStreamImplicits._

import scala.concurrent.Future

@Singleton
class BalancesGraphqlAdapter @Inject() (
    balancesService: BalancesService
)(implicit val mat: Materializer)
    extends graphql.BalancesRepository {
  override def fetchMonthlyBetween(
      userId: types.UserId,
      monetaryAccounts: Set[MonetaryAccountConfiguration],
      startDate: Instant,
      endDate: Instant
  ): Future[Seq[MonthlyBalance]] =
    balancesService
      .fetchMonthlyBalancesBetween(userId, monetaryAccounts.map(_.monetaryAccountId).toSeq, startDate, endDate)
      .map(balance =>
        MonthlyBalance(
          monetaryAccountId = balance.monetaryAccount,
          monthStart = balance.timestamp,
          balance = balance.balance * findMultiplier(monetaryAccounts, balance.monetaryAccount)
        )
      )
      .runWith(Sink.seq)

  override def fetchLatestFor(monetaryAccountIds: Seq[types.MonetaryAccountId]): Future[Seq[LatestBalance]] =
    balancesService
      .fetchLatestFor(monetaryAccountIds.toSet)
      .map(balance =>
        LatestBalance(
          monetaryAccountId = balance.monetaryAccount,
          latestUpdate = balance.timestamp,
          balance = balance.balance
        )
      )
      .runWith(Sink.seq)

  private def findMultiplier(accounts: Set[MonetaryAccountConfiguration], monetaryAccountId: types.MonetaryAccountId): BigDecimal =
    accounts
      .find(a => monetaryAccountId == a.monetaryAccountId)
      .map(c => BigDecimal(if (c.joint) 0.5 else 1))
      .getOrElse(BigDecimal(1))
}

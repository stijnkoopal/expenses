package graphql

import akka.stream.Materializer
import akka.stream.scaladsl.Sink
import javax.inject.{Inject, Singleton}
import modules.allocation.domain.TransactionCategory
import streaming.AkkaStreamImplicits._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class TransactionCategoryGraphqlAdapter @Inject() (
    categoryRepository: modules.allocation.domain.TransactionCategoryRepository
)(implicit val mat: Materializer, ec: ExecutionContext)
    extends graphql.TransactionCategoryRepository {
  override def fetchInboundCategories(): Future[Seq[graphql.TransactionCategory]] =
    categoryRepository
      .findInboundCategories()
      .map(mapCategory)
      .runWith(Sink.seq)

  override def fetchOutboundCategories(): Future[Seq[graphql.TransactionCategory]] =
    categoryRepository
      .findOutboundCategories()
      .map(mapCategory)
      .runWith(Sink.seq)

  override def fetchMultipleById(ids: Seq[String]): Future[Seq[graphql.TransactionCategory]] =
    categoryRepository
      .findByIds(ids)
      .map(mapCategory)
      .runWith(Sink.seq)

  private def mapCategory(category: TransactionCategory): graphql.TransactionCategory = graphql.TransactionCategory(
    id = category.id,
    parentId = category.parentId,
    label = category.label,
    icon = category.id
  )
}

package persistence

import java.sql.ResultSet
import java.time.Period
import java.util.UUID

import eu.timepit.refined.api.{RefType, Refined, Validate}
import eu.timepit.refined.refineV
import scalikejdbc.interpolation.SQLSyntax
import scalikejdbc.{NoExtractor, ParameterBinderFactory, SQL, SQLBatch, TypeBinder, WithExtractor}

import scala.language.higherKinds
import scala.util.Try

private object Util {
  def unwrapRefinedType(param: Any): Any = param match {
    case p: Refined[_, _] => p.value
    case p: Iterable[_]   => p.map(unwrapRefinedType)
    case p: Option[_]     => p.map(unwrapRefinedType)
    case p                => p
  }
}

/**
  * Pretty stupid, but with scalalikejdbc you cannot use ParameterBinderFactory with StringInterpolation.
  * See http://scalikejdbc.org/documentation/operations.html
  *
  * Then we do it this way!!!!!
  */
class SqlInterpolationStringAcceptingRefinedTypes(private val s: StringContext) extends AnyVal {
  def sqlr[A](params: Any*): SQL[A, NoExtractor] = {
    val sqlInterpolation = new scalikejdbc.SQLInterpolationString(s)
    val refinedParams    = params.map(Util.unwrapRefinedType)
    sqlInterpolation.sql(refinedParams: _*)
  }

  def sqlrs(params: Any*): SQLSyntax = {
    val sqlInterpolation = new scalikejdbc.SQLInterpolationString(s)
    val refinedParams    = params.map(Util.unwrapRefinedType)
    sqlInterpolation.sqls(refinedParams: _*)
  }
}

object ScalalikejdbcImplicits {
  @inline implicit def implicitSqlInterpolationWithRefinedTypes(s: StringContext): SqlInterpolationStringAcceptingRefinedTypes =
    new SqlInterpolationStringAcceptingRefinedTypes(s)

  implicit class BatchByNameRefined[A, E <: WithExtractor](s: SQL[A, E]) {
    def batchByNameRefined(parameters: scala.collection.Seq[(Symbol, Any)]*): SQLBatch = {
      val refinedParams = parameters.map(s => s.map { case (symbol, value) => (symbol, Util.unwrapRefinedType(value)) })
      s.batchByName(refinedParams: _*)
    }
  }

  implicit val periodTypeBinder: TypeBinder[Period] = new TypeBinder[Period] {
    def apply(rs: ResultSet, label: String): Period = Period.parse(rs.getString(label))
    def apply(rs: ResultSet, index: Int): Period    = Period.parse(rs.getString(index))
  }

  implicit val periodParameterBinderFactory: ParameterBinderFactory[Period] = ParameterBinderFactory[Period] { value => (stmt, idx) =>
    stmt.setString(idx, value.toString)
  }

  implicit val uuidTypeBinder: TypeBinder[UUID] = new TypeBinder[UUID] {
    def apply(rs: ResultSet, label: String): UUID = UUID.fromString(rs.getString(label))
    def apply(rs: ResultSet, index: Int): UUID    = UUID.fromString(rs.getString(index))
  }

  implicit val uuidParameterBinderFactory: ParameterBinderFactory[UUID] = ParameterBinderFactory[UUID] { value => (stmt, idx) =>
    stmt.setString(idx, value.toString)
  }

  implicit def refinedTypeBinder[T, P](implicit validate: Validate[T, P], based: TypeBinder[T]): TypeBinder[Refined[T, P]] =
    based.map(x =>
      try {
        refineV[P].unsafeFrom(x)
      } catch {
        case e: IllegalArgumentException if e.getMessage.startsWith("Predicate failed: null") => throw new NullPointerException()
        case e: Throwable                                                                     => throw e
      }
    )

  implicit def refinedParameterBinderFactory[T, P, F[_, _]](
      implicit
      based: ParameterBinderFactory[T],
      refType: RefType[F]
  ): ParameterBinderFactory[F[T, P]] =
    based.contramap(refType.unwrap)
}

package streaming

import akka.NotUsed
import akka.stream._
import akka.stream.scaladsl.{Sink, Source}
import org.reactivestreams.{Publisher, Subscriber}

import scala.language.implicitConversions

object AkkaStreamImplicits {
  implicit def subscriberToSink[T](subscriber: Subscriber[T]): Sink[T, NotUsed]  = Sink.fromSubscriber(subscriber)
  implicit def publisherToSource[T](publisher: Publisher[T]): Source[T, NotUsed] = Source.fromPublisher(publisher)
  implicit def sourceToPublisher[T](source: Source[T, NotUsed])(implicit mat: Materializer): Publisher[T] =
    source.runWith(Sink.asPublisher(true))
}

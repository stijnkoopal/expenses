package streaming

import akka.stream.Materializer
import akka.stream.scaladsl.{Sink, Source}
import org.reactivestreams.Publisher

import scala.util.{Failure, Success, Try}

object AkkaStreamsUtil {
  def emptyPublisher[T]()(implicit mat: Materializer): Publisher[T] =
    Source
      .empty[T]
      .runWith(Sink.asPublisher(false))

  def publisherSingleFromLazyFunc[T](blockFn: => Try[T], fanout: Boolean = false)(implicit mat: Materializer): Publisher[T] = {
    Source
      .lazySource { () =>
        {
          blockFn match {
            case Success(value)     => Source.single(value)
            case Failure(exception) => Source.failed(exception)
          }
        }
      }
      .runWith(Sink.asPublisher[T](fanout = fanout))
  }

  def publisherSeqFromLazyFunc[T](blockFn: => Try[Seq[T]], fanout: Boolean = false)(implicit mat: Materializer): Publisher[T] = {
    Source
      .lazySource { () =>
        blockFn match {
          case Success(value)     => Source.fromIterator(() => value.iterator)
          case Failure(exception) => Source.failed(exception)
        }
      }
      .runWith(Sink.asPublisher[T](fanout = fanout))
  }
}

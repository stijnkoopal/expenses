package messaging

import akka.NotUsed
import akka.actor.CoordinatedShutdown
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}
import akka.stream.{KillSwitches, Materializer}
import javax.inject.{Inject, Singleton}
import org.reactivestreams.{Publisher, Subscriber}
import serde.{Deserializable, Serializable}

case class MessageWithEnvelope(payloadType: String, payload: Array[Byte])

trait MessagePublisher {
  def publishTo[T: serde.Serializable](channel: String, payloadType: String): Subscriber[T]
}

trait MessageReceiver {
  def receive[T: Deserializable](channel: String, payloadType: String): Publisher[T]
}

@Singleton
class InJvmAkkaStreamsMessaging @Inject() (cs: CoordinatedShutdown)(implicit mat: Materializer)
    extends MessagePublisher
    with MessageReceiver {
  private val bufferSize       = 1000
  private val overflowStrategy = akka.stream.OverflowStrategy.backpressure

  private val ((queue, killSwitch), publisher) = Source
    .queue[MessageWithEnvelope](bufferSize, overflowStrategy)
    .async
    .viaMat(KillSwitches.single)(Keep.both)
    .toMat(Sink.asPublisher(fanout = true))(Keep.both)
    .run()

  cs.addTask(CoordinatedShutdown.PhaseServiceUnbind, "stop-messaging") { () =>
    killSwitch.shutdown()
    queue.watchCompletion()
  }

  def shutdown(): Unit = killSwitch.shutdown()

  override def publishTo[T: serde.Serializable](channel: String, payloadType: String): Subscriber[T] =
    Flow[T]
      .mapAsync(1) { payload =>
        queue.offer(wrapPayload(payload, payloadType))
      }
      .to(Sink.ignore)
      .runWith(Source.asSubscriber)

  override def receive[T: Deserializable](channel: String, payloadType: String): Publisher[T] =
    filteredAndDeserializedMessagesFor(channel, payloadType)
      .runWith(Sink.asPublisher(true))

  private def filteredAndDeserializedMessagesFor[T: Deserializable](channel: String, payloadType: String): Source[T, NotUsed] =
    Source
      .fromPublisher(publisher)
      .filter(_.payloadType == payloadType)
      .async
      .map(x => Deserializable.deserialize(x.payload).get) // Deliberate .get to throw exception on deser error (resulting in error log)

  private def wrapPayload[T: serde.Serializable](payload: T, payloadType: String) =
    MessageWithEnvelope(
      payloadType = payloadType,
      payload = Serializable.serialize(payload)
    )
}

package messaging

import play.api.inject.{Binding, Module}
import play.api.{Configuration, Environment}

class MessagingModule extends Module {
  override def bindings(environment: Environment, configuration: Configuration): Seq[Binding[_]] = Seq(
    bind[MessagePublisher].to[InJvmAkkaStreamsMessaging],
    bind[MessageReceiver].to[InJvmAkkaStreamsMessaging]
  )
}

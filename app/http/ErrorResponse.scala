package http

import play.api.libs.json.{Format, Json}
import serde.PlayJsonDefaultSerialization

case class ErrorResponse(messages: String*)

object HttpSerde extends PlayJsonDefaultSerialization {
  implicit val errorResponseFormat: Format[ErrorResponse] = Json.format
}

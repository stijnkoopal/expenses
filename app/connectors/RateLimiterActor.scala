package connectors

import akka.actor.{Actor, ActorRef, Props}

import scala.collection.immutable
import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration.FiniteDuration

object RateLimiterActor {
  case object WantToPass
  case object MayPass

  case object ReplenishTokens

  def props(maxAvailableTokens: Int, tokenRefreshPeriod: FiniteDuration): Props =
    Props(new RateLimiterActor(maxAvailableTokens, tokenRefreshPeriod))
}

class RateLimiterActor(val maxAvailableTokens: Int, val tokenRefreshPeriod: FiniteDuration) extends Actor {
  import RateLimiterActor._
  import akka.actor.Status

  implicit val ec: ExecutionContextExecutor = context.system.dispatcher

  val tokenRefreshAmount: Int = maxAvailableTokens

  private var waitQueue    = immutable.Queue.empty[ActorRef]
  private var permitTokens = maxAvailableTokens
  private val replenishTimer =
    context.system.scheduler.scheduleWithFixedDelay(
      initialDelay = tokenRefreshPeriod,
      delay = tokenRefreshPeriod,
      receiver = self,
      ReplenishTokens
    )

  override def receive: Receive = open

  private val open: Receive = {
    case ReplenishTokens =>
      permitTokens = math.min(permitTokens + tokenRefreshAmount, maxAvailableTokens)
    case WantToPass =>
      permitTokens -= 1
      sender() ! MayPass
      if (permitTokens == 0) context.become(closed)
  }

  private val closed: Receive = {
    case ReplenishTokens =>
      permitTokens = math.min(permitTokens + tokenRefreshAmount, maxAvailableTokens)
      releaseWaiting()
    case WantToPass =>
      waitQueue = waitQueue.enqueue(sender())
  }

  private def releaseWaiting(): Unit = {
    val (toBeReleased, remainingQueue) = waitQueue.splitAt(permitTokens)
    waitQueue = remainingQueue
    permitTokens -= toBeReleased.size
    toBeReleased foreach (_ ! MayPass)
    if (permitTokens > 0) context.become(open)
  }

  override def postStop(): Unit = {
    replenishTimer.cancel()
    waitQueue foreach (_ ! Status.Failure(new IllegalStateException("rate limiter stopped")))
  }
}

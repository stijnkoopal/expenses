package connectors

import akka.NotUsed
import akka.actor.ActorRef
import akka.stream.scaladsl.Flow

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.FiniteDuration

object RateLimiterFlow {
  def limitGlobal[T](
      limiter: ActorRef,
      maxAllowedWait: FiniteDuration
  )(implicit ec: ExecutionContext): Flow[T, T, NotUsed] = {
    import akka.pattern.ask
    import akka.util.Timeout

    Flow[T]
      .mapAsync(4)((element: T) => {
        implicit val triggerTimeout: Timeout = Timeout(maxAllowedWait)
        val limiterTriggerFuture             = limiter ? RateLimiterActor.WantToPass
        limiterTriggerFuture.map(_ => element)
      })
  }
}

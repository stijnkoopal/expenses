package connectors.bunq

import java.time.Instant
import java.util.UUID

import connectors.bunq.api._
import eu.timepit.refined.api.Refined
import modules.core.domain.IdUtil
import modules.core.messaging.{MonetaryAccountDocument, TransactionDocument, TransactionPartyInMessage}
import modules.recurring.messaging.{RecurringDirectDebitTransactionDocument, RecurringScheduleDocument}

object TransactionMapper {
  def mapToTransactionDocument(userId: types.UserId, monetaryAccount: types.MonetaryAccountId, tx: BunqTransaction): TransactionDocument = {
    val id = IdUtil.calculateTransactionId(
      payerIban = tx.from.iban,
      payeeIban = tx.to.iban,
      amount = tx.amount.value,
      description = tx.description,
      transactionDate = tx.timestamp.toInstant
    )

    TransactionDocument(
      id = id,
      userId = userId,
      monetaryAccountId = monetaryAccount,
      currency = tx.amount.currency,
      amount = tx.amount.value,
      payer = CounterpartyMapper.mapCounterparty(tx.from),
      payee = CounterpartyMapper.mapCounterparty(tx.to),
      description = tx.description,
      direction = tx.direction,
      scheduleId = tx.scheduleId.map(_ =>
        IdUtil.calculateRecurringScheduleId(types.Institution.Bunq, monetaryAccount, tx.amount.value, tx.to.iban, tx.description)),
      balanceAfterMutation = Some(tx.balanceAfterMutation),
      timestamp = tx.timestamp.toInstant
    )
  }
}

object RecurringScheduleMapper {
  def mapToRecurringScheduleDocument(accountId: types.MonetaryAccountId, schedule: BunqRecurringSchedule): RecurringScheduleDocument =
    RecurringScheduleDocument(
      id = schedule.id,
      startDate = schedule.startDate.toInstant,
      endDate = schedule.endDate.map(_.toInstant),
      frequency = schedule.frequency,
      amount = schedule.amount.value,
      currency = schedule.amount.currency,
      monetaryAccountId = accountId,
      payee = CounterpartyMapper.mapCounterpartyInRecurring(schedule.payee),
      payer = CounterpartyMapper.mapCounterpartyInRecurring(schedule.payer),
      institution = types.Institution.Bunq,
      description = schedule.description
    )
}

object RecurringDirectDebitTransactionMapper {
  def mapToRecurringDirectDebitTransactionDocument(
      userId: types.UserId,
      monetaryAccount: types.MonetaryAccountId,
      directDebit: BunqRecurringDirectDebit
  ): RecurringDirectDebitTransactionDocument = {
    val amount = directDebit.amount.value.abs * -1
    val id = IdUtil.calculateTransactionId(
      payerIban = directDebit.from.iban,
      payeeIban = directDebit.to.iban,
      amount = amount,
      description = directDebit.description,
      transactionDate = directDebit.responded.toInstant
    )

    val recurringTransactionId = IdUtil.calculateRecurringTransactionId(
      institution = types.Institution.Bunq,
      amount = amount,
      creditSchemeId = directDebit.creditSchemeId
    )

    RecurringDirectDebitTransactionDocument(
      id = id,
      recurringTransactionId = recurringTransactionId,
      userId = userId,
      monetaryAccountId = monetaryAccount,
      currency = directDebit.amount.currency,
      amount = amount,
      payee = CounterpartyMapper.mapCounterpartyInRecurring(directDebit.to),
      payer = CounterpartyMapper.mapCounterpartyInRecurring(directDebit.from),
      description = directDebit.description,
      created = directDebit.created.toInstant,
      responded = directDebit.responded.toInstant
    )
  }
}

object MonetaryAccountMapper {
  def mapToMonetaryAccountDocument(userId: types.UserId, bunqAccount: BunqApiAccount, connectionId: UUID): MonetaryAccountDocument =
    MonetaryAccountDocument(
      id = IdUtil.calculateMonetaryAccountId(types.Institution.Bunq, bunqAccount.id),
      connectionId = connectionId,
      userId = userId,
      iban = bunqAccount.iban,
      institution = types.Institution.Bunq,
      alias = bunqAccount.aliases.filterNot(_.trim.isEmpty).headOption.map(Refined.unsafeApply),
      currency = bunqAccount.balance.currency.getCurrencyCode,
      balance = Some(bunqAccount.balance.value),
      joint = Some(bunqAccount.joint),
      fetchDateTime = Instant.now()
    )
}

private object CounterpartyMapper {
  def mapCounterparty(counterparty: BunqApiCounterparty): TransactionPartyInMessage =
    TransactionPartyInMessage(
      iban = counterparty.iban,
      name = counterparty.names.filterNot(_.trim.isEmpty).headOption.map(Refined.unsafeApply)
    )

  def mapCounterpartyInRecurring(counterparty: BunqApiCounterparty): modules.recurring.messaging.TransactionPartyInMessage =
    modules.recurring.messaging.TransactionPartyInMessage(
      iban = counterparty.iban,
      name = counterparty.names.filterNot(_.trim.isEmpty).headOption.map(Refined.unsafeApply)
    )
}

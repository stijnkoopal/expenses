package connectors.bunq

import akka.stream.scaladsl.{Flow, FlowWithContext, Keep, Sink}
import akka.stream.{Materializer, SharedKillSwitch}
import akka.{Done, NotUsed}
import connectors.bunq.api._
import javax.inject.{Inject, Singleton}
import messaging.{MessagePublisher, MessageReceiver}
import modules.recurring.messaging.{RecurringDirectDebitTransactionDocument, RecurringMessaging}
import play.api.Logging
import streaming.AkkaStreamImplicits._

import scala.concurrent.Future
import scala.language.postfixOps

@Singleton
private[bunq] class RecurringDirectDebitTransactionsRefreshReactor @Inject() (
    receiver: MessageReceiver,
    publisher: MessagePublisher,
    authenticationRepository: BunqAuthenticationRepository,
    recurringDirectDebitFetcher: BunqApiRecurringDirectDebitFetcher,
    val accountsRepository: BunqAccountsRepository,
    bunqSessionProvider: BunqSessionProvider
)(implicit val mat: Materializer)
    extends BunqAccountFlows
    with Logging {
  import BunqSerde._
  import modules.recurring.messaging.RecurringTransactionSerde._
  import serde.PlayJsonDeserialization._
  import serde.PlayJsonSerialization._

  logger.info(s"Starting to listen for 'BunqTransactionRefreshRequested' messages on '${BunqMessaging.channel}'")
  private val source = receiver.receive[BunqTransactionRefreshRequested](
    channel = BunqMessaging.channel,
    payloadType = BunqTransactionRefreshRequested.payloadType
  )

  private val refreshRecurringTransactionsFlow =
    FlowWithContext[BunqTransactionRefreshRequested, CompletedBunqAuthentication].asFlow
      .log("Refreshing recurring direct debits", {
        case (request, _) => s"BunqAccount(${request.accountId})"
      })
      .flatMapConcat {
        case (request, auth) =>
          bunqSessionProvider
            .activeApiContext(auth.apiContext)
            .flatMapConcat(context => recurringDirectDebitFetcher.fetch(context, request.bunqAccountId))
            .map(directDebit => RecurringDirectDebitTransactionMapper.mapToRecurringDirectDebitTransactionDocument(auth.userId, request.accountId, directDebit))
      }
      .to(
        publisher.publishTo[RecurringDirectDebitTransactionDocument](
          RecurringMessaging.channel,
          RecurringDirectDebitTransactionDocument.payloadType
        )
      )

  private val refreshRecurringTransactionsSink: Sink[BunqTransactionRefreshRequested, NotUsed] =
    Flow[BunqTransactionRefreshRequested]
      .flatMapConcat(request =>
        authenticationRepository
          .fetchCompleteAuthorization(request.authenticationId)
          .map(auth => (request, auth))
      )
      .alsoTo(refreshRecurringTransactionsFlow)
      .to(Sink.ignore)

  def start(killSwitch: SharedKillSwitch): Future[Done] =
    source
      .alsoTo(refreshRecurringTransactionsSink)
      .via(killSwitch.flow)
      .toMat(Sink.ignore)(Keep.right)
      .run()
}

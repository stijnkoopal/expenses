package connectors.bunq

import java.util.Currency

import com.bunq.sdk.model.generated.`object`.{Amount, LabelMonetaryAccount}
import eu.timepit.refined.api.Refined

package object api {
  case class BunqApiCounterparty(
      iban: Option[types.IbanString],
      names: Set[String]
  )

  object BunqApiCounterparty {
    def fromAlias(alias: LabelMonetaryAccount): BunqApiCounterparty = BunqApiCounterparty(
      iban = Option(alias.getIban).map(Refined.unsafeApply),
      names = Set(
        alias.getDisplayName,
        alias.getLabelUser.getDisplayName,
        alias.getLabelUser.getPublicNickName
      ).filter(x => x != null && x.nonEmpty)
    )
  }

  case class BunqApiAmount(value: BigDecimal, currency: Currency)

  object BunqApiAmount {
    def fromAmount(amount: Amount): BunqApiAmount = BunqApiAmount(
      value = BigDecimal(amount.getValue),
      currency = Currency.getInstance(amount.getCurrency)
    )
  }
}

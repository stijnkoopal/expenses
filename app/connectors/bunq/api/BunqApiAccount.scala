package connectors.bunq.api

import java.util.{Currency, UUID}

import akka.actor.ActorRef
import akka.stream.Materializer
import com.bunq.sdk.context.ApiContext
import com.bunq.sdk.http.{ApiClient, BunqResponse}
import com.bunq.sdk.model.core.BunqModel
import com.bunq.sdk.model.generated.endpoint.{MonetaryAccountBank, MonetaryAccountJoint, MonetaryAccountSavings}
import eu.timepit.refined.api.Refined
import javax.inject.{Inject, Named, Singleton}
import org.reactivestreams.Publisher
import streaming.AkkaStreamImplicits._

import scala.concurrent.ExecutionContext
import scala.jdk.CollectionConverters._
import scala.util.Try

case class BunqApiAccount(
    id: Int,
    bunqUserId: String,
    balance: BunqApiAmount,
    iban: Option[types.IbanString],
    aliases: Set[String],
    joint: Boolean
)

@Singleton
class BunqApiAccountsFetcher @Inject() (@Named("bunq-rate-limiter-accounts") rateLimiter: ActorRef)(implicit ec: ExecutionContext)
    extends BunqStreaming(rateLimiter) {
  val IBAN_TYPE = "IBAN"

  def fetch(apiContext: ApiContext)(implicit mat: Materializer): Publisher[BunqApiAccount] = {
    val bankSource = streamPaginated[BunqApiAccount, MonetaryAccountBank](
      fetch = params => ExtendedMonetaryAccountBank.list(apiContext)(params),
      mapper = items => items.map(toApiAccount)
    )

    val jointSource = streamPaginated[BunqApiAccount, MonetaryAccountJoint](
      fetch = params => ExtendedMonetaryAccountJoint.list(apiContext)(params),
      mapper = items => items.map(toApiAccount)
    )

    val savingsSource = streamPaginated[BunqApiAccount, MonetaryAccountSavings](
      fetch = params => ExtendedMonetaryAccountSavings.list(apiContext)(params),
      mapper = items => items.map(toApiAccount)
    )

    bankSource.concat(jointSource).concat(savingsSource)
  }

  private def toApiAccount(account: MonetaryAccountBank): BunqApiAccount =
    BunqApiAccount(
      id = account.getId,
      balance = BunqApiAmount(
        value = BigDecimal(account.getBalance.getValue),
        currency = Currency.getInstance(account.getBalance.getCurrency)
      ),
      iban = account.getAlias.asScala.find(_.getType == IBAN_TYPE).map(_.getValue).map(Refined.unsafeApply),
      aliases = Set(account.getDescription),
      bunqUserId = account.getUserId.toString,
      joint = false
    )

  private def toApiAccount(account: MonetaryAccountJoint): BunqApiAccount =
    BunqApiAccount(
      id = account.getId,
      balance = BunqApiAmount(
        value = BigDecimal(account.getBalance.getValue),
        currency = Currency.getInstance(account.getBalance.getCurrency)
      ),
      iban = account.getAlias.asScala.find(_.getType == IBAN_TYPE).map(_.getValue).map(Refined.unsafeApply),
      aliases = Set(account.getDescription),
      bunqUserId = account.getUserId.toString,
      joint = true
    )

  private def toApiAccount(account: MonetaryAccountSavings): BunqApiAccount =
    BunqApiAccount(
      id = account.getId,
      balance = BunqApiAmount(
        value = BigDecimal(account.getBalance.getValue),
        currency = Currency.getInstance(account.getBalance.getCurrency)
      ),
      iban = account.getAlias.asScala.find(_.getType == IBAN_TYPE).map(_.getValue).map(Refined.unsafeApply),
      aliases = Set(account.getDescription),
      bunqUserId = account.getUserId.toString,
      joint = false
    )
}

object ExtendedMonetaryAccountBank extends MonetaryAccountBank {
  def list(apiContext: ApiContext)(params: Map[String, String]): BunqResponse[java.util.List[MonetaryAccountBank]] = {
    val apiClient = new ApiClient(apiContext)
    val responseRaw =
      apiClient.get(String.format(MonetaryAccountBank.ENDPOINT_URL_LISTING, apiContext.getSessionContext.getUserId), params.asJava, null)

    BunqModel.fromJsonList(classOf[MonetaryAccountBank], responseRaw, MonetaryAccountBank.OBJECT_TYPE_GET)
  }
}

object ExtendedMonetaryAccountJoint extends MonetaryAccountJoint {
  def list(apiContext: ApiContext)(params: Map[String, String]): BunqResponse[java.util.List[MonetaryAccountJoint]] = {
    val apiClient = new ApiClient(apiContext)
    val responseRaw =
      apiClient.get(String.format(MonetaryAccountJoint.ENDPOINT_URL_LISTING, apiContext.getSessionContext.getUserId), params.asJava, null)

    BunqModel.fromJsonList(classOf[MonetaryAccountJoint], responseRaw, MonetaryAccountJoint.OBJECT_TYPE_GET)
  }
}

object ExtendedMonetaryAccountSavings extends MonetaryAccountSavings {
  def list(apiContext: ApiContext)(params: Map[String, String]): BunqResponse[java.util.List[MonetaryAccountSavings]] = {
    val apiClient = new ApiClient(apiContext)
    val responseRaw =
      apiClient.get(
        String.format(MonetaryAccountSavings.ENDPOINT_URL_LISTING, apiContext.getSessionContext.getUserId),
        params.asJava,
        null
      )

    BunqModel.fromJsonList(classOf[MonetaryAccountSavings], responseRaw, MonetaryAccountSavings.OBJECT_TYPE_GET)
  }
}

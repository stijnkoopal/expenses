package connectors.bunq.api

import akka.NotUsed
import akka.actor.ActorRef
import akka.stream.Materializer
import akka.stream.scaladsl.Source
import com.bunq.sdk.context.{ApiContext, ApiEnvironmentType, BunqContext}
import com.bunq.sdk.exception.TooManyRequestsException
import connectors.RateLimiterFlow
import javax.inject.{Inject, Named, Singleton}
import org.reactivestreams.Publisher
import streaming.AkkaStreamImplicits._
import streaming.AkkaStreamsUtil

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.jdk.CollectionConverters._
import scala.language.postfixOps
import scala.util.Try

@Singleton
class BunqSessionProvider @Inject() (@Named("bunq-rate-limiter-session-activate") rateLimiter: ActorRef)(implicit ec: ExecutionContext) {
  private val rateLimiterFlow = RateLimiterFlow.limitGlobal[NotUsed](rateLimiter, 100 seconds)

  def activeApiContext(apiContext: String)(implicit mat: Materializer): Publisher[ApiContext] =
    Source
      .single(NotUsed)
      .via(rateLimiterFlow)
      .flatMapConcat(_ => initializeFromJsonAndActivate(apiContext))
      .recoverWithRetries(
        3, {
          case e: TooManyRequestsException =>
            Source
              .single(NotUsed)
              .delay(5 seconds)
              .via(rateLimiterFlow)
              .flatMapConcat(_ => initializeFromJsonAndActivate(apiContext))
        }
      )

  private def initializeFromJsonAndActivate(json: String)(implicit mat: Materializer): Publisher[ApiContext] =
    AkkaStreamsUtil.publisherSingleFromLazyFunc {
      Try {
        val context = ApiContext.fromJson(json)
        context.ensureSessionActive()
        context
      }
    }
}

object BunqApiContext {
  type API_KEY = String

  def initializeFromJson(json: String)(implicit mat: Materializer): Publisher[ApiContext] =
    AkkaStreamsUtil.publisherSingleFromLazyFunc {
      Try {
        ApiContext.fromJson(json)
      }
    }

  def initializeFromApiKeyAndGetJson(apiKey: API_KEY)(implicit mat: Materializer): Publisher[String] =
    AkkaStreamsUtil.publisherSingleFromLazyFunc {
      Try {
        val apiContext = ApiContext.create(ApiEnvironmentType.PRODUCTION, apiKey, "Expenses", List("*").asJava) // TODO: remove *
        BunqContext.loadApiContext(apiContext)
        apiContext.toJson
      }
    }
}

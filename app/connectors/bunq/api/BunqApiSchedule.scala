package connectors.bunq.api

import java.time.format.DateTimeFormatter
import java.time.{Period, ZoneId, ZonedDateTime}

import akka.actor.ActorRef
import akka.stream.Materializer
import com.bunq.sdk.context.ApiContext
import com.bunq.sdk.http.{ApiClient, BunqResponse}
import com.bunq.sdk.model.core.BunqModel
import com.bunq.sdk.model.generated.`object`.SchedulePaymentEntry
import com.bunq.sdk.model.generated.endpoint.{Schedule, SchedulePayment}
import javax.inject.{Inject, Named, Singleton}
import modules.core.domain.IdUtil
import org.reactivestreams.Publisher
import streaming.AkkaStreamImplicits._

import scala.concurrent.ExecutionContext
import scala.jdk.CollectionConverters._
import scala.util.Try

case class BunqRecurringSchedule(
    id: types.RecurringScheduleId,
    startDate: ZonedDateTime,
    endDate: Option[ZonedDateTime],
    frequency: Period,
    payee: BunqApiCounterparty,
    payer: BunqApiCounterparty,
    amount: BunqApiAmount,
    description: String
)

@Singleton
class BunqApiScheduleFetcher @Inject()(@Named("bunq-rate-limiter-scheduled-payments") rateLimiterr: ActorRef)(
    implicit ec: ExecutionContext
) extends BunqStreaming(rateLimiterr) {
  val dateTimeFormat: DateTimeFormatter = DateTimeFormatter
    .ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS")
    .withZone(ZoneId.of("UTC"))

  def fetch(apiContext: ApiContext, bunqAccountId: Int)(implicit mat: Materializer): Publisher[BunqRecurringSchedule] = {
    streamPaginated[Option[BunqRecurringSchedule], SchedulePayment](
      fetch = params => ExtendedSchedule.list(apiContext)(bunqAccountId, params),
      mapper = schedules =>
        schedules.map(schedule => toRecurringSchedule(schedule, IdUtil.calculateMonetaryAccountId(types.Institution.Bunq, bunqAccountId)))
    ).collect {
      case Some(recurringSchedule) => recurringSchedule
    }
  }

  private def toRecurringSchedule(wrapper: SchedulePayment, monetaryAccountId: types.MonetaryAccountId): Option[BunqRecurringSchedule] = {
    val schedule: Schedule            = wrapper.getSchedule
    val payment: SchedulePaymentEntry = wrapper.getPayment

    val frequency = schedule.getRecurrenceUnit match {
      case "ONCE"    => None
      case "HOURLY"  => None
      case "DAILY"   => Some(Period.ofDays(schedule.getRecurrenceSize))
      case "WEEKLY"  => Some(Period.ofWeeks(schedule.getRecurrenceSize))
      case "MONTHLY" => Some(Period.ofMonths(schedule.getRecurrenceSize))
      case "YEARLY"  => Some(Period.ofYears(schedule.getRecurrenceSize))
    }

    frequency.map(frequency => {
      val amount = BunqApiAmount.fromAmount(payment.getAmount)
      val payee  = BunqApiCounterparty.fromAlias(payment.getCounterpartyAlias)
      val id =
        IdUtil.calculateRecurringScheduleId(types.Institution.Bunq, monetaryAccountId, amount.value, payee.iban, payment.getDescription)

      BunqRecurringSchedule(
        id = id,
        startDate = ZonedDateTime.parse(schedule.getTimeStart, dateTimeFormat),
        endDate = Option(schedule.getTimeEnd).map(e => ZonedDateTime.parse(e, dateTimeFormat)),
        frequency = frequency,
        payee = payee,
        payer = BunqApiCounterparty.fromAlias(payment.getAlias),
        amount = amount,
        description = payment.getDescription
      )
    })
  }
}

object ExtendedSchedule extends SchedulePayment {
  def list(apiContext: ApiContext)(bunqAccountId: Int, params: Map[String, String]): BunqResponse[java.util.List[SchedulePayment]] = {
    val apiClient = new ApiClient(apiContext)

    val responseRaw =
      apiClient.get(
        String.format(SchedulePayment.ENDPOINT_URL_LISTING, apiContext.getSessionContext.getUserId, bunqAccountId),
        params.asJava,
        null
      )

    BunqModel.fromJsonList(classOf[SchedulePayment], responseRaw, SchedulePayment.OBJECT_TYPE_GET)
  }
}

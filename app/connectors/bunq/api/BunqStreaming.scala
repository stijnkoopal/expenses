package connectors.bunq.api

import akka.NotUsed
import akka.actor.ActorRef
import akka.stream.Materializer
import akka.stream.scaladsl.{Sink, Source}
import com.bunq.sdk.exception.TooManyRequestsException
import com.bunq.sdk.http.{BunqResponse, Pagination}
import connectors.RateLimiterFlow
import org.reactivestreams.Publisher

import scala.concurrent.ExecutionContext
import scala.concurrent.Future.successful
import scala.concurrent.duration._
import scala.jdk.CollectionConverters._
import scala.language.postfixOps

abstract class BunqStreaming(rateLimiter: ActorRef, maxItemsPerRequest: Int = 50)(implicit ec: ExecutionContext) {
  private val rateLimiterFlow = RateLimiterFlow.limitGlobal[NotUsed](rateLimiter, 100 seconds)

  def streamPaginated[T, BunqType](
      fetch: Map[String, String] => BunqResponse[java.util.List[BunqType]],
      mapper: Seq[BunqType] => Seq[T]
  )(implicit mat: Materializer): Publisher[T] = {
    val initialPagination = new Pagination()
    initialPagination.setCount(maxItemsPerRequest)

    Source
      .unfoldAsync(Option(initialPagination.getUrlParamsCountOnly.asScala.toMap)) {
        case None => successful(None)
        case Some(pagination) =>
          Source
            .single(NotUsed)
            .via(rateLimiterFlow)
            .map(_ => fetch(pagination))
            .map(response => {
              val items = mapper(response.getValue.asScala.toSeq)

              if (items.isEmpty) None
              else if (Option(response.getPagination).exists(_.hasPreviousPage) && items.size == maxItemsPerRequest) {
                Some((Some(response.getPagination.getUrlParamsPreviousPage.asScala.toMap), items))
              } else {
                Some((None, items))
              }
            })
            .recover {
              case e: TooManyRequestsException =>
                Some(Some(pagination), Seq())
            }
            .runWith(Sink.head)
      }
      .mapConcat(items => items)
      .runWith(Sink.asPublisher(false))
  }
}

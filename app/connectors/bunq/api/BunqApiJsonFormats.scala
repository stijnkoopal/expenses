package connectors.bunq.api

import java.util.Currency

import connectors.bunq.{BunqAccount, OAuthTokenResponse}
import play.api.libs.json._

case class BunqSanboxUserResponse(Response: Array[BunqApiKeyWrapper])
case class BunqApiKeyWrapper(ApiKey: BunqApiKey)
case class BunqApiKey(api_key: String)

object BunqPlayJsonFormats {
  import de.cbley.refined.play.json._

  private val currencyWrites = new Writes[Currency] {
    def writes(currency: Currency): JsValue = JsString(currency.getCurrencyCode)
  }

  private val currencyReads = new Reads[Currency] {
    override def reads(json: JsValue): JsResult[Currency] = json match {
      case JsString(x) => JsSuccess(Currency.getInstance(x))
      case _           => JsError("String expected")
    }
  }

  implicit val currencyFormat: Format[Currency]                             = Format(currencyReads, currencyWrites)
  implicit val bunqAmountFormat: Format[BunqApiAmount]                      = Json.format
  implicit val bunqAccountFormat: Format[BunqAccount]                       = Json.format
  implicit val bunqErrorFormat: Format[BunqApiError]                        = Json.format
  implicit val bunqApiKeyFormat: Format[BunqApiKey]                         = Json.format
  implicit val bunqApiKeyWrapperFormat: Format[BunqApiKeyWrapper]           = Json.format
  implicit val bunqSanboxUserResponseFormat: Format[BunqSanboxUserResponse] = Json.format
  implicit val bunqOAuthTokenResponse: Format[OAuthTokenResponse]           = Json.format
}

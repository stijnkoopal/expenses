package connectors.bunq.api

import akka.stream.Materializer
import com.softwaremill.sttp.playJson.asJson
import com.softwaremill.sttp.{Id, SttpBackend, UriContext, sttp}
import connectors.bunq.OAuthTokenResponse
import org.reactivestreams.Publisher
import streaming.AkkaStreamsUtil

import scala.util.{Failure, Success}

case class BunqApiError(error: String, error_description: String)

class BunqAuthorizationCodeForAccessTokenExchanger(
    private val redirectUrl: String,
    private val clientId: String,
    private val clientSecret: String
) {
  import BunqPlayJsonFormats._

  def exchange(
      authorizationCode: String
  )(implicit httpBackend: SttpBackend[Id, Nothing], mat: Materializer): Publisher[String] = {
    AkkaStreamsUtil.publisherSingleFromLazyFunc {
      val response = sttp
        .post(
          uri"https://api.oauth.bunq.com/v1/token?grant_type=authorization_code&code=$authorizationCode&redirect_uri=$redirectUrl&client_id=$clientId&client_secret=$clientSecret"
        )
        .response(asJson[OAuthTokenResponse])
        .send()

      response.body match {
        case Left(error) =>
          Failure(new RuntimeException(error))

        case Right(body) =>
          body match {
            case Right(tokenResponse) => Success(tokenResponse.access_token)
            case Left(error)          => Failure(new RuntimeException(error.message))
          }
      }
    }
  }
}

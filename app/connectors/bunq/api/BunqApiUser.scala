package connectors.bunq.api

import akka.stream.Materializer
import com.bunq.sdk.model.generated.endpoint.User
import org.reactivestreams.Publisher
import streaming.AkkaStreamsUtil

import scala.util.Try

case class BunqApiUser(bunqUserId: Int)

class BunqApiUserFetcher {
  def fetch()(implicit mat: Materializer): Publisher[BunqApiUser] = {
    AkkaStreamsUtil.publisherSingleFromLazyFunc {
      Try {
        BunqApiUser(User.get().getValue.getUserApiKey.getId)
      }
    }
  }
}

object BunqApiUserFetcher {}

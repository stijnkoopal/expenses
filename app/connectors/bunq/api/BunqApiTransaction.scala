package connectors.bunq.api

import java.time.format.DateTimeFormatter
import java.time.{Instant, ZoneId, ZonedDateTime}

import akka.actor.ActorRef
import akka.stream.Materializer
import com.bunq.sdk.context.ApiContext
import com.bunq.sdk.http.{ApiClient, BunqResponse}
import com.bunq.sdk.model.core.BunqModel
import com.bunq.sdk.model.generated.endpoint.Payment
import javax.inject.{Inject, Named, Singleton}
import org.reactivestreams.Publisher

import scala.concurrent.ExecutionContext
import scala.jdk.CollectionConverters._
import scala.util.Try

case class BunqTransaction(
    id: Int,
    amount: BunqApiAmount,
    from: BunqApiCounterparty,
    to: BunqApiCounterparty,
    description: String,
    direction: types.TransactionDirection,
    scheduleId: Option[Int],
    balanceAfterMutation: BigDecimal,
    timestamp: ZonedDateTime
) extends Ordered[BunqTransaction] {
  override def compare(that: BunqTransaction): Int =
    if (this.timestamp.isBefore(that.timestamp)) -1
    else if (this.timestamp.isAfter(that.timestamp)) 1
    else 0
}

@Singleton
class BunqApiTransactionsFetcher @Inject() (
    @Named("bunq-rate-limiter-payments") rateLimiterActor: ActorRef
)(implicit ex: ExecutionContext)
    extends BunqStreaming(rateLimiterActor) {
  private val dateTimeFormat: DateTimeFormatter = DateTimeFormatter
    .ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS")
    .withZone(ZoneId.of("UTC"))

  def fetch(apiContext: ApiContext, bunqAccountId: Int, newerThan: Instant)(implicit mat: Materializer): Publisher[BunqTransaction] =
    streamPaginated[BunqTransaction, Payment](
      fetch = params => ExtendedPayment.list(apiContext)(bunqAccountId, params),
      mapper = payments => payments.map(paymentToTransaction).filter(t => t.timestamp.isAfter(newerThan.atZone(ZoneId.of("UTC"))))
    )

  private def paymentToTransaction(payment: Payment): BunqTransaction = {
    val amount    = BunqApiAmount.fromAmount(payment.getAmount)
    val direction = if (amount.value < 0) types.TransactionDirection.Outbound else types.TransactionDirection.Inbound

    val alias            = BunqApiCounterparty.fromAlias(payment.getAlias)
    val counterparyAlias = BunqApiCounterparty.fromAlias(payment.getCounterpartyAlias)

    BunqTransaction(
      id = payment.getId,
      amount = amount,
      from = if (direction == types.TransactionDirection.Inbound) counterparyAlias else alias,
      to = if (direction == types.TransactionDirection.Outbound) counterparyAlias else alias,
      direction = direction,
      description = payment.getDescription,
      scheduleId = Option(payment.getScheduledId).flatMap(value => if (value == 0) None else Some(value)),
      balanceAfterMutation = BigDecimal(payment.getBalanceAfterMutation.getValue),
      timestamp = ZonedDateTime.parse(payment.getCreated, dateTimeFormat)
    )
  }
}

object ExtendedPayment extends Payment {
  def list(apiContext: ApiContext)(bunqAccountId: Int, params: Map[String, String]): BunqResponse[java.util.List[Payment]] = {
    val apiClient = new ApiClient(apiContext)
    val responseRaw =
      apiClient.get(
        String.format(Payment.ENDPOINT_URL_LISTING, apiContext.getSessionContext.getUserId, bunqAccountId),
        params.asJava,
        null
      )

    BunqModel.fromJsonList(classOf[Payment], responseRaw, Payment.OBJECT_TYPE_GET)
  }
}

package connectors.bunq.api

import java.time.format.DateTimeFormatter
import java.time.{ZoneId, ZonedDateTime}

import akka.actor.ActorRef
import akka.stream.Materializer
import com.bunq.sdk.context.ApiContext
import com.bunq.sdk.http.{ApiClient, BunqResponse}
import com.bunq.sdk.model.core.BunqModel
import com.bunq.sdk.model.generated.endpoint.RequestResponse
import connectors.bunq.api.BunqApiRecurringDirectDebitFetcher.dateTimeFormat
import javax.inject.{Inject, Named, Singleton}
import org.reactivestreams.Publisher
import streaming.AkkaStreamImplicits._

import scala.concurrent.ExecutionContext
import scala.jdk.CollectionConverters._
import scala.util.Try

case class BunqRecurringDirectDebit(
    id: Int,
    amount: BunqApiAmount,
    from: BunqApiCounterparty,
    to: BunqApiCounterparty,
    description: String,
    creditSchemeId: String,
    mandateId: String,
    created: ZonedDateTime,
    responded: ZonedDateTime
)

@Singleton
class BunqApiRecurringDirectDebitFetcher @Inject()(
    @Named("bunq-rate-limiter-request-response") rateLimiter: ActorRef
)(
    implicit ec: ExecutionContext
) extends BunqStreaming(rateLimiter) {
  def fetch(apiContext: ApiContext, bunqAccountId: Int)(implicit mat: Materializer): Publisher[BunqRecurringDirectDebit] = {
    streamPaginated[Option[BunqRecurringDirectDebit], RequestResponse](
      fetch = params => ExtendedRequestResponse.list(apiContext)(bunqAccountId, params),
      mapper = items => items.map(toRecurringDirectDebit)
    ).collect {
      case Some(recurringDirectDebit) => recurringDirectDebit
    }
  }

  def toRecurringDirectDebit(requestResponse: RequestResponse): Option[BunqRecurringDirectDebit] = requestResponse.getSubType match {
    case "RECURRING" if requestResponse.getTimeResponded != null =>
      Some(
        BunqRecurringDirectDebit(
          id = requestResponse.getId,
          amount = BunqApiAmount.fromAmount(Option(requestResponse.getAmountResponded).getOrElse(requestResponse.getAmountInquired)),
          to = BunqApiCounterparty.fromAlias(requestResponse.getCounterpartyAlias),
          from = BunqApiCounterparty.fromAlias(requestResponse.getAlias),
          description = requestResponse.getDescription,
          creditSchemeId = requestResponse.getCreditSchemeIdentifier,
          mandateId = requestResponse.getMandateIdentifier,
          created = ZonedDateTime.parse(requestResponse.getCreated, dateTimeFormat),
          responded = ZonedDateTime.parse(requestResponse.getTimeResponded, dateTimeFormat)
        )
      )

    case _ => None
  }
}

object ExtendedRequestResponse extends RequestResponse {
  def list(apiContext: ApiContext)(bunqAccountId: Int, params: Map[String, String]): BunqResponse[java.util.List[RequestResponse]] = {
    val apiClient = new ApiClient(apiContext)

    val responseRaw =
      apiClient.get(
        String.format(RequestResponse.ENDPOINT_URL_LISTING, apiContext.getSessionContext.getUserId, bunqAccountId),
        params.asJava,
        null
      )

    BunqModel.fromJsonList(classOf[RequestResponse], responseRaw, RequestResponse.OBJECT_TYPE_GET)
  }
}

object BunqApiRecurringDirectDebitFetcher {
  val dateTimeFormat: DateTimeFormatter = DateTimeFormatter
    .ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS")
    .withZone(ZoneId.of("UTC"))
}

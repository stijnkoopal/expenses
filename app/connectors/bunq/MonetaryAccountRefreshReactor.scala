package connectors.bunq

import akka.stream.scaladsl.{Flow, Keep, Sink}
import akka.stream.{Materializer, SharedKillSwitch}
import akka.{Done, NotUsed}
import connectors.bunq.api._
import javax.inject.{Inject, Singleton}
import messaging.{MessagePublisher, MessageReceiver}
import modules.core.messaging.{AllMonetaryAccountsRefreshRequested, MonetaryAccountMessaging}
import play.api.Logging
import streaming.AkkaStreamImplicits._

import scala.concurrent.Future
import scala.language.postfixOps

@Singleton
private[bunq] class MonetaryAccountRefreshReactor @Inject() (
    receiver: MessageReceiver,
    val publisher: MessagePublisher,
    val authenticationRepository: BunqAuthenticationRepository,
    val accountsFetcher: BunqApiAccountsFetcher,
    val transactionFetcher: BunqApiTransactionsFetcher,
    val bunqSessionProvider: BunqSessionProvider
)(implicit val mat: Materializer)
    extends BunqApiAccountFlows
    with BunqAuthenticationFlows
    with BunqApiAuthFlows
    with BunqAccountBuilderFlows
    with BunqRefreshFlows
    with Logging {
  import modules.core.messaging.MonetaryAccountSerde._
  import serde.PlayJsonDeserialization._

  logger.info(s"Starting to listen for 'AllMonetaryAccountsRefreshRequested' messages on '${MonetaryAccountMessaging.channel}'")
  private val source = receiver.receive[AllMonetaryAccountsRefreshRequested](
    channel = MonetaryAccountMessaging.channel,
    payloadType = AllMonetaryAccountsRefreshRequested.payloadType
  )

  private val refreshForAuthorizationsFlow =
    Flow[CompletedBunqAuthentication]
      .via(pruneObsoleteAuthenticationsFlow)
      .via(refreshFlow)

  private val refreshAllMonetaryAccountsSink: Sink[AllMonetaryAccountsRefreshRequested, NotUsed] = Flow[AllMonetaryAccountsRefreshRequested]
    .map(_.userId)
    .log("Starting refresh", userId => s"User $userId")
    .via(fetchCompleteAuthorizationsFlow)
    .via(refreshForAuthorizationsFlow)
    .to(Sink.ignore)

  def start(killSwitch: SharedKillSwitch): Future[Done] =
    source
      .wireTap(refreshAllMonetaryAccountsSink)
      .via(killSwitch.flow)
      .toMat(Sink.ignore)(Keep.right)
      .run()
}

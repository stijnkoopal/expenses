package connectors
package bunq

import com.google.inject.AbstractModule
import connectors.bunq.api.BunqSessionProvider
import play.api.libs.concurrent.AkkaGuiceSupport

import scala.concurrent.duration._
import scala.language.postfixOps

class BunqModule extends AbstractModule with AkkaGuiceSupport {
  override def configure(): Unit = {
    bind(classOf[BunqAuthenticationRepository]).to(classOf[PostgresqlBunqAuthenticationProjection])
    bind(classOf[BunqAccountsRepository]).to(classOf[PostgresBunqAccountsProjection])
    bind(classOf[TransactionsRefreshReactor])
    bind(classOf[MonetaryAccountRefreshReactor])
    bind(classOf[RefreshOnConnectReactor])
    bind(classOf[StreamsStarter]).asEagerSingleton()
    bind(classOf[BunqSessionProvider])

    // TODO: try using 3, 6 -> results in error now. Why?
    bindActor[RateLimiterActor]("bunq-rate-limiter-session-activate", _ => RateLimiterActor.props(2, 12 seconds))
    bindActor[RateLimiterActor]("bunq-rate-limiter-payments", _ => RateLimiterActor.props(2, 12 seconds))
    bindActor[RateLimiterActor]("bunq-rate-limiter-accounts", _ => RateLimiterActor.props(2, 12 seconds))
    bindActor[RateLimiterActor]("bunq-rate-limiter-scheduled-payments", _ => RateLimiterActor.props(2, 12 seconds))
    bindActor[RateLimiterActor]("bunq-rate-limiter-request-response", _ => RateLimiterActor.props(2, 12 seconds))
  }
}

package connectors
package bunq

import java.util.UUID
import java.util.UUID.randomUUID

import akka.stream.Materializer
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}
import app.auth.AuthEnforcedAction
import com.softwaremill.sttp.{HttpURLConnectionBackend, Id, SttpBackend}
import connectors.bunq.api.{BunqApiContext, BunqApiUserFetcher, BunqAuthorizationCodeForAccessTokenExchanger}
import http.ErrorResponse
import javax.inject._
import messaging.MessagePublisher
import org.reactivestreams.Publisher
import play.api.Configuration
import play.api.libs.json._
import play.api.mvc._
import streaming.AkkaStreamImplicits._
import eu.timepit.refined.auto._
import modules.core.messaging.{ConnectionEstablished, ConnectionMessaging}

import scala.concurrent.Future

object ResponsePlayJsonFormats {
  implicit val oauthTokenResponseFormat: Format[OAuthTokenResponse] = Json.format
}

case class OAuthTokenResponse(
    access_token: String,
    token_type: String,
    state: String
)

@Singleton
class OAuthController @Inject()(
    cc: ControllerComponents,
    messagePublisher: MessagePublisher,
    readRepository: BunqAuthenticationRepository,
    config: Configuration,
    secureAction: AuthEnforcedAction,
    bunqUserFetcher: BunqApiUserFetcher
)(implicit val mat: Materializer)
    extends AbstractController(cc) {
  import BunqSerde._
  import http.HttpSerde._
  import modules.core.messaging.ConnectionSerde._
  import serde.PlayJsonSerialization._

  private val clientId     = config.get[String]("bunq.clientId")
  private val clientSecret = config.get[String]("bunq.secret")
  private val redirectUrl  = config.get[String]("bunq.redirectUrl")

  implicit val httpBackend: SttpBackend[Id, Nothing] = HttpURLConnectionBackend()

  def start(): Action[AnyContent] = secureAction { request =>
    val uniqueId = randomUUID()
    Source
      .single(BunqConnectStarted(request.userId, uniqueId))
      .runWith(
        Sink
          .fromSubscriber(messagePublisher.publishTo[BunqAuthenticationEvent](BunqMessaging.channel, BunqAuthenticationEvent.payloadType))
      )

    NoContent
      .withHeaders(
        "Location" -> s"https://oauth.bunq.com/auth?response_type=code&client_id=$clientId&redirect_uri=$redirectUrl&state=$uniqueId"
      )
      .withHeaders("Access-Control-Expose-Headers" -> "Location")
  }

  def authorize(code: Option[String], error: Option[String], state: UUID): Action[AnyContent] = Action.async { _: Request[AnyContent] =>
    val codeOrError = error match {
      case Some(e) => Right(e)
      case None =>
        code match {
          case Some(c) => Left(c)
          case None    => Right("No `code` or `error` provided")
        }
    }

    codeOrError match {
      case Left(c) =>
        readRepository
          .fetchAuthorization(state)
          .collect {
            case IncompleteBunqAuthentication(_, userId) => userId
          }
          .log(s"Found Bunq authorization for $state", userId => s"UserId = $userId")
          .flatMapConcat(_ => exchangeAuthorizationCodeForAccessToken(c))
          .log("Got Bunq access token", _ => "")
          .flatMapConcat(token => BunqApiContext.initializeFromApiKeyAndGetJson(token))
          .flatMapConcat(context => bunqUserFetcher.fetch().map(user => (context, user)))
          .log("Got Bunq user for token", { case (_, user) => user })
          .map { case (jsonApiContext, user) => BunqApiContextLoaded(state, jsonApiContext, user.bunqUserId.toString) }
          .alsoTo(
            Flow[BunqApiContextLoaded]
              .to(messagePublisher.publishTo[BunqAuthenticationEvent](BunqMessaging.channel, BunqAuthenticationEvent.payloadType))
          )
          .alsoTo(
            Flow[BunqApiContextLoaded]
              .map(
                context =>
                  ConnectionEstablished(
                    id = context.uniqId,
                    institution = types.Institution.Bunq
                ))
              .to(messagePublisher.publishTo[ConnectionEstablished](ConnectionMessaging.channel, ConnectionEstablished.payloadType))
          )
          .map(_ => Ok("OK"))
          .orElse(Source.single(PreconditionFailed("No such state found")))
          .recover { case e => BadRequest(e.getMessage) }
          .toMat(Sink.head)(Keep.right)
          .run()

      case Right(e) =>
        Future.successful(ExpectationFailed(Json.toJson(ErrorResponse(e))))
    }
  }

  private def exchangeAuthorizationCodeForAccessToken(code: String): Publisher[String] =
    new BunqAuthorizationCodeForAccessTokenExchanger(
      redirectUrl = redirectUrl,
      clientId = clientId,
      clientSecret = clientSecret
    ).exchange(code)
}

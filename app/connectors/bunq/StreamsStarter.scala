package connectors.bunq

import akka.Done
import akka.actor.CoordinatedShutdown
import akka.stream.KillSwitches
import javax.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class StreamsStarter @Inject()(
    monetaryAccountRefresh: MonetaryAccountRefreshReactor,
    transactionRefresh: TransactionsRefreshReactor,
    recurringDirectDebitRefreshReactor: RecurringDirectDebitTransactionsRefreshReactor,
    recurringScheduleRefreshReactor: RecurringScheduleRefreshReactor,
    accountsProjection: PostgresBunqAccountsProjection,
    authenticationProjection: PostgresqlBunqAuthenticationProjection,
    refreshOnConnectReactor: RefreshOnConnectReactor,
    cs: CoordinatedShutdown
)(implicit ec: ExecutionContext) {
  private val killSwitch = KillSwitches.shared("Bunq")

  private val futures = List(
    monetaryAccountRefresh.start(killSwitch),
    transactionRefresh.start(killSwitch),
    recurringDirectDebitRefreshReactor.start(killSwitch),
    recurringScheduleRefreshReactor.start(killSwitch),
    accountsProjection.start(killSwitch),
    authenticationProjection.start(killSwitch),
    refreshOnConnectReactor.start(killSwitch)
  )

  cs.addTask(CoordinatedShutdown.PhaseServiceUnbind, "stop-bunq-streams") { () =>
    killSwitch.shutdown()
    Future.sequence(futures).map(_ => Done)
  }
}

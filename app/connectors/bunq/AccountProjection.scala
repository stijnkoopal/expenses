package connectors
package bunq

import java.time.{Clock, Instant}

import akka.Done
import akka.stream.scaladsl.Sink
import akka.stream.{Materializer, SharedKillSwitch}
import javax.inject.{Inject, Singleton}
import messaging.MessageReceiver
import modules.core.messaging.{MonetaryAccountMessaging, MonetaryAccountRefreshDone}
import org.reactivestreams.Publisher
import persistence.DatabaseExecutionContext
import persistence.ScalalikejdbcImplicits._
import play.api.Logging
import scalikejdbc.streams._
import scalikejdbc.{DB, SQLSyntax, WrappedResultSet}
import streaming.AkkaStreamImplicits._
import streaming.AkkaStreamsUtil

import scala.concurrent.Future

case class BunqAccountFetched(
    id: types.MonetaryAccountId,
    bunqId: Int,
    bunqUserId: String,
    iban: Option[types.IbanString],
    currency: String,
    balance: BigDecimal,
    alias: Option[String]
)

object BunqAccountFetched {
  val payloadType = "BunqAccountFetched"
}

case class BunqAccount(
    id: types.MonetaryAccountId,
    iban: Option[types.IbanString],
    bunqId: Int,
    bunqUserId: String,
    currency: String,
    alias: Option[String],
    lastTransactionSync: Option[Instant] = None,
    inserted: Instant,
    updated: Option[Instant] = None
)

case object BunqAccount {
  def fromResultSet(rs: WrappedResultSet): BunqAccount = BunqAccount(
    id = rs.get("id"),
    iban = rs.get("iban"),
    bunqId = rs.get("bunq_id"),
    bunqUserId = rs.get("bunq_user_id"),
    currency = rs.get("currency"),
    alias = rs.get("alias"),
    lastTransactionSync = rs.get("last_transactions_sync"),
    inserted = rs.get("inserted"),
    updated = rs.get("updated")
  )
}

trait BunqAccountsRepository {
  def fetchAccountByIds(ids: Set[types.MonetaryAccountId]): Publisher[BunqAccount]
}

@Singleton
private[bunq] class PostgresBunqAccountsProjection @Inject() (messageReceiver: MessageReceiver, clock: Clock)(
    implicit ec: DatabaseExecutionContext,
    mat: Materializer
) extends BunqAccountsRepository
    with Logging {
  import BunqSerde._
  import modules.core.messaging.MonetaryAccountSerde._
  import serde.PlayJsonDeserialization._

  val tableName     = "bunq_account"
  private val table = SQLSyntax.createUnsafely(tableName)

  def start(killSwitch: SharedKillSwitch): Future[Done] = {
    logger.info(s"Starting to listen for 'BunqAccountFetched' messages on '${BunqMessaging.channel}'")
    val bunqAccountFoundFuture = messageReceiver
      .receive[BunqAccountFetched](BunqMessaging.channel, BunqAccountFetched.payloadType)
      .log("Storing bunq account", _.id)
      .runWith(Sink.foreach(handle))

    logger.info(s"Starting to listen for 'MonetaryAccountRefreshDone' messages on '${MonetaryAccountMessaging.channel}'")
    val refreshDoneFuture = messageReceiver
      .receive[MonetaryAccountRefreshDone](MonetaryAccountMessaging.channel, MonetaryAccountRefreshDone.payloadType)
      .log("Storing last transaction sync")
      .runWith(Sink.foreach(handle))

    Future.sequence(List(bunqAccountFoundFuture, refreshDoneFuture)).map(_ => Done)
  }

  override def fetchAccountByIds(ids: Set[types.MonetaryAccountId]): Publisher[BunqAccount] =
    if (ids.isEmpty) {
      AkkaStreamsUtil.emptyPublisher()
    } else {
      DB.readOnlyStream {
        sqlr"select * from $table where id IN ($ids)"
          .map(BunqAccount.fromResultSet)
          .iterator()
      }
    }

  private def store(b: BunqAccount) = DB.localTx { implicit session =>
    sqlr"""
      INSERT INTO $table(id, iban, bunq_id, bunq_user_id, currency, last_transactions_sync, alias, inserted)
      VALUES(${b.id}, ${b.iban}, ${b.bunqId}, ${b.bunqUserId}, ${b.currency}, ${b.lastTransactionSync}, ${b.alias}, ${b.inserted})
      ON CONFLICT (id)
      DO UPDATE SET
        currency = ${b.currency},
        alias = ${b.alias},
        last_transactions_sync = COALESCE(${b.lastTransactionSync}, $table.last_transactions_sync),
        updated = now()""".update
      .apply()
  }

  private def handle(event: BunqAccountFetched): Unit = {
    store(
      BunqAccount(
        id = event.id,
        iban = event.iban,
        bunqId = event.bunqId,
        bunqUserId = event.bunqUserId,
        currency = event.currency,
        alias = event.alias,
        inserted = Instant.now(clock)
      )
    )
  }

  private def handle(event: MonetaryAccountRefreshDone): Unit = DB.localTx { implicit session =>
    sqlr"UPDATE $table SET last_transactions_sync = ${event.lastTransactionSync}, updated = ${Instant
      .now(clock)} WHERE id = ${event.id}"
      .update()
      .apply()
  }
}

package connectors
package bunq

import java.util.UUID

import akka.Done
import akka.stream.scaladsl.{Sink, Source}
import akka.stream.{Materializer, SharedKillSwitch}
import javax.inject.{Inject, Singleton}
import messaging.MessageReceiver
import org.reactivestreams.Publisher
import persistence.DatabaseExecutionContext
import persistence.ScalalikejdbcImplicits._
import play.api.Logging
import scalikejdbc.interpolation.Implicits._
import scalikejdbc.streams._
import scalikejdbc.{DB, SQLSyntax, WrappedResultSet}
import streaming.AkkaStreamImplicits._
import eu.timepit.refined.auto._

import scala.concurrent.Future

sealed trait BunqAuthentication
case class CompletedBunqAuthentication(
    id: UUID,
    userId: types.UserId,
    apiContext: String,
    bunqUserId: String
) extends BunqAuthentication

case class IncompleteBunqAuthentication(
    id: UUID,
    userId: types.UserId
) extends BunqAuthentication

object BunqAuthentication {
  def fromResultSet(rs: WrappedResultSet): BunqAuthentication = rs.stringOpt("api_context") match {
    case Some(apiContext) =>
      CompletedBunqAuthentication(
        id = rs.get("id"),
        userId = rs.get("user_id"),
        apiContext = rs.get("api_context"),
        bunqUserId = rs.get("bunq_id")
      )

    case None =>
      IncompleteBunqAuthentication(
        id = rs.get("id"),
        userId = rs.get("user_id")
      )
  }
}

trait BunqAuthenticationRepository {
  def fetchAuthorization(id: UUID): Publisher[BunqAuthentication]
  def fetchCompleteAuthorization(id: UUID): Publisher[CompletedBunqAuthentication]
  def fetchCompleteAuthorizations(userId: types.UserId): Publisher[CompletedBunqAuthentication]
}

@Singleton
private[bunq] class PostgresqlBunqAuthenticationProjection @Inject() (messageReceiver: MessageReceiver)(
    implicit ec: DatabaseExecutionContext,
    mat: Materializer
) extends BunqAuthenticationRepository
    with Logging {
  import BunqSerde._
  import serde.PlayJsonDeserialization._

  val tableName     = "bunq_authentication"
  private val table = SQLSyntax.createUnsafely(tableName)

  def start(killSwitch: SharedKillSwitch): Future[Done] = {
    logger.info(s"Starting to listen for 'BunqAuthenticationEvent' messages on '${BunqMessaging.channel}'")
    Source
      .fromPublisher(messageReceiver.receive[BunqAuthenticationEvent](BunqMessaging.channel, BunqAuthenticationEvent.payloadType))
      .log(
        "Storing Bunq authentication", {
          case BunqApiContextLoaded(uniqId, _, _) => s"BunqApiContextLoaded(uniqId = $uniqId)"
          case BunqConnectStarted(userId, uniqId) => s"BunqConnectStarted(uniqId = $uniqId, userId = $userId)"
          case BunqConnectionAccepted(uniqId, _)  => s"BunqConnectionAccepted(uniqId = $uniqId)"
          case BunqAccessTokenObtained(uniqId, _) => s"BunqAccessTokenObtained(uniqId = $uniqId)"
        }
      )
      .via(killSwitch.flow)
      .runWith(Sink.foreach(handle))

    // TODO: should listen for BunqConnectionDropped
  }

  override def fetchAuthorization(id: UUID): Publisher[BunqAuthentication] = DB.readOnlyStream {
    sqlr"select * from $table where id = ${id}"
      .map(BunqAuthentication.fromResultSet)
      .iterator
  }

  override def fetchCompleteAuthorization(id: UUID): Publisher[CompletedBunqAuthentication] =
    DB.readOnlyStream {
        sqlr"select * from $table where id = ${id} AND api_context IS NOT NULL"
          .map(BunqAuthentication.fromResultSet)
          .iterator
      }
      .collect {
        case a @ CompletedBunqAuthentication(_, _, _, _) => a
      }

  override def fetchCompleteAuthorizations(userId: types.UserId): Publisher[CompletedBunqAuthentication] =
    DB.readOnlyStream {
        sqlr"select * from $table where user_id = ${userId} AND api_context IS NOT NULL"
          .map(BunqAuthentication.fromResultSet)
          .iterator
      }
      .collect {
        case a @ CompletedBunqAuthentication(_, _, _, _) => a
      }

  private def store(auth: BunqAuthentication) = DB.localTx { implicit session =>
    val sql = auth match {
      case CompletedBunqAuthentication(id, userId, apiContext, bunqUserId) =>
        sqlr"SELECT * FROM $table WHERE bunq_id = $bunqUserId".headOption().result(_ => true, session) match {
          case Some(_) =>
            sqlr"UPDATE $table SET api_context = $apiContext WHERE bunq_id = $bunqUserId"

          case None =>
            sqlr"""
              INSERT INTO $table (id, user_id, api_context, bunq_id)
              VALUES ($id, $userId, $apiContext, $bunqUserId)
              ON CONFLICT (id)
              DO UPDATE SET api_context = $apiContext, bunq_id = $bunqUserId
            """
        }

      case IncompleteBunqAuthentication(id, userId) =>
        sqlr" INSERT INTO $table (id, user_id) VALUES ($id, $userId)"
    }
    sql.update().apply()
  }

  private def handle(event: BunqAuthenticationEvent): Unit = event match {
    case BunqConnectStarted(userId, uniqId) =>
      store(
        IncompleteBunqAuthentication(
          id = uniqId,
          userId = userId
        )
      )

    case BunqApiContextLoaded(uniqId, apiContext, bunqUserId) =>
      store(
        CompletedBunqAuthentication(
          id = uniqId,
          userId = "never-upserted", // never upserted
          apiContext = apiContext,
          bunqUserId = bunqUserId
        )
      )

    case _ =>
  }
}

package connectors.bunq

import akka.stream.scaladsl.{Flow, Keep, Sink}
import akka.stream.{Materializer, SharedKillSwitch}
import akka.{Done, NotUsed}
import connectors.bunq.api._
import javax.inject.{Inject, Singleton}
import messaging.{MessagePublisher, MessageReceiver}
import modules.core.messaging.{ConnectionMessaging, MonetaryAccountsOnConnectionRefreshRequested}
import play.api.Logging
import streaming.AkkaStreamImplicits._

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps

@Singleton
private[bunq] class RefreshOnConnectReactor @Inject() (
    receiver: MessageReceiver,
    val publisher: MessagePublisher,
    val authenticationRepository: BunqAuthenticationRepository,
    val accountsFetcher: BunqApiAccountsFetcher,
    val transactionFetcher: BunqApiTransactionsFetcher,
    val bunqSessionProvider: BunqSessionProvider
)(implicit val mat: Materializer)
    extends BunqApiAccountFlows
    with BunqAuthenticationFlows
    with BunqApiAuthFlows
    with BunqAccountBuilderFlows
    with BunqRefreshFlows
    with Logging {
  import modules.core.messaging.ConnectionSerde._
  import serde.PlayJsonDeserialization._

  logger.info(s"Starting to listen for 'MonetaryAccountsOnConnectionRefreshRequested' messages on '${ConnectionMessaging.channel}'")
  private val source = receiver.receive[MonetaryAccountsOnConnectionRefreshRequested](
    channel = ConnectionMessaging.channel,
    payloadType = MonetaryAccountsOnConnectionRefreshRequested.payloadType
  )

  private val fetchBunqAuthentication: Flow[MonetaryAccountsOnConnectionRefreshRequested, CompletedBunqAuthentication, NotUsed] =
    Flow[MonetaryAccountsOnConnectionRefreshRequested]
      .delay(4 seconds)
      .map(_.connectionId)
      .via(fetchCompleteAuthorizationById)

  private val refreshMonetaryAccounts =
    Flow[MonetaryAccountsOnConnectionRefreshRequested]
      .via(fetchBunqAuthentication)
      .via(refreshFlow)
      .to(Sink.ignore)

  def start(killSwitch: SharedKillSwitch): Future[Done] =
    source
      .log("Refresh Bunq on connect")
      .wireTap(refreshMonetaryAccounts)
      .via(killSwitch.flow)
      .toMat(Sink.ignore)(Keep.right)
      .run()
}

package connectors.bunq

import java.time.Instant
import java.util.UUID

import akka.NotUsed
import akka.stream.Materializer
import akka.stream.scaladsl.{Flow, Sink, Source}
import connectors.bunq.api.{BunqApiAccount, BunqApiAccountsFetcher, BunqApiContext, BunqSessionProvider}
import messaging.MessagePublisher
import modules.core.domain.IdUtil
import modules.core.messaging.{MonetaryAccountDocument, MonetaryAccountMessaging, MonetaryAccountRefreshStarted}
import org.reactivestreams.Publisher
import streaming.AkkaStreamImplicits._

import scala.language.postfixOps
import scala.util.Try

trait BunqApiAuthFlows {
  implicit def mat: Materializer

  import BunqSerde._
  import serde.PlayJsonSerialization._

  def publisher: MessagePublisher

  private def errorHandlingFlow =
    Flow[Either[_, BunqConnectionDropped]]
      .collect { case Right(msg) => msg }
      .log("Pruning bunq auth")
      .to(publisher.publishTo[BunqConnectionDropped](BunqMessaging.channel, BunqConnectionDropped.payloadType))

  val pruneObsoleteAuthenticationsFlow: Flow[CompletedBunqAuthentication, CompletedBunqAuthentication, NotUsed] =
    Flow[CompletedBunqAuthentication]
      .log("Trying to prune bunq auth", _.id)
      .flatMapConcat { auth =>
        BunqApiContext
          .initializeFromJson(auth.apiContext)
          .map(context =>
            Try {
              context.resetSession()
              auth
            }.toEither.swap.map(_ => BunqConnectionDropped(auth.id))
          )
          .divertTo(errorHandlingFlow, _.isRight)
          .collect {
            case Left(auth) => auth
          }
      }
}

trait BunqAuthenticationFlows {
  implicit def mat: Materializer

  def authenticationRepository: BunqAuthenticationRepository

  val fetchCompleteAuthorizationsFlow: Flow[types.UserId, CompletedBunqAuthentication, NotUsed] =
    Flow[types.UserId]
      .flatMapConcat { userId =>
        authenticationRepository.fetchCompleteAuthorizations(userId)
      }

  val fetchCompleteAuthorizationById: Flow[UUID, CompletedBunqAuthentication, NotUsed] =
    Flow[UUID]
      .flatMapConcat { authenticationId =>
        authenticationRepository.fetchCompleteAuthorization(authenticationId)
      }
}

trait BunqAccountFlows {
  implicit def mat: Materializer

  def accountsRepository: BunqAccountsRepository

  def fetchLastSyncDateFlow(accountId: types.MonetaryAccountId): Publisher[Instant] =
    accountsRepository
      .fetchAccountByIds(Set(accountId))
      .map(account => account.lastTransactionSync)
      .collect {
        case Some(lastTransactionSync) => lastTransactionSync
      }
      .orElse(Source.lazySource(() => Source.single(Instant.EPOCH)))
}

trait BunqApiAccountFlows {
  implicit def mat: Materializer

  def accountsFetcher: BunqApiAccountsFetcher
  def bunqSessionProvider: BunqSessionProvider

  val fetchApiAccountsFlow: Flow[CompletedBunqAuthentication, (BunqApiAccount, CompletedBunqAuthentication), NotUsed] =
    Flow[CompletedBunqAuthentication]
      .flatMapConcat { auth =>
        bunqSessionProvider
          .activeApiContext(auth.apiContext)
          .flatMapConcat(context => accountsFetcher.fetch(context))
          .map(account => (account, auth))
      }

}

trait BunqAccountBuilderFlows {
  val buildBunqAccountFlow: Flow[BunqApiAccount, BunqAccountFetched, NotUsed] =
    Flow[BunqApiAccount].map(buildBunqAccountFound)

  private def buildBunqAccountFound(bunqAccount: BunqApiAccount) =
    BunqAccountFetched(
      id = IdUtil.calculateMonetaryAccountId(types.Institution.Bunq, bunqAccount.id),
      bunqId = bunqAccount.id,
      bunqUserId = bunqAccount.bunqUserId,
      iban = bunqAccount.iban,
      currency = bunqAccount.balance.currency.getCurrencyCode,
      balance = bunqAccount.balance.value,
      alias = bunqAccount.aliases.headOption
    )
}

trait BunqRefreshFlows { this: BunqAccountBuilderFlows with BunqApiAccountFlows =>
  implicit def mat: Materializer
  import BunqSerde._
  import modules.core.messaging.MonetaryAccountSerde._
  import serde.PlayJsonSerialization._

  def publisher: MessagePublisher

  private val publishMonetaryAccountDocument: Sink[(BunqApiAccount, types.UserId, UUID), NotUsed] =
    Flow[(BunqApiAccount, types.UserId, UUID)]
      .map { case (account, userId, connectionId) => MonetaryAccountMapper.mapToMonetaryAccountDocument(userId, account, connectionId) }
      .to(publisher.publishTo[MonetaryAccountDocument](MonetaryAccountMessaging.channel, MonetaryAccountDocument.payloadType))

  private val publishBunqAccountFetched: Sink[BunqApiAccount, NotUsed] =
    buildBunqAccountFlow
      .to(publisher.publishTo[BunqAccountFetched](BunqMessaging.channel, BunqAccountFetched.payloadType))

  private val publishMonetaryAccountRefreshStarted: Sink[BunqApiAccount, NotUsed] =
    Flow[BunqApiAccount]
      .map(apiAccount => IdUtil.calculateMonetaryAccountId(types.Institution.Bunq, apiAccount.id))
      .map(accountId => MonetaryAccountRefreshStarted(accountId))
      .to(
        publisher
          .publishTo[MonetaryAccountRefreshStarted](MonetaryAccountMessaging.channel, MonetaryAccountRefreshStarted.payloadType)
      )

  private val publishBunqTransactionRefresh: Sink[(BunqApiAccount, CompletedBunqAuthentication), NotUsed] =
    Flow[(BunqApiAccount, CompletedBunqAuthentication)]
      .map {
        case (account, auth) =>
          BunqTransactionRefreshRequested(
            accountId = IdUtil.calculateMonetaryAccountId(types.Institution.Bunq, account.id),
            bunqAccountId = account.id,
            authenticationId = auth.id
          )
      }
      .to(
        publisher
          .publishTo[BunqTransactionRefreshRequested](BunqMessaging.channel, BunqTransactionRefreshRequested.payloadType)
      )

  val refreshFlow: Flow[CompletedBunqAuthentication, (BunqApiAccount, CompletedBunqAuthentication), NotUsed] =
    Flow[CompletedBunqAuthentication]
      .log("Starting to fetch bunq accounts", auth => s"CompletedBunqAuthentication(${auth.id})")
      .via(fetchApiAccountsFlow)
      //          .via(deduplicateAccounts) // TODO
      .log(
        "Bunq account found, starting refresh",
        account =>
          s"monetaryAccountId: ${IdUtil
            .calculateMonetaryAccountId(types.Institution.Bunq, account._1.id)}, alias: ${account._1.aliases.headOption.getOrElse("")}"
      )
      .wireTap(
        Flow[(BunqApiAccount, CompletedBunqAuthentication)]
          .map { case (account, _) => account }
          .to(publishMonetaryAccountRefreshStarted)
      )
      .alsoTo(
        Flow[(BunqApiAccount, CompletedBunqAuthentication)]
          .map { case (account, auth) => (account, auth.userId, auth.id) }
          .alsoTo(publishMonetaryAccountDocument)
          .map { case (account, _, _) => account }
          .alsoTo(publishBunqAccountFetched)
          .to(Sink.ignore)
      )
      .alsoTo(publishBunqTransactionRefresh)
}

package connectors.bunq

import java.util.UUID

import eu.timepit.refined.types.string.NonEmptyString
import play.api.libs.json.{Format, Json}
import serde.{PlayJsonDefaultDeserialization, PlayJsonDefaultSerialization}

sealed trait BunqAuthenticationEvent {
  def uniqId: UUID
}
case class BunqConnectStarted(userId: types.UserId, override val uniqId: UUID)                           extends BunqAuthenticationEvent
case class BunqConnectionAccepted(override val uniqId: UUID, authorizationCode: NonEmptyString)            extends BunqAuthenticationEvent
case class BunqAccessTokenObtained(override val uniqId: UUID, accessToken: NonEmptyString)                 extends BunqAuthenticationEvent
case class BunqApiContextLoaded(override val uniqId: UUID, apiContext: String, bunqUserId: String) extends BunqAuthenticationEvent

object BunqAuthenticationEvent {
  val payloadType = "BunqAuthenticationEvent"
}

case class BunqConnectionDropped(uniqId: UUID)
object BunqConnectionDropped {
  val payloadType = "bunqConnectionDropped"
}

case class BunqTransactionRefreshRequested(
    accountId: types.MonetaryAccountId,
    bunqAccountId: Int,
    authenticationId: UUID
)
object BunqTransactionRefreshRequested {
  val payloadType = "bunqTransactionRefreshRequested"
}

object BunqMessaging {
  val channel = "bunq"
}

object BunqSerde extends PlayJsonDefaultSerialization with PlayJsonDefaultDeserialization {
  import de.cbley.refined.play.json._

  implicit val bunqAccountFoundFormat: Format[BunqAccountFetched] = Json.format

  implicit val bunqConnectStartedFormat: Format[BunqConnectStarted]           = Json.format
  implicit val bunqApiContextLoadedFormat: Format[BunqApiContextLoaded]       = Json.format
  implicit val bunqConnectionAcceptedFormat: Format[BunqConnectionAccepted]   = Json.format
  implicit val bunqAccessTokenObtainedFormat: Format[BunqAccessTokenObtained] = Json.format
  implicit val bunqAuthenticationEventFormat: Format[BunqAuthenticationEvent] = Json.format

  implicit val bunqConnectionDroppedFormat: Format[BunqConnectionDropped] = Json.format

  implicit val bunqTransactionRefreshRequestedFormat: Format[BunqTransactionRefreshRequested] = Json.format
}

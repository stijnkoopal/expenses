package connectors
package bunq

import java.time.Instant

import akka.stream.scaladsl.{Flow, FlowWithContext, Keep, Sink, Source}
import akka.stream.{Materializer, SharedKillSwitch}
import akka.{Done, NotUsed}
import connectors.bunq.api._
import javax.inject.{Inject, Singleton}
import messaging.{MessagePublisher, MessageReceiver}
import modules.core.messaging.{MonetaryAccountMessaging, MonetaryAccountRefreshDone, TransactionDocument, TransactionMessaging}
import play.api.Logging
import streaming.AkkaStreamImplicits._

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps

@Singleton
private[bunq] class TransactionsRefreshReactor @Inject()(
    receiver: MessageReceiver,
    publisher: MessagePublisher,
    authenticationRepository: BunqAuthenticationRepository,
    transactionFetcher: BunqApiTransactionsFetcher,
    val accountsRepository: BunqAccountsRepository,
    bunqSessionProvider: BunqSessionProvider
)(implicit val mat: Materializer)
    extends BunqAccountFlows
    with Logging {
  import BunqSerde._
  import modules.core.messaging.MonetaryAccountSerde._
  import modules.core.messaging.TransactionSerde._
  import serde.PlayJsonDeserialization._
  import serde.PlayJsonSerialization._

  logger.info(s"Starting to listen for 'BunqTransactionRefreshRequested' messages on '${BunqMessaging.channel}'")
  private val source = receiver.receive[BunqTransactionRefreshRequested](
    channel = BunqMessaging.channel,
    payloadType = BunqTransactionRefreshRequested.payloadType
  )

  // COMMENT
  private val refreshTransactionsFlow = FlowWithContext[BunqTransactionRefreshRequested, CompletedBunqAuthentication]
    .via(Flow[(BunqTransactionRefreshRequested, CompletedBunqAuthentication)].flatMapConcat {
      case (request, auth) =>
        fetchLastSyncDateFlow(request.accountId)
          .map(lastSyncDate => ((request, lastSyncDate), auth))
    })
    .asFlow
    .log("Refreshing transactions", { case ((request, lastSyncDate), _) => s"BunqAccount(${request.accountId}) from '$lastSyncDate'" })
    .flatMapConcat {
      case ((request, lastSyncDate), auth) =>
        bunqSessionProvider
          .activeApiContext(auth.apiContext)
          .flatMapConcat(context => transactionFetcher.fetch(context, request.bunqAccountId, lastSyncDate))
          .map(tx => TransactionMapper.mapToTransactionDocument(auth.userId, request.accountId, tx))
          .wireTap(
            Sink.onComplete(x => {
              Source
                .single(request)
                .map(request => MonetaryAccountRefreshDone(request.accountId, Instant.now()))
                .delay(3 seconds) // TODO: actually we want this emitted if all TransactionDocument messages are flushed; mock that with 3 seconds for now
                .to(
                  publisher
                    .publishTo[MonetaryAccountRefreshDone](MonetaryAccountMessaging.channel, MonetaryAccountRefreshDone.payloadType)
                )
                .run()
            })
          )
    }
    .to(publisher.publishTo[TransactionDocument](TransactionMessaging.channel, TransactionDocument.payloadType))

  private val refreshTransactionsSink: Sink[BunqTransactionRefreshRequested, NotUsed] =
    Flow[BunqTransactionRefreshRequested]
      .flatMapConcat(
        request =>
          authenticationRepository
            .fetchCompleteAuthorization(request.authenticationId)
            .map(auth => (request, auth)))
      .alsoTo(refreshTransactionsFlow)
      .to(Sink.ignore)

  def start(killSwitch: SharedKillSwitch): Future[Done] =
    source
      .alsoTo(refreshTransactionsSink)
      .via(killSwitch.flow)
      .toMat(Sink.ignore)(Keep.right)
      .run()
}

variable "project" {}
variable "cluster" {}
variable "region" {}
variable "zones" {}
variable "k8s_version" {}
variable "k8s_location" {}

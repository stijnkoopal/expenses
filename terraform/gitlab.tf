# Retrieve an access token as the Terraform runner
data "google_client_config" "provider" {}

provider "kubernetes" {
  load_config_file = false

  host = "https://${google_container_cluster.expenses.endpoint}"
  token = data.google_client_config.provider.access_token
  cluster_ca_certificate = base64decode(
  google_container_cluster.expenses.master_auth[0].cluster_ca_certificate,
  )
}

provider "helm" {
  kubernetes {
    host = "https://${google_container_cluster.expenses.endpoint}"
    token = data.google_client_config.provider.access_token
    cluster_ca_certificate = base64decode(
    google_container_cluster.expenses.master_auth[0].cluster_ca_certificate,
    )
  }
}


resource "kubernetes_namespace" "gitlab" {
  metadata {
    name = "gitlab"
  }
}

resource "kubernetes_service_account" "sa-gitlab" {
  metadata {
    name = "sa-gitlab"
    namespace = kubernetes_namespace.gitlab.metadata.0.name
  }
  secret {
    name = kubernetes_secret.sa-gitlab.metadata.0.name
  }
  depends_on = [
    google_container_cluster.expenses
  ]
}

resource "kubernetes_secret" "sa-gitlab" {
  metadata {
    name = "sa-gitlab"
    namespace = kubernetes_namespace.gitlab.metadata.0.name
  }
  depends_on = [
    google_container_cluster.expenses
  ]
}

resource "kubernetes_cluster_role_binding" "sa-gitlab-owner" {
  metadata {
    name = "sa-gitlab-admin"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind = "ClusterRole"
    name = "cluster-admin"
  }

  subject {
    kind = "ServiceAccount"
    name = kubernetes_service_account.sa-gitlab.metadata[0].name
    namespace = kubernetes_namespace.gitlab.metadata.0.name
  }

  depends_on = [
    google_container_cluster.expenses
  ]
}

output "secrets_gitlab-registry" {
  value = "run: kubectl create secret docker-registry gitlab-registry --docker-username=stijn@koopal.io --docker-password=[password] --docker-email=stijn@koopal.io --docker-server=registry.gitlab.com"
}

output "gitlab-integration" {
  value = "do not forget to do you gitlab integration. All gitlab resources are in the gitlab namespace"
}

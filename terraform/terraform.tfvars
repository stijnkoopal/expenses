project = "expenses-plompstratie-295118"
cluster = "expenses"
region = "europe-west4"
zones = [
  "europe-west4-b"
]
k8s_version = "1.17.13-gke.1400"
k8s_location = "europe-west4-b"

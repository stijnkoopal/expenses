resource "kubernetes_secret" "bunq-credentials" {
  metadata {
    name = "bunq-credentials"
    namespace = "default"
  }

  data = {
    client_id = ""
    secret = ""
  }

  depends_on = [
    google_container_cluster.expenses
  ]

  lifecycle {
    ignore_changes = [
      data
    ]
  }
}

output "secrets_bunq-credentials" {
  value = "run: kubectl edit secret bunq-credentials"
}

resource "kubernetes_secret" "play-http-secret" {
  metadata {
    name = "play-http-secret"
    namespace = "default"
  }

  data = {
    secret = ""
  }

  depends_on = [
    google_container_cluster.expenses
  ]

  lifecycle {
    ignore_changes = [
      data
    ]
  }
}

output "secrets_play-http-secret" {
  value = "run: kubectl edit secret play-http-secret"
}

resource "kubernetes_secret" "cloudsql-expenses-credentials" {
  metadata {
    name = "cloudsql-expenses-credentials"
    namespace = "default"
  }

  data = {
    username = ""
    password = ""
  }

  depends_on = [
    google_container_cluster.expenses
  ]

  lifecycle {
    ignore_changes = [
      data
    ]
  }
}

output "secrets_cloudsql-expenses-credentials" {
  value = "run: kubectl edit secret cloudsql-expenses-credentials"
}

// only with cloudsql:
//resource "kubernetes_secret" "cloudsql-instance-credentials" {
//  metadata {
//    name = "cloudsql-instance-credentials"
//    namespace = "default"
//  }
//
//  depends_on = [
//    google_container_cluster.expenses
//  ]
//
//  lifecycle {
//    ignore_changes = [
//      data
//    ]
//  }
//}
//
//output "secrets_cloudsql-instance-credentials" {
//  value = "run: kubectl edit secret cloudsql-instance-credentials"
//}

data "google_compute_network" "default" {
  project = var.project
  name = "default"
}

resource "google_container_cluster" "expenses" {
  project = var.project
  name = "expenses"
  location = var.k8s_location
  min_master_version = var.k8s_version

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count = 1

  network = data.google_compute_network.default.name

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = true
    }
  }
}

output "gcp_cluster_name" {
  value = google_container_cluster.expenses.name
}

resource "google_container_node_pool" "expenses_app_node_pool" {
  project = var.project
  name = "app-node-pool"
  location = var.k8s_location
  cluster = google_container_cluster.expenses.name
  node_count = 1
  node_locations = var.zones

  node_config {
    preemptible = false
    machine_type = "n1-standard-1"

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

resource "google_compute_address" "expenses_static_ip" {
  project = var.project
  name = "expenses-static-ip"
  region = var.region
}

output "gcp_static_ip" {
  value = "Change node of static ip (${google_compute_address.expenses_static_ip.address}) on https://console.cloud.google.com/networking/addresses/list?authuser=2&project=expenses-plompstratie"
}

resource "google_compute_firewall" "expenses-https" {
  name = "expenses-https"
  network = google_container_cluster.expenses.network
  project = var.project

  allow {
    protocol = "tcp"
    ports = [
      "443"
    ]
  }
}

output "telepresence" {
  value = "do not forget to update swap.sh"
}

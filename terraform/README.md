## Setup k8s env on GKE
```bash

gcloud auth login
gcloud projects list

export project_id="fill-in-the-right-project-id!"

gcloud config set project $project_id

gcloud services enable compute
gcloud services enable container

gcloud iam service-accounts create sa-terraform \
    --description="Terraform your env" \
    --display-name="Terraform"

mkdir -f $HOME/secrets

gcloud projects add-iam-policy-binding $project_id --member "serviceAccount:sa-terraform@$project_id.iam.gserviceaccount.com" --role "roles/owner"
gcloud projects add-iam-policy-binding $project_id --member "serviceAccount:sa-terraform@$project_id.iam.gserviceaccount.com" --role "roles/compute.admin"

gcloud iam service-accounts keys create $HOME/secrets/terraform-expenses-plompstratie.json \
  --iam-account sa-terraform@$project_id.iam.gserviceaccount.com

export GOOGLE_APPLICATION_CREDENTIALS="$HOME/secrets/terraform-expenses-plompstratie.json"

# in terraform.tfvars change the project to $project_id

cd terraform
terraform apply

gcloud container clusters get-credentials expenses --zone europe-west4-b --project expenses-plompstratie
# edit secrets now

#-------------- Build --------------#
FROM hseeberger/scala-sbt:11.0.5_1.3.6_2.13.1 AS build

RUN mkdir -p /root/project-build/project
WORKDIR /root/project-build

ADD ./project/plugins.sbt project/
ADD ./project/build.properties project/
ADD ./project/Versions.scala project/
ADD build.sbt .
RUN sbt update

ADD . /root/project-build/
RUN sbt clean stage

##-------------- Container --------------#
FROM azul/zulu-openjdk-alpine:13
COPY --from=build /root/project-build/target/universal/stage /app
WORKDIR /app
CMD ["bin/expenses"]
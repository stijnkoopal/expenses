name := "expenses"

scalaVersion := Versions.scala

ThisBuild / turbo := true

lazy val root = (project in file("."))
  .enablePlugins(PlayScala) // TODO: PlayService instead of PlayScala?

resolvers ++= Seq(
  "Jitpack" at "https://jitpack.io",
  Resolver.bintrayRepo("cbley", "maven")
)

// Git versioning
enablePlugins(GitVersioning)
git.useGitDescribe := true

// Docker
enablePlugins(AshScriptPlugin)

// ----- Libraries

// Http client
libraryDependencies += "com.softwaremill.sttp" %% "core"      % Versions.sttp
libraryDependencies += "com.softwaremill.sttp" %% "play-json" % Versions.sttp

// Required for bunq
libraryDependencies += "com.github.bunq"  % "sdk_java"  % Versions.bunqSdk
libraryDependencies += "javax.xml.bind"   % "jaxb-api"  % Versions.jaxb
libraryDependencies += "com.sun.xml.bind" % "jaxb-core" % Versions.jaxb
libraryDependencies += "com.sun.xml.bind" % "jaxb-impl" % Versions.jaxb

// Dependency injection
libraryDependencies += guice

// Database
libraryDependencies ++= Seq(evolutions, jdbc)
libraryDependencies += "org.postgresql"  % "postgresql"                      % Versions.postgresDriver
libraryDependencies += "org.scalikejdbc" %% "scalikejdbc"                    % Versions.scalalikejdbc
libraryDependencies += "org.scalikejdbc" %% "scalikejdbc-config"             % Versions.scalalikejdbc
libraryDependencies += "org.scalikejdbc" %% "scalikejdbc-streams"            % Versions.scalalikejdbc
libraryDependencies += "org.scalikejdbc" %% "scalikejdbc-play-fixture"       % Versions.scalalikejdbcPlay
libraryDependencies += "org.scalikejdbc" %% "scalikejdbc-play-dbapi-adapter" % Versions.scalalikejdbcPlay

// Graphql
libraryDependencies += "org.sangria-graphql" %% "sangria"           % Versions.sangria
libraryDependencies += "org.sangria-graphql" %% "sangria-play-json" % Versions.sangriaPlayJson

// Reactive streams
libraryDependencies += "com.typesafe.akka" %% "akka-stream" % Versions.akka

// Event sourcing
libraryDependencies += "com.typesafe.akka"   %% "akka-persistence-typed" % Versions.akka
libraryDependencies += "com.github.dnvriend" %% "akka-persistence-jdbc"  % Versions.akkaPersistenceJdbc excludeAll (ExclusionRule(organization =
  "com.typesafe.akka"
))
libraryDependencies += "com.typesafe.akka" %% "akka-persistence-query" % Versions.akka

libraryDependencies += "com.typesafe.akka" %% "akka-cluster-sharding-typed" % Versions.akka

libraryDependencies += clusterSharding
libraryDependencies += "com.typesafe.akka"            %% "akka-http"                     % Versions.akkaHttpVersion
libraryDependencies += "com.typesafe.akka"            %% "akka-http-core"                % Versions.akkaHttpVersion
libraryDependencies += "com.typesafe.akka"            %% "akka-http-spray-json"          % Versions.akkaHttpVersion
libraryDependencies += "com.lightbend.akka.discovery" %% "akka-discovery-kubernetes-api" % Versions.akkaDiscoveryK8sVersion  excludeAll (ExclusionRule(organization =
  "com.typesafe.akka"
))
libraryDependencies += "com.lightbend.akka.management" %% "akka-management-cluster-bootstrap" % Versions.akkaManagementClusterBootstrapVersion excludeAll (ExclusionRule(organization =
  "com.typesafe.akka"
))

libraryDependencies += "com.typesafe.akka" %% "akka-discovery" % Versions.akka

// Type enhancements
libraryDependencies += "eu.timepit" %% "refined"           % Versions.refined
libraryDependencies += "de.cbley"   %% "play-json-refined" % Versions.playJsonRefined

// Ibans
libraryDependencies += "org.iban4j" % "iban4j" % Versions.iban4j

// Logging
libraryDependencies += "com.typesafe.akka" %% "akka-slf4j" % Versions.akka

// JWT
libraryDependencies += "com.pauldijou" %% "jwt-play" % Versions.jwt
libraryDependencies += "com.pauldijou" %% "jwt-core" % Versions.jwt
libraryDependencies += "com.auth0"     % "jwks-rsa"  % Versions.auth0Jwks

// Test
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play"       % Versions.playTest  % Test
libraryDependencies += "org.scalactic"          %% "scalactic"                % Versions.scalaTest % Test
libraryDependencies += "org.scalatest"          %% "scalatest"                % Versions.scalaTest % Test
libraryDependencies += "org.scalamock"          %% "scalamock"                % Versions.scalaMock % Test
libraryDependencies += "com.typesafe.akka"      %% "akka-stream-testkit"      % Versions.akka      % Test
libraryDependencies += "com.typesafe.akka"      %% "akka-persistence-testkit" % Versions.akka      % Test
libraryDependencies += "org.scalacheck"         %% "scalacheck"               % Versions.scalaCheck  % "test"


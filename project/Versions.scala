object Versions {
  val scala = "2.13.1"

  val bunqSdk = "1.13.1"
  val jaxb    = "2.3.0"
  val sttp    = "1.6.0"

  val postgresDriver    = "42.2.6"
  val scalalikejdbc     = "3.3.5"
  val scalalikejdbcPlay = "2.7.1-scalikejdbc-3.3"

  val sangria         = "2.0.0-M2"
  val sangriaPlayJson = "2.0.0"

  val akka                                  = "2.6.5"
  val akkaHttpVersion                       = "10.1.12"
  val akkaDiscoveryK8sVersion               = "1.0.6"
  val akkaManagementClusterBootstrapVersion = "1.0.6"
  val akkaPersistenceJdbc                   = "3.5.3"

  val refined         = "0.9.13"
  val playJsonRefined = "0.8.0"

  val iban4j = "3.2.1"

  val jwt       = "4.2.0"
  val auth0Jwks = "0.9.0"

  val playTest   = "4.0.3"
  val scalaMock  = "4.4.0"
  val scalaTest  = "3.1.0"
  val scalaCheck = "1.14.1"
}

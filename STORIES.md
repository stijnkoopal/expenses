# Need to have
- Transactions per period (all, top 10)
- Incoming recurrent transactions
- Add `loans` type, and append link transactions to it
- schedule for one account can be inbound schedule to other
- other banks do not have balanceAfterMutation, setup way to extract that for each tx

# Refactor
- Not directly inject cs into InJvmAkkaStreamsMessaging
- a lot of mapping
- domain / adapter etc
- graphql split up
- errors in streams 


# EPIC 2
Add cash wallet + manual transactions onit



TODO:
 - 1 card with transaction, categorize it. Swipable
 - 

IDEAS
 - Sankey chart to visualize income to expenses per month
 - inkblot chart to visualize category over time 
    - http://factbased.blogspot.com/2012/01/categorizing-my-expenses.html#more
 




https://eazybi.com/blog/data_visualization_and_chart_types/






# POSSIBLE FEATURES
- Login multiple users
- Encrypted data with key per user, so easily user deletable
- Sync bunq, rabo, knab
- Manual categorization of transactions
- Automatic categorization of transactions
  - Based on store (AH, Jumbo etc -> groceries)
  - Based on timestamp (11.30 - 13.30 could be lunch if price correct)
  - Learning model based on user input
- Show which transactions are synced, but not yet seen by user
- Selection of accounts to show in graphs
- Accounts can be and/or accounts, set that per account
- Net worth
  - DeGiro
  - Binck
  - Binance
  - Studieschuld
- Readonly accounts 
- Push notifications on new transaction
- Learn to find recurring transactions
- Make a profile based on income and spendings
- Spendings per store, per category
- (Automatische incassos op 1 dag)
- NO RECLAME
- Financial efficiency:
    - Hey you pay for car insurance, what is your license plate: there might be cheaper options
    - Mortgage, what is your address. Can it be cheaper
    - Zorgverzekering
- Tips waar je je geld kan laten
 - Stel je spaart 80 pm
 - Laten zien wat deze 80 over 1, 3, 5, 10, 25, 50 jaar waard is op spaar-rekening/belegging etc
- Linken aan FIRE (4% rule)
- Splitten van transacties (80 uitgegeven bij H&M, 30 daarvan voor baby)
- Hypotheek
 - Op basis van inkomen
 - Op basis van inkomen + uitgavepatroon
 - Op basis van inkomen + uitgavepatroon + schulden

- Virtual wallet
  - Put budget on it, will decrease with incidental spendings. 
  - Weekly budget?
  - Put push notification when at end of budget
  
Categories:
 - Groceries
 - Eating in
 - Gifts
 - Vakanties
 

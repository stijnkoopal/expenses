package connectors.rabo

import play.api.libs.json.{Format, Json}
import serde.{PlayJsonDefaultDeserialization, PlayJsonDefaultSerialization}

object RaboMessaging {
  val channel = "rabo"
}

object RaboSerde extends PlayJsonDefaultDeserialization with PlayJsonDefaultSerialization {
  implicit val raboAccountFoundFormat: Format[RaboAccountFound]                           = Json.format
  implicit val raboTransactionFetchCompletedFormat: Format[RaboTransactionFetchCompleted] = Json.format

  implicit val raboConnectStartedFormat: Format[RaboConnectStarted]           = Json.format
  implicit val raboConnectionAcceptedFormat: Format[RaboConnectionAccepted]   = Json.format
  implicit val raboAccessTokenAcquiredFormat: Format[RaboAccessTokenAcquired] = Json.format
  implicit val raboAuthenticationEventFormat: Format[RaboAuthenticationEvent] = Json.format
}

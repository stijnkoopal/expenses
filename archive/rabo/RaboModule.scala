package connectors.rabo

import play.api.inject._
import play.api.{Configuration, Environment}

class RaboModule extends Module {
  override def bindings(environment: Environment, configuration: Configuration): Seq[Binding[_]] = Seq(
    bind[RaboAuthenticationRepository].to[PostgresqlRaboAuthenticationProjection],
    bind[MonetaryAccountsRefreshReactor].toSelf,
    bind[RaboAccountsRepository].to[PostgresRaboAccountsProjection],
    bind[StreamsStarter].toSelf.eagerly()
  )
}

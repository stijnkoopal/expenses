package connectors.rabo

import javax.inject.{Inject, Singleton}
import play.api.Configuration

@Singleton
case class RaboConfiguration @Inject()(private val config: Configuration) {
  val baseUrl: String      = config.get[String]("rabo.baseUrl")
  val clientId: String     = config.get[String]("rabo.clientId")
  val clientSecret: String = config.get[String]("rabo.secret")
  val publicCertificate: String = config
    .get[String]("rabo.publicCertificate")
    .replaceAll("-----END CERTIFICATE-----", "")
    .replaceAll("-----BEGIN CERTIFICATE-----", "")
    .replaceAll("\n", "")
  val privateKey: String = config
    .get[String]("rabo.privateKey")
    .replaceAll("-----END PRIVATE KEY-----", "")
    .replaceAll("-----BEGIN PRIVATE KEY-----", "")
    .replaceAll("\n", "")
  val keyId: String = config.get[String]("rabo.keyId")
}

package connectors.rabo

import java.time.{Clock, LocalDateTime}

import akka.Done
import akka.stream.scaladsl.{Sink, Source}
import akka.stream.{Materializer, SharedKillSwitch}
import app.auth.UserId
import javax.inject.{Inject, Singleton}
import messaging.MessageReceiver
import org.reactivestreams.Publisher
import persistence.DatabaseExecutionContext
import play.api.Logging
import scalikejdbc.interpolation.Implicits._
import scalikejdbc.streams._
import scalikejdbc.{DB, SQLSyntax, WrappedResultSet}

import scala.concurrent.Future

case class RaboTransactionFetchCompleted(
    iban: String,
    lastTransactionTimestamp: LocalDateTime
)

object RaboTransactionFetchCompleted {
  val payloadType: String = "RaboTransactionFetchCompleted"
}

case class RaboAccountFound(
    id: String,
    iban: String,
    currency: String,
    balance: BigDecimal,
    alias: Option[String]
)

object RaboAccountFound {
  val payloadType = "RaboAccountFound"
}

case class RaboAccount(
    userId: UserId,
    iban: String,
    raboId: String,
    currency: String,
    balance: BigDecimal,
    alias: Option[String],
    lastTransactionSync: Option[LocalDateTime] = None,
    inserted: LocalDateTime,
    updated: Option[LocalDateTime] = None
)

case object RaboAccount {
  def fromResultSet(rs: WrappedResultSet): RaboAccount = RaboAccount(
    userId = rs.get("user_id"),
    iban = rs.get("iban"),
    raboId = rs.get("rabo_id"),
    currency = rs.get("currency"),
    balance = rs.get("balance"),
    alias = rs.get("alias"),
    lastTransactionSync = rs.get("last_transactions_sync"),
    inserted = rs.get("inserted"),
    updated = rs.get("updated")
  )
}

trait RaboAccountsRepository {
  def fetchAccounts(): Publisher[RaboAccount]
}

@Singleton
private[rabo] class PostgresRaboAccountsProjection @Inject() (messageReceiver: MessageReceiver, clock: Clock)(
    implicit ec: DatabaseExecutionContext,
    mat: Materializer
) extends RaboAccountsRepository
    with Logging {
  import RaboSerde._
  import serde.PlayJsonDeserialization._

  val tableName     = "rabo_account"
  private val table = SQLSyntax.createUnsafely(tableName)

  def start(killSwitch: SharedKillSwitch): Future[Done] = {
    logger.info(s"Starting to listen for 'RaboAccountFound' messages on '${RaboMessaging.channel}'")
    Source
      .fromPublisher(messageReceiver.receive[RaboAccountFound](RaboMessaging.channel, RaboAccountFound.payloadType))
      .log("Storing rabo account", _.id)
      .runWith(Sink.foreach(handle))
  }

  logger.info(s"Starting to listen for 'RaboTransactionFetchCompleted' messages on '${RaboMessaging.channel}'")
  Source
    .fromPublisher(messageReceiver.receive[RaboTransactionFetchCompleted](RaboMessaging.channel, RaboTransactionFetchCompleted.payloadType))
    .log("Storing last transaction sync")
    .runWith(Sink.foreach(handle))

  override def fetchAccounts(): Publisher[RaboAccount] = DB.readOnlyStream {
    sqlr"select * from $table"
      .map(RaboAccount.fromResultSet)
      .iterator()
  }

  private def upsert(b: RaboAccount) = DB.localTx { implicit session =>
    sqlr"INSERT INTO $table(user_id, iban, rabo_id, currency, balance, last_transactions_sync, alias, inserted) VALUES(${b.userId}, ${b.iban}, ${b.raboId}, ${b.currency}, ${b.balance}, ${b.lastTransactionSync}, ${b.alias}, ${b.inserted}) ON CONFLICT (iban) DO UPDATE SET rabo_id = ${b.raboId}, currency = ${b.currency}, balance = ${b.balance}, alias = ${b.alias}, last_transactions_sync = ${b.lastTransactionSync}, updated = ${b.updated}".update
      .apply()
  }

  private def handle(event: RaboAccountFound): Unit = {
    upsert(
      RaboAccount(
        userId = ???, // TODO
        iban = event.iban,
        raboId = event.id,
        currency = event.currency,
        balance = event.balance,
        alias = event.alias,
        inserted = LocalDateTime.now(clock)
      )
    )
  }

  private def handle(event: RaboTransactionFetchCompleted): Unit = DB.localTx { implicit session =>
    sqlr"UPDATE $table SET last_transactions_sync = ${event.lastTransactionTimestamp}, updated = ${LocalDateTime
      .now(clock)} WHERE iban = ${event.iban}"
      .update()
      .apply()
  }
}

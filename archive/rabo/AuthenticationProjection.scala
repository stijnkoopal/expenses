package connectors.rabo

import java.time.{Clock, LocalDateTime, ZoneOffset}
import java.util.UUID

import akka.Done
import akka.stream.scaladsl.{Sink, Source}
import akka.stream.{Materializer, SharedKillSwitch}
import connectors.rabo.RaboAuthorizationStatus.{Authorized, NotAuthorized}
import eu.timepit.refined.types.string.NonEmptyString
import javax.inject.{Inject, Singleton}
import messaging.{MessagePublisher, MessageReceiver}
import org.reactivestreams.Publisher
import persistence.DatabaseExecutionContext
import play.api.Logging
import scalikejdbc.interpolation.Implicits._
import scalikejdbc.streams._
import scalikejdbc.{DB, SQLSyntax, WrappedResultSet}
import streaming.AkkaStreamImplicits._
import persistence.ScalalikejdbcImplicits._
import scala.concurrent.Future

sealed trait RaboAuthenticationEvent
case class RaboConnectStarted(uniqId: UUID)                                        extends RaboAuthenticationEvent
case class RaboConnectionAccepted(uniqId: UUID, authorizationCode: NonEmptyString) extends RaboAuthenticationEvent
case class RaboAccessTokenAcquired(
    uniqId: UUID,
    accessToken: NonEmptyString,
    accessTokenExpiry: LocalDateTime,
    refreshToken: NonEmptyString,
    refreshTokenExpiry: LocalDateTime
) extends RaboAuthenticationEvent

object RaboAuthenticationEvent {
  val payloadType = "RaboAuthenticationEvent"
}

case class RaboAuthentication(
    userId: types.UserId,
    accessToken: NonEmptyString,
    accessTokenExpiry: LocalDateTime,
    refreshToken: NonEmptyString,
    refreshTokenExpiry: LocalDateTime
)

object RaboAuthentication {
  def fromResultSet(rs: WrappedResultSet): RaboAuthentication = RaboAuthentication(
    userId = rs.get("user_id"),
    accessToken = rs.get("access_token"),
    accessTokenExpiry = rs.get("access_token_expiry"),
    refreshToken = rs.get("refresh_token"),
    refreshTokenExpiry = rs.get("refresh_token_expiry")
  )
}

sealed trait RaboAuthorizationStatus
object RaboAuthorizationStatus {
  case object Authorized    extends RaboAuthorizationStatus
  case object NotAuthorized extends RaboAuthorizationStatus
}

trait RaboAuthenticationRepository {
  def isAuthorized: Publisher[RaboAuthorizationStatus]
  def fetchAuthorization(): Publisher[RaboAuthentication]
}

@Singleton
private[rabo] class PostgresqlRaboAuthenticationProjection @Inject() (messageReceiver: MessageReceiver)(
    implicit ec: DatabaseExecutionContext,
    mat: Materializer
) extends RaboAuthenticationRepository
    with Logging {
  import RaboSerde._
  import serde.PlayJsonDeserialization._

  val tableName     = "rabo_authentication"
  private val table = SQLSyntax.createUnsafely(tableName)

  def start(killSwitch: SharedKillSwitch): Future[Done] = {
    logger.info(s"Starting to listen for 'RaboAuthenticationEvent' messages on '${RaboMessaging.channel}'")
    Source
      .fromPublisher(messageReceiver.receive[RaboAuthenticationEvent](RaboMessaging.channel, RaboAuthenticationEvent.payloadType))
      .runWith(Sink.foreach(handle))
  }

  override def fetchAuthorization(): Publisher[RaboAuthentication] = DB.readOnlyStream {
    sqlr"select * from $table"
      .map(rs => RaboAuthentication.fromResultSet(rs))
      .iterator
  }

  private def deleteAuthentication() = DB.localTx { implicit session =>
    sqlr"delete from $table".update.apply()
  }

  private def upsertAuthentication(b: RaboAuthentication) = DB.localTx { implicit session =>
    deleteAuthentication()
    sqlr"insert into $table values (${b.userId}, ${b.accessToken}, ${b.accessTokenExpiry}, ${b.refreshToken}, ${b.refreshTokenExpiry})".update
      .apply()
  }

  private def handle(event: RaboAuthenticationEvent): Unit = event match {
    case RaboConnectStarted(_) =>
      deleteAuthentication()

    case RaboAccessTokenAcquired(_, accessToken, accessTokenExpiry, refreshToken, refreshTokenExpiry) =>
      val authentication = RaboAuthentication(
        userId = ???, // TODO
        accessToken = accessToken,
        accessTokenExpiry = accessTokenExpiry,
        refreshToken = refreshToken,
        refreshTokenExpiry = refreshTokenExpiry
      )
      upsertAuthentication(authentication)

    case _ =>
  }

  override def isAuthorized: Publisher[RaboAuthorizationStatus] = DB.readOnlyStream {
    sqlr"select case when exists (select 1 from $table) then 1 else 0 end as has_auth"
      .single()
      .map[RaboAuthorizationStatus] { x =>
        if (x.int("has_auth") == 1) Authorized
        else NotAuthorized
      }
      .iterator
  }
}

@Singleton
class RaboOAuthService @Inject() (
    clock: Clock,
    messagePublisher: MessagePublisher,
    exchanger: RaboTokenExchanger,
    readRepository: RaboAuthenticationRepository
)(implicit mat: Materializer) {
  import RaboSerde._
  import serde.PlayJsonSerialization._

  def fetchAccessToken(): Publisher[String] =
    readRepository
      .fetchAuthorization()
      .flatMapMerge(
        1,
        authentication => {
          if (isStillValid(authentication.accessTokenExpiry)) {
            Source.single(authentication.accessToken)
          } else if (isStillValid(authentication.refreshTokenExpiry)) {
            exchanger
              .exchange(Right(authentication.refreshToken))
              .map(oauthResponseToEvent)
              .wireTap(oauth =>
                messagePublisher.publishTo[RaboAccessTokenAcquired](RaboMessaging.channel, RaboAuthenticationEvent.payloadType)
              )
              .map(_.accessToken)
          } else {
            Source.empty[String]
          }
        }
      )
      .runWith(Sink.asPublisher(false))

  private def isStillValid(expiryDate: LocalDateTime) =
    expiryDate.isAfter(LocalDateTime.now(clock).minusMinutes(1))

  private def oauthResponseToEvent(response: RaboOAuthTokenResponse) = RaboAccessTokenAcquired(
    uniqId = UUID.randomUUID(),
    accessToken = response.access_token,
    accessTokenExpiry = LocalDateTime.ofEpochSecond(response.consented_on + response.expires_in, 0, ZoneOffset.UTC),
    refreshToken = response.refresh_token,
    refreshTokenExpiry = LocalDateTime.ofEpochSecond(response.consented_on + response.refresh_token_expires_in, 0, ZoneOffset.UTC)
  )
}

package connectors.rabo

import java.io.InputStream
import java.nio.charset.StandardCharsets.UTF_8
import java.security.{KeyStore, SecureRandom}
import java.time.format.DateTimeFormatter
import java.time.{Clock, LocalDate, LocalDateTime, ZoneId}
import java.util.{Base64, UUID}

import akka.stream.Materializer
import com.google.common.io.BaseEncoding
import com.softwaremill.sttp.playJson.asJson
import com.softwaremill.sttp.{HttpURLConnectionBackend, Id, MonadError, Request, Response, SttpBackend, UriContext, sttp}
import javax.inject.{Inject, Singleton}
import javax.net.ssl.{HttpsURLConnection, KeyManagerFactory, SSLContext, TrustManagerFactory}
import org.reactivestreams.Publisher
import play.api.Environment
import play.api.libs.json.{Json, _}
import serde.{PlayJsonDefaultDeserialization, PlayJsonDefaultSerialization}
import streaming.AkkaStreamsUtil

import scala.util.{Failure, Success}

object RaboApi {
  val SCOPE_BALANCES_READ              = "ais.balances.read"
  val SCOPE_TRANSACTIONS_READ_90_DAYS  = "ais.transactions.read-90days"
  val SCOPES_TRANSACTIONS_READ_HISTORY = "ais.transactions.read-history"

  val allScopes = Seq(SCOPE_BALANCES_READ, SCOPE_TRANSACTIONS_READ_90_DAYS, SCOPES_TRANSACTIONS_READ_HISTORY)
}

@Singleton
class RaboApiOAuthStart @Inject() (config: RaboConfiguration) {
  def redirectUrl(scopes: Seq[String], uniqueId: UUID) =
    s"${config.baseUrl}/oauth2/authorize?client_id=${config.clientId}&response_type=code&scope=${scopes.mkString(" ")}&state=$uniqueId"
}

@Singleton
class RaboTokenExchanger @Inject() (config: RaboConfiguration) {
  implicit val httpBackend: SttpBackend[Id, Nothing] = HttpURLConnectionBackend()

  import RaboApiSerde._

  def exchange(authorizationCodeOrRefreshToken: Either[String, String])(implicit mat: Materializer): Publisher[RaboOAuthTokenResponse] = {
    AkkaStreamsUtil.publisherSingleFromLazyFunc {
      val authorization: String = Base64.getEncoder.encodeToString(s"${config.clientId}:${config.clientSecret}".getBytes(UTF_8))

      val body = authorizationCodeOrRefreshToken match {
        case Left(authorizationCode) =>
          Map(
            "grant_type" -> "authorization_code",
            "code"       -> authorizationCode
          )
        case Right(refreshToken) =>
          Map(
            "grant_type"    -> "refresh_token",
            "refresh_token" -> refreshToken
          )
      }

      val response = sttp
        .post(
          uri"${config.baseUrl}/oauth2/token"
        )
        .headers(
          "Content-Type"  -> "application/x-www-form-urlencoded",
          "Authorization" -> s"Basic $authorization"
        )
        .body(body)
        .response(asJson[RaboOAuthTokenResponse])
        .send()

      response.body match {
        case Left(error) =>
          Failure(new RuntimeException(error))
        case Right(body) =>
          body match {
            case Right(tokenResponse) => Success(tokenResponse)
            case Left(error)          => throw new RuntimeException(error.message)
          }
      }
    }
  }
}

private object HttpURLConnectionBackendWithSSL {
  private val password: Array[Char] = "changeit".toCharArray

  private val keyStore: KeyStore          = KeyStore.getInstance("PKCS12")
  private val keyStoreStream: InputStream = getClass.getClassLoader.getResourceAsStream("rabo/sandbox/cert_key.p12")

  require(keyStoreStream != null, "Keystore required!")
  keyStore.load(keyStoreStream, password)

  private val keyManagerFactory: KeyManagerFactory = KeyManagerFactory.getInstance("SunX509")
  keyManagerFactory.init(keyStore, password)

  private val tmf: TrustManagerFactory = TrustManagerFactory.getInstance("SunX509")
  tmf.init(keyStore)

  val sslContext: SSLContext = SSLContext.getInstance("TLS")
  sslContext.init(keyManagerFactory.getKeyManagers, tmf.getTrustManagers, new SecureRandom)
}

private class HttpURLConnectionBackendWithSSL extends SttpBackend[Id, Nothing] {
  private val backend = HttpURLConnectionBackend(
    customizeConnection = {
      case conn: HttpsURLConnection =>
        conn.setSSLSocketFactory(HttpURLConnectionBackendWithSSL.sslContext.getSocketFactory)
    }
  )

  override def send[T](request: Request[T, Nothing]): Id[Response[T]] = backend.send(request)
  override def close(): Unit                                          = backend.close()
  override def responseMonad: MonadError[Id]                          = backend.responseMonad
}

@Singleton
class RaboAccountsFetcher @Inject() (config: RaboConfiguration, signing: RaboApiSigning, clock: Clock) {
  implicit val httpBackend: SttpBackend[Id, Nothing] = new HttpURLConnectionBackendWithSSL()

  import RaboApiSerde._

  def fetch(accessToken: String)(implicit mat: Materializer): Publisher[RaboApiAccount] = {
    AkkaStreamsUtil.publisherSeqFromLazyFunc {
      val requestId = UUID.randomUUID()
      val body      = ""
      val dateTime  = LocalDateTime.now(clock)

      val response = sttp
        .get(
          uri"${config.baseUrl}/payments/account-information/ais/v3/accounts"
        )
        .headers(
          "Accept"                    -> "application/json",
          "Authorization"             -> s"Bearer $accessToken",
          "x-ibm-client-id"           -> config.clientId,
          "TPP-Signature-Certificate" -> config.publicCertificate,
          "X-Request-ID"              -> requestId.toString,
          "Digest"                    -> signing.digest(body),
          "Signature"                 -> signing.signatureHeader(signing.signString(signing.buildSigningString(dateTime, body, requestId))),
          "Date"                      -> dateTime.atZone(ZoneId.of("GMT")).format(DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss z"))
        )
        .response(asJson[RaboAccountsResponse])
        .send()

      response.body match {
        case Left(error) =>
          Failure(new RuntimeException(error))

        case Right(body) =>
          body match {
            case Right(accountsResponse) => Success(accountsResponse.accounts)
            case Left(error)             => Failure(new RuntimeException(error.message))
          }
      }
    }
  }
}

@Singleton
class RaboTransactionsFetcher @Inject() (config: RaboConfiguration, signing: RaboApiSigning, clock: Clock) {
  implicit val httpBackend: SttpBackend[Id, Nothing] = new HttpURLConnectionBackendWithSSL()

  import RaboApiSerde._

  def fetch(accessToken: String, raboAccountId: String, newerThan: LocalDateTime)(
      implicit mat: Materializer
  ): Publisher[RaboApiTransaction] = {
    AkkaStreamsUtil.publisherSeqFromLazyFunc {
      val requestId = UUID.randomUUID()
      val body      = ""
      val dateTime  = LocalDateTime.now(clock)

      val response = sttp
        .get(
          uri"${config.baseUrl}/payments/account-information/ais/v3/accounts/$raboAccountId/transactions?bookingStatus=booked&dateFrom=2011-08-15T11:51:04.772Z&size=500"
        )
        .headers(
          "Accept"                    -> "application/json",
          "Authorization"             -> s"Bearer $accessToken",
          "x-ibm-client-id"           -> config.clientId,
          "TPP-Signature-Certificate" -> config.publicCertificate,
          "X-Request-ID"              -> requestId.toString,
          "Digest"                    -> signing.digest(body),
          "Signature"                 -> signing.signatureHeader(signing.signString(signing.buildSigningString(dateTime, body, requestId))),
          "Date"                      -> dateTime.atZone(ZoneId.of("GMT")).format(DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss z"))
        )
        .response(asJson[RaboTransactionsResponse])
        .send()

      response.body match {
        case Left(error) =>
          Failure(new RuntimeException(error))

        case Right(body) =>
          body match {
            case Right(transactionsResponse) => Success(transactionsResponse.transactions.booked)
            case Left(error)                 => Failure(new RuntimeException(error.message))
          }
      }
    }
  }
}

case class RaboAccountsResponse(
    accounts: List[RaboApiAccount]
)

case class RaboApiAccount(
    resourceId: String,
    currency: String,
    iban: String,
    name: String,
    status: String
)

case class RaboApiAmount(
    amount: BigDecimal,
    currency: String
)

case class RaboApiExchangeRate(
    currencyFrom: String,
    currencyTo: String,
    rateContract: Option[String],
    rateDate: Option[LocalDateTime],
    rateFrom: BigDecimal,
    rateTo: Option[BigDecimal]
)
case class RaboApiCounterparty(
    currency: String,
    iban: String
)

case class RaboApiTransaction(
    bankTransactionCode: Option[String],
    bookingDate: LocalDate,
    creditorAccount: RaboApiCounterparty,
    creditorAgent: String,
    creditorId: Option[String],
    creditorName: Option[String],
    debtorAccount: RaboApiCounterparty,
    debtorAgent: String,
    debtorName: Option[String],
    endToEndId: Option[String],
    entryReference: String,
    exchangeRate: Option[List[RaboApiExchangeRate]],
    initiatingPartyName: String,
    instructedAmount: Option[RaboApiAmount],
    paymentInformationIdentification: Option[String],
    proprietaryBankTransactionCode: Option[String],
    raboBookingDateTime: LocalDateTime,
    raboDetailedTransactionType: String,
    raboTransactionTypeName: Option[String],
    reasonCode: Option[String],
    transactionAmount: RaboApiAmount,
    valueDate: LocalDate
)

case class RaboTransactionsBookedAndPending(
    booked: List[RaboApiTransaction],
    pending: Option[List[RaboApiTransaction]]
)

case class RaboTransactionsResponse(
    transactions: RaboTransactionsBookedAndPending
)

case class RaboOAuthTokenResponse(
    token_type: String,
    access_token: String,
    expires_in: Int,
    consented_on: Long,
    scope: String,
    refresh_token: String,
    refresh_token_expires_in: Long
)

case class RaboApiError(
    error_description: String
)

object RaboApiSerde extends PlayJsonDefaultSerialization with PlayJsonDefaultDeserialization {
  implicit val raboOAuthTokenResponseFormat: Format[RaboOAuthTokenResponse] = Json.format
  implicit val raboApiError: Format[RaboApiError]                           = Json.format
  implicit val raboAccount: Format[RaboApiAccount]                          = Json.format
  implicit val raboAccountsResponse: Format[RaboAccountsResponse]           = Json.format
  implicit val counterpary: Format[RaboApiCounterparty]                     = Json.format
  implicit val amount: Format[RaboApiAmount]                                = Json.format
  implicit val raboApiExchangeRate: Format[RaboApiExchangeRate]             = Json.format
  implicit val raboApiTransaction: Format[RaboApiTransaction]               = Json.format
  implicit val bookedAndPending: Format[RaboTransactionsBookedAndPending]   = Json.format
  implicit val raboTransactionsResponse: Format[RaboTransactionsResponse]   = Json.format
}

@Singleton
class RaboApiSigning @Inject() (env: Environment, config: RaboConfiguration) {
  import java.security._

  private val digester = MessageDigest.getInstance("SHA-512")

  def digest(body: String): String = {
    val digest512 = digester.digest(body.getBytes("UTF-8"))
    "sha-512=" + BaseEncoding.base64().encode(digest512)
  }

  def buildSigningString(date: LocalDateTime, body: String, requestId: UUID): String =
    s"""date: ${date.atZone(ZoneId.of("GMT")).format(DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss z"))}
       |digest: ${digest(body)}
       |x-request-id: ${requestId.toString}""".stripMargin

  def signString(string: String): String = {
    import java.security.spec.PKCS8EncodedKeySpec
    import java.security.{KeyFactory, Signature}

    val privateKey = config.privateKey

    val kf               = KeyFactory.getInstance("RSA")
    val privateSignature = Signature.getInstance("SHA512withRSA")
    import java.util.Base64
    val b1   = Base64.getDecoder.decode(privateKey)
    val spec = new PKCS8EncodedKeySpec(b1)

    privateSignature.initSign(kf.generatePrivate(spec))
    privateSignature.update(string.getBytes("UTF-8"))
    Base64.getEncoder.encodeToString(privateSignature.sign)
  }

  def signatureHeader(signature: String) =
    s"""keyId="${config.keyId}",algorithm="rsa-sha512",headers="date digest x-request-id", signature="$signature""""
}

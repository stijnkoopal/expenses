package connectors.rabo

import akka.Done
import akka.stream.{Materializer, SharedKillSwitch}
import javax.inject.{Inject, Singleton}
import messaging.{MessagePublisher, MessageReceiver}

import scala.concurrent.Future

@Singleton
private[rabo] class MonetaryAccountsRefreshReactor @Inject() (
    messageReceiver: MessageReceiver,
    messagePublisher: MessagePublisher,
    raboOAuthService: RaboOAuthService,
    accountsFetcher: RaboAccountsFetcher
)(implicit val mat: Materializer) {

  def start(killSwitch: SharedKillSwitch): Future[Done] = Future.successful(Done)

//  private val source = messageReceiver
//    .receive[MonetaryAccountsRefreshRequested](
//      channel = MonetaryAccountMessaging.channel,
//      payloadType = MonetaryAccountsRefreshRequested.payloadType
//    )
//
//  private val publishAccountFound = Flow[RaboApiAccount]
//    .map(buildMonetaryAccountDocument)
//    .to(messagePublisher.publishTo[MonetaryAccountDocument](MonetaryAccountMessaging.channel, MonetaryAccountDocument.payloadType))
//
//  private val publishRaboAccountFound = Flow[RaboApiAccount]
//    .map(buildRaboAccountFound)
//    .to(messagePublisher.publishTo[RaboAccountFound](RaboMessaging.channel, RaboAccountFound.payloadType))
//
//  // TODO: also fetch balances
//
//  def start() =
//    source
//      .flatMapConcat(_ => fetchAccessToken())
//      .flatMapConcat(fetchAccounts)
//      .wireTap(publishRaboAccountFound)
//      .wireTap(publishAccountFound)
//      .withAttributes(supervisionStrategy(Supervision.resumingDecider))
//      .runWith(Sink.ignore)
//
//  private def fetchAccessToken(): Source[String, NotUsed] =
//    raboOAuthService.fetchAccessToken()
//
//  private def fetchAccounts(token: String): Source[RaboApiAccount, NotUsed] =
//    accountsFetcher.fetch(token)
//
//  private def buildMonetaryAccountDocument(raboAccount: RaboApiAccount) = MonetaryAccountDocument(
//    userId = ???, // TODO
//    iban = raboAccount.iban,
//    institution = Some("Rabo"),
//    alias = Option(raboAccount.name),
//    currency = raboAccount.currency,
//    balance = 0
//  )
//
//  private def buildRaboAccountFound(raboAccount: RaboApiAccount) = RaboAccountFound(
//    id = raboAccount.resourceId,
//    iban = raboAccount.iban,
//    currency = raboAccount.currency,
//    balance = 0,
//    alias = Option(raboAccount.name)
//  )
}

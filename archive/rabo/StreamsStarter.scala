package connectors.rabo

import akka.Done
import akka.actor.CoordinatedShutdown
import akka.stream.KillSwitches
import javax.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class StreamsStarter @Inject() (
    monetaryAccountsRefreshReactor: MonetaryAccountsRefreshReactor,
    accountsProjection: PostgresRaboAccountsProjection,
    authenticationProjection: PostgresqlRaboAuthenticationProjection,
    cs: CoordinatedShutdown
)(implicit ec: ExecutionContext) {
  private val killSwitch = KillSwitches.shared("Rabo")

  private val futures = List(
    accountsProjection.start(killSwitch),
    authenticationProjection.start(killSwitch),
    monetaryAccountsRefreshReactor.start(killSwitch)
  )

  cs.addTask(CoordinatedShutdown.PhaseServiceUnbind, "stop-rabo-streams") { () =>
    killSwitch.shutdown()
    Future.sequence(futures).map(_ => Done)
  }
}

package connectors.rabo

import java.time.{LocalDateTime, ZoneOffset}
import java.util.UUID
import java.util.UUID.randomUUID

import akka.stream.Materializer
import akka.stream.scaladsl.{Sink, Source}
import app.auth.AuthEnforcedAction
import com.softwaremill.sttp.{HttpURLConnectionBackend, Id, SttpBackend}
import http.ErrorResponse
import javax.inject._
import messaging.MessagePublisher
import play.api.libs.json.Json
import play.api.mvc._
import streaming.AkkaStreamImplicits._

import scala.concurrent.Future

@Singleton
class OAuthController @Inject() (
    cc: ControllerComponents,
    messagePublisher: MessagePublisher,
    readRepository: RaboAuthenticationRepository,
    raboApiStart: RaboApiOAuthStart,
    exchanger: RaboTokenExchanger,
    accountsFetcher: RaboAccountsFetcher,
    raboOAuthService: RaboOAuthService,
    secureAction: AuthEnforcedAction
)(implicit val mat: Materializer)
    extends AbstractController(cc) {
  import RaboSerde._
  import http.HttpSerde._
  import serde.PlayJsonSerialization._

  implicit val httpBackend: SttpBackend[Id, Nothing] = HttpURLConnectionBackend()

  def start(): Action[AnyContent] = secureAction { _ =>
    val uniqueId = randomUUID()
    Source
      .single(RaboConnectStarted(uniqueId))
      .runWith(
        Sink.fromSubscriber(messagePublisher.publishTo[RaboConnectStarted](RaboMessaging.channel, RaboAuthenticationEvent.payloadType))
      )

    Redirect(raboApiStart.redirectUrl(RaboApi.allScopes, uniqueId))
  }

  def authorize(code: Option[String], error: Option[String], state: UUID): Action[AnyContent] = Action.async { _: Request[AnyContent] =>
    Future.successful(Ok(""))

    val codeOrError = error match {
      case Some(e) => Right(e)
      case None =>
        code match {
          case Some(c) => Left(c)
          case None    => Right("No `code` or `error` provided")
        }
    }

    codeOrError match {
      case Left(authorizationCode) =>
        Source
          .single(RaboConnectionAccepted(state, authorizationCode))
          .wireTap(messagePublisher.publishTo[RaboConnectionAccepted](RaboMessaging.channel, RaboAuthenticationEvent.payloadType))
          .flatMapConcat(_ => exchanger.exchange(Left(authorizationCode)))
          .map(oauthResponse =>
            RaboAccessTokenAcquired(
              state,
              oauthResponse.access_token,
              LocalDateTime.ofEpochSecond(oauthResponse.consented_on + oauthResponse.expires_in, 0, ZoneOffset.UTC),
              oauthResponse.refresh_token,
              LocalDateTime.ofEpochSecond(oauthResponse.consented_on + oauthResponse.refresh_token_expires_in, 0, ZoneOffset.UTC)
            )
          )
          .wireTap(messagePublisher.publishTo[RaboAccessTokenAcquired](RaboMessaging.channel, RaboAuthenticationEvent.payloadType))
          .map(_ => Ok("OK"))
          .runWith(Sink.head)

      case Right(e) =>
        Future.successful(ExpectationFailed(Json.toJson(ErrorResponse(e))))
    }
  }
}
